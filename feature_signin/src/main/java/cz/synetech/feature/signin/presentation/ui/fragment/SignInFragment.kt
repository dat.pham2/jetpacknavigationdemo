package cz.synetech.feature.signin.presentation.ui.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import cz.synetech.base.databinding.ext.setupDataBinding
import cz.synetech.base.livedata.observer.SpecificEventObserver
import cz.synetech.base.logger.Logger
import cz.synetech.baseapp.ui.actionbar.ActionBarController
import cz.synetech.feature.signin.BR
import cz.synetech.feature.signin.InternalSignInRouter
import cz.synetech.feature.signin.R
import cz.synetech.feature.signin.SignInFeatureModule
import cz.synetech.feature.signin.SignInRouter
import cz.synetech.feature.signin.databinding.FragmentSignInBinding
import cz.synetech.feature.signin.domain.externalsignin.ExternalSignInCallback
import cz.synetech.feature.signin.domain.externalsignin.FacebookSignInManager
import cz.synetech.feature.signin.domain.externalsignin.GoogleSignInManager
import cz.synetech.feature.signin.presentation.viewmodel.SignInFragmentViewModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class SignInFragment : Fragment() {
    private val router: SignInRouter by inject()
    private val internalRouter: InternalSignInRouter by inject()
    private val actionBarController: ActionBarController by inject()
    private val fragmentViewModel: SignInFragmentViewModel by viewModel()
    private val fbManager: FacebookSignInManager by lazy { FacebookSignInManager(fbCallback) }
    private val googleManager: GoogleSignInManager by lazy { GoogleSignInManager(googleCallback) }

    private val fbCallback = object : ExternalSignInCallback {
        override fun onSuccess(data: Bundle?) {
            data?.getString(FacebookSignInManager.FACEBOOK_SIGN_IN_TOKEN_KEY)?.let { accessToken ->
                fragmentViewModel.onFacebookSignedIn(accessToken)
            }
        }

        override fun onCancel(data: Bundle?) {
            Logger.w("Facebook sign-in canceled.")
        }

        override fun onError(exception: Exception) {
            Logger.e("Facebook sign-in failed.", exception)
            context?.let { ctx ->
                Toast.makeText(ctx, "An error occurred, please retry.", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private val googleCallback = object : ExternalSignInCallback {
        override fun onSuccess(data: Bundle?) {
            data?.getString(GoogleSignInManager.GOOGLE_SIGN_IN_EMAIL_KEY)?.let { email ->
                fragmentViewModel.onGoogleSignedIn(email)
            }
        }

        override fun onCancel(data: Bundle?) {
            Logger.w("Google Sign-in canceled.")
        }

        override fun onError(exception: Exception) {
            Logger.e("Google Sign-in failed.", exception)
            context?.let { ctx ->
                Toast.makeText(ctx, "An error occurred, please retry.", Toast.LENGTH_SHORT).show()
            }
        }
    }

    init {
        SignInFeatureModule.load()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity as? AppCompatActivity)?.let { actionBarController.setVisibility(it, false) }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val binding: FragmentSignInBinding = setupDataBinding(
            R.layout.fragment_sign_in,
            BR.viewModel to fragmentViewModel
        )
        binding.lifecycleOwner = viewLifecycleOwner
        fragmentViewModel.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observe()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            GoogleSignInManager.GOOGLE_SIGN_IN_REQUEST_CODE -> {
                googleManager.onActivityResult(requestCode, resultCode, data)
            }
            FacebookSignInManager.FACEBOOK_SIGN_IN_REQUEST_CODE -> {
                fbManager.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    private fun observe() {
        fragmentViewModel.signInEvent.observe(viewLifecycleOwner, SpecificEventObserver { event ->
            when (event) {
                is SignInFragmentViewModel.SignInEvent.SignInWithFacebookEvent -> {
                    fbManager.signIn(this)
                }
                is SignInFragmentViewModel.SignInEvent.SignInWithGoogleEvent -> {
                    googleManager.signIn(this)
                }
                is SignInFragmentViewModel.SignInEvent.SignInWithEmailEvent -> {
                    internalRouter.openSignInWithEmailScreen(this)
                }
                is SignInFragmentViewModel.SignInEvent.OnSignInSuccess -> {
                    router.toMain(requireActivity())
                }
            }
        })
    }
}
