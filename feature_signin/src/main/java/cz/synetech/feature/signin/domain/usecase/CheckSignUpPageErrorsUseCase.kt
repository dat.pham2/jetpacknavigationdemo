package cz.synetech.feature.signin.domain.usecase

import cz.synetech.feature.signin.R
import cz.synetech.feature.signin.domain.model.SignUpPageErrorsModel

interface CheckSignUpPageErrorsUseCase {
    fun checkSignUpPageErrors(
        email: String?,
        password: String?,
        confirmPassword: String?,
        tncChecked: Boolean
    ): SignUpPageErrorsModel
}

class CheckSignUpPageErrorsUseCaseImpl(
    private val loginRequirementsValidationUseCase: LoginRequirementsValidationUseCase
) : CheckSignUpPageErrorsUseCase {

    @Suppress("ComplexMethod")
    override fun checkSignUpPageErrors(
        email: String?,
        password: String?,
        confirmPassword: String?,
        tncChecked: Boolean
    ): SignUpPageErrorsModel {
        var emailErrorHintResId: Int? = null
        var passwordErrorHintResId: Int? = null
        var confirmPasswordErrorHintResId: Int? = null

        if (!isEmailValid(email)) {
            emailErrorHintResId = R.string.signup_error_email
        }

        var passwordFilled = false
        if (password?.isNotBlank() == true) {
            if (!isPasswordValid(password)) {
                passwordErrorHintResId = R.string.signup_error_password_weak
            }
            passwordFilled = true
        } else {
            passwordErrorHintResId = R.string.signup_error_password_missing
        }

        var confirmPasswordFilled = false
        if (confirmPassword?.isNotBlank() == true) {
            if (!isPasswordValid(confirmPassword)) {
                confirmPasswordErrorHintResId = R.string.signup_error_password_weak
            }
            confirmPasswordFilled = true
        } else {
            confirmPasswordErrorHintResId = R.string.signup_error_password_missing
        }

        if (passwordFilled && confirmPasswordFilled) {
            if (password != confirmPassword) {
                passwordErrorHintResId = R.string.signup_error_password_mismatch
                confirmPasswordErrorHintResId = R.string.signup_error_password_mismatch
            } else if (!isPasswordValid(password)) {
                passwordErrorHintResId = R.string.signup_error_password_weak
                confirmPasswordErrorHintResId = R.string.signup_error_password_weak
            }
        }

        return SignUpPageErrorsModel(
            emailErrorHintResId = emailErrorHintResId,
            passwordErrorHintResId = passwordErrorHintResId,
            confirmPasswordErrorHintResId = confirmPasswordErrorHintResId,
            tncUnchecked = !tncChecked
        )
    }

    private fun isEmailValid(email: String?): Boolean {
        return loginRequirementsValidationUseCase.checkEmailValid(email)
    }

    private fun isPasswordValid(password: String?): Boolean {
        return loginRequirementsValidationUseCase.checkPasswordValid(password)
    }
}
