package cz.synetech.feature.signin.data.datasource

import io.reactivex.rxjava3.core.Completable
import java.util.concurrent.TimeUnit

interface EmailSignInDatasource {
    fun signIn(email: String, password: String): Completable
}

class EmailSignInDatasourceMocked : EmailSignInDatasource {

    override fun signIn(email: String, password: String): Completable {
        return Completable.timer(REQUEST_DELAY_S, TimeUnit.SECONDS)
    }

    private companion object {
        private const val REQUEST_DELAY_S = 2L
    }
}
