package cz.synetech.feature.signin.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import cz.synetech.base.databinding.ext.setupDataBinding
import cz.synetech.base.livedata.observer.SpecificEventObserver
import cz.synetech.feature.signin.InternalSignInRouter
import cz.synetech.feature.signin.R
import cz.synetech.feature.signin.SignInFeatureModule
import cz.synetech.feature.signin.SignInRouter
import cz.synetech.feature.signin.databinding.FragmentSignUpBinding
import cz.synetech.feature.signin.presentation.viewmodel.SignUpFragmentViewModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class SignUpFragment : Fragment() {
    private val router: SignInRouter by inject()
    private val internalRouter: InternalSignInRouter by inject()
    private val fragmentViewModel: SignUpFragmentViewModel by viewModel()

    init {
        SignInFeatureModule.load()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val dataBinding: FragmentSignUpBinding = setupDataBinding(R.layout.fragment_sign_up)
        dataBinding.lifecycleOwner = viewLifecycleOwner
        dataBinding.viewModel = fragmentViewModel
        fragmentViewModel.lifecycleOwner = viewLifecycleOwner
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observe()
    }

    private fun observe() {
        fragmentViewModel.signUpEvent.observe(viewLifecycleOwner, SpecificEventObserver(::handleSignUpEvent))
        fragmentViewModel.onBackPressedEvent.observe(viewLifecycleOwner, SpecificEventObserver {
            activity?.onBackPressed()
        })
    }

    private fun handleSignUpEvent(event: SignUpFragmentViewModel.SignUpNavigationEvent) {
        when (event) {
            is SignUpFragmentViewModel.SignUpNavigationEvent.SignUpSuccessfulEvent -> {
                showSignUpSuccessfulDialog()
            }
            is SignUpFragmentViewModel.SignUpNavigationEvent.SignUpFailedEvent -> {
                showRetryDialog()
            }
            is SignUpFragmentViewModel.SignUpNavigationEvent.TermsAndConditionsEvent -> {
                internalRouter.toTermsAndConditions(this)
            }
        }
    }

    private fun showSignUpSuccessfulDialog() {
        activity?.takeIf { !it.isFinishing && !it.isDestroyed }?.let { activity ->
            AlertDialog.Builder(activity)
                .setCancelable(false)
                .setTitle(R.string.signup_successful_dialog_title)
                .setMessage(R.string.signup_successful_dialog_message)
                .setPositiveButton(R.string.common_ok) { _, _ ->
                    router.toMain(activity)
                }
                .show()
        }
    }

    private fun showRetryDialog() {
        activity?.takeIf { !it.isFinishing && !it.isDestroyed }?.let { activity ->
            AlertDialog.Builder(activity)
                .setMessage(R.string.common_networking_error_description)
                .setPositiveButton(R.string.common_retry) { _, _ ->
                    fragmentViewModel.onSignUpClicked()
                }
                .setNegativeButton(R.string.common_cancel) { _, _ -> Unit }
                .show()
        }
    }
}
