package cz.synetech.feature.signin.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cz.synetech.base.livedata.model.Event
import cz.synetech.base.logger.Logger
import cz.synetech.baseapp.viewmodel.BackHandlingViewModel
import cz.synetech.feature.signin.domain.model.noError
import cz.synetech.feature.signin.domain.usecase.CheckSignUpPageErrorsUseCase
import cz.synetech.feature.signin.domain.usecase.SignUpUseCase
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers

class SignUpFragmentViewModel(
    private val signUpUseCase: SignUpUseCase,
    private val checkSignUpPageErrorsUseCase: CheckSignUpPageErrorsUseCase
) : BackHandlingViewModel() {
    private val _signUpEvent = MutableLiveData<SignUpNavigationEvent>()
    val signUpEvent: LiveData<SignUpNavigationEvent> = _signUpEvent

    val isLoading = MutableLiveData<Boolean>(false)
    val emailText = MutableLiveData<String>()
    val passwordText = MutableLiveData<String>()
    val confirmPasswordText = MutableLiveData<String>()
    val emailErrorHintResId = MutableLiveData<Int>()
    val passwordErrorHintResId = MutableLiveData<Int>()
    val confirmPasswordErrorHintResId = MutableLiveData<Int>()
    val showTnCUncheckedError = MutableLiveData<Boolean>(false)
    val tncChecked = MutableLiveData<Boolean>()

    fun onSignUpClicked() {
        startStopDisposeBag?.let { bag ->
            if (areInputsValid()) {
                val email = emailText.value ?: ""
                val password = passwordText.value ?: ""
                signUpUseCase.signUp(email = email, password = password)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .doOnSubscribe { isLoading.postValue(true) }
                    .doFinally { isLoading.postValue(false) }
                    .subscribe({
                        _signUpEvent.postValue(SignUpNavigationEvent.SignUpSuccessfulEvent())
                    }, { thr ->
                        Logger.e(thr)
                        _signUpEvent.postValue(SignUpNavigationEvent.SignUpFailedEvent())
                    })
                    .addTo(bag)
            }
        }
    }

    fun onTermsAndConditionsClicked() {
        _signUpEvent.postValue(SignUpNavigationEvent.TermsAndConditionsEvent())
    }

    private fun areInputsValid(): Boolean {
        val signUpPageErrors = checkSignUpPageErrorsUseCase.checkSignUpPageErrors(
            email = emailText.value,
            password = passwordText.value,
            confirmPassword = confirmPasswordText.value,
            tncChecked = tncChecked.value ?: false
        )
        emailErrorHintResId.postValue(signUpPageErrors.emailErrorHintResId)
        passwordErrorHintResId.postValue(signUpPageErrors.passwordErrorHintResId)
        confirmPasswordErrorHintResId.postValue(signUpPageErrors.confirmPasswordErrorHintResId)
        showTnCUncheckedError.postValue(signUpPageErrors.tncUnchecked)
        return signUpPageErrors.noError()
    }

    fun onCheckBoxClicked() {
        tncChecked.postValue(tncChecked.value?.not() ?: true)
    }

    sealed class SignUpNavigationEvent : Event() {
        class TermsAndConditionsEvent : SignUpNavigationEvent()
        class SignUpSuccessfulEvent : SignUpNavigationEvent()
        class SignUpFailedEvent : SignUpNavigationEvent()
    }
}
