package cz.synetech.feature.signin.domain.externalsignin

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment

interface ExternalSignInManager {
    fun signIn(fragment: Fragment, data: Bundle? = null)
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean
}
