package cz.synetech.feature.signin.domain.model

data class SignUpPageErrorsModel(
    val emailErrorHintResId: Int?,
    val passwordErrorHintResId: Int?,
    val confirmPasswordErrorHintResId: Int?,
    val tncUnchecked: Boolean
)

fun SignUpPageErrorsModel.noError(): Boolean {
    return emailErrorHintResId == null && passwordErrorHintResId == null &&
            confirmPasswordErrorHintResId == null && !tncUnchecked
}
