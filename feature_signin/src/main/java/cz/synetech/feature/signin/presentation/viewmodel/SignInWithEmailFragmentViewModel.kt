package cz.synetech.feature.signin.presentation.viewmodel

import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cz.synetech.base.livedata.model.Event
import cz.synetech.base.logger.Logger
import cz.synetech.baseapp.viewmodel.BackHandlingViewModel
import cz.synetech.feature.signin.R
import cz.synetech.feature.signin.domain.usecase.SignInEmailUseCase
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers

class SignInWithEmailFragmentViewModel(
    private val signInEmailUseCase: SignInEmailUseCase
) : BackHandlingViewModel() {
    private val _signInEvent = MutableLiveData<Event>()
    val signInEvent: LiveData<Event> = _signInEvent

    val isLoading = MutableLiveData<Boolean>(false)
    val emailErrorHintResId = MutableLiveData<Int>()
    val passwordErrorHintResId = MutableLiveData<Int>()
    val emailText = MutableLiveData<String>()
    val passwordText = MutableLiveData<String>()

    fun onSignInClicked() {
        if (isLoading.value == false) {
            handleValidation(emailText.value?.trim(), passwordText.value?.trim())
        }
    }

    private fun login(email: String, password: String) {
        startStopDisposeBag?.let { bag ->
            signInEmailUseCase.loginWithEmail(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .doOnSubscribe { isLoading.postValue(true) }
                .doFinally { isLoading.postValue(false) }
                .subscribe({
                    _signInEvent.postValue(NavigationEvent.SignInSuccessful())
                }, { throwable ->
                    Logger.e(throwable)
                    _signInEvent.postValue(NavigationEvent.SignInError())
                })
                .addTo(bag)
        }
    }

    private fun handleValidation(email: String?, password: String?) {
        val emailValid = isEmailValid(email)
        val passwordNotEmpty = isPasswordValid(password)

        if (emailValid && passwordNotEmpty) {
            hideErrorHints()
            email ?: return
            password ?: return
            login(email, password)
        } else {
            val emailNotEmpty = email?.isNotBlank() == true
            handleEmailErrorHintVisibility(!emailNotEmpty, emailValid)
            handlePasswordErrorHintVisibility(!passwordNotEmpty)
        }
    }

    private fun isEmailValid(email: String?): Boolean {
        return email?.isNotBlank() == true && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun isPasswordValid(password: String?): Boolean {
        return password?.isNotBlank() == true
    }

    private fun hideErrorHints() {
        handleEmailErrorHintVisibility(isEmpty = false, isValid = true)
        handlePasswordErrorHintVisibility(isEmpty = false)
    }

    private fun handleEmailErrorHintVisibility(isEmpty: Boolean, isValid: Boolean) {
        emailErrorHintResId.postValue(
            if (isEmpty) {
                R.string.signin_email_email_validation_missing
            } else {
                if (isValid) {
                    0
                } else {
                    R.string.signin_email_email_validation_error
                }
            }
        )
    }

    private fun handlePasswordErrorHintVisibility(isEmpty: Boolean) {
        passwordErrorHintResId.postValue(
            if (isEmpty) {
                R.string.signin_email_password_validation_missing
            } else {
                0
            }
        )
    }

    fun onSignUpClicked() {
        _signInEvent.postValue(NavigationEvent.ToSignUp())
    }

    fun onForgotPasswordClicked() {
        _signInEvent.postValue(NavigationEvent.ToForgotPassword())
    }

    sealed class NavigationEvent : Event() {
        class SignInSuccessful : NavigationEvent()
        class SignInError : NavigationEvent()
        class ToForgotPassword : NavigationEvent()
        class ToSignUp : NavigationEvent()
    }
}
