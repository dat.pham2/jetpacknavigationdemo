package cz.synetech.feature.signin.domain.usecase

import cz.synetech.feature.signin.data.datasource.EmailSignUpDatasource
import cz.synetech.feature.signin.domain.model.SignUpMethod
import cz.synetech.feature.signin.domain.repository.CredentialsRepository
import io.reactivex.rxjava3.core.Completable

interface SignUpUseCase {
    fun signUp(email: String, password: String): Completable
}

class SignUpUseCaseImpl(
    private val signUpDatasource: EmailSignUpDatasource,
    private val credentialsRepository: CredentialsRepository
) : SignUpUseCase {
    override fun signUp(email: String, password: String): Completable {
        return signUpDatasource.signUp(email, password).doOnComplete {
            credentialsRepository.storeSignUpInfo(SignUpMethod.EMAIL, email, password)
        }
    }
}
