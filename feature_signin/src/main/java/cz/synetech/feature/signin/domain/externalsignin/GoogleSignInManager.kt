package cz.synetech.feature.signin.domain.externalsignin

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import cz.synetech.base.logger.Logger
import java.lang.IllegalStateException

class GoogleSignInManager(
    private val callback: ExternalSignInCallback
) : ExternalSignInManager {

    override fun signIn(fragment: Fragment, data: Bundle?) {
        fragment.context?.let { ctx ->
            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build()
            val googleSignInClient = GoogleSignIn.getClient(ctx, gso)
            googleSignInClient.signOut()
            val signInIntent = googleSignInClient.signInIntent
            fragment.startActivityForResult(signInIntent, GOOGLE_SIGN_IN_REQUEST_CODE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        if (requestCode == GOOGLE_SIGN_IN_REQUEST_CODE) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            return handleSignInResult(task)
        }
        return false
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>): Boolean {
        try {
            val account = completedTask.getResult(ApiException::class.java)
                    ?: throw IllegalStateException("Account is null.")
            val bundle = Bundle()
            bundle.putString(GOOGLE_SIGN_IN_EMAIL_KEY, account.email)
            callback.onSuccess(bundle)
            return true
        } catch (e: ApiException) {
            callback.onError(e)
            Logger.e("Google signInResult:failed code=${e.statusCode}", e)
        } catch (e: IllegalStateException) {
            callback.onError(e)
            Logger.e("Google signInResult failed.", e)
        }
        return false
    }

    companion object {
        const val GOOGLE_SIGN_IN_REQUEST_CODE = 1002
        const val GOOGLE_SIGN_IN_EMAIL_KEY = "google_email"
    }
}
