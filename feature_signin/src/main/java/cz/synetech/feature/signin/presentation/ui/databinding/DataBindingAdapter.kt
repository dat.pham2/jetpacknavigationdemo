package cz.synetech.feature.signin.presentation.ui.databinding

import android.graphics.Paint
import androidx.databinding.BindingAdapter
import com.google.android.material.textview.MaterialTextView

@BindingAdapter("underline")
fun MaterialTextView.underline(underline: Boolean?) {
    paintFlags = if (underline == true) {
        paintFlags or Paint.UNDERLINE_TEXT_FLAG
    } else {
        paintFlags and Paint.UNDERLINE_TEXT_FLAG.inv()
    }
}
