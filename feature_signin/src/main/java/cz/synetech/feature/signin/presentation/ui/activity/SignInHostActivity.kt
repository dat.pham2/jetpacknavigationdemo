package cz.synetech.feature.signin.presentation.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import cz.synetech.base.databinding.ext.setupDataBinding
import cz.synetech.feature.signin.R
import cz.synetech.feature.signin.databinding.ActivitySignInHostBinding

class SignInHostActivity : AppCompatActivity() {
    private val layoutId: Int = R.layout.activity_sign_in_host
    private lateinit var navigationController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDataBinding<ActivitySignInHostBinding>(layoutId)
        setupNavController()
    }

    private fun setupNavController() {
        navigationController = Navigation.findNavController(this, R.id.fr_sign_in_host)
        val appBarConfiguration = AppBarConfiguration(setOf(
            R.id.fragment_sign_in
        ))
        NavigationUI.setupActionBarWithNavController(this, navigationController, appBarConfiguration)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navigationController.navigateUp()
    }
}
