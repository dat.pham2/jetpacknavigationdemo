package cz.synetech.feature.signin

import android.app.Activity
import androidx.fragment.app.Fragment

interface SignInRouter {
    fun toForgotPassword(caller: Fragment)
    fun toMain(activity: Activity)
}
