package cz.synetech.feature.signin.domain.usecase

import java.util.regex.Pattern

interface LoginRequirementsValidationUseCase {
    fun checkPasswordValid(password: String?): Boolean
    fun checkEmailValid(email: String?): Boolean
}

class LoginRequirementsValidationUseCaseImpl : LoginRequirementsValidationUseCase {

    override fun checkPasswordValid(password: String?): Boolean {
        password ?: return false
        val isLongEnough = password.length >= MIN_PASSWORD_LENGTH
        val hasDigit = password.any { it.isDigit() }
        val hasUpperCase = password.any { it.isUpperCase() }
        val hasLowerCase = password.any { it.isLowerCase() }
        val hasSpecialCharacter = password.any { !it.isLetterOrDigit() }
        val numberOfCases = hasDigit.int + hasUpperCase.int + hasLowerCase.int + hasSpecialCharacter.int
        return password.isNotBlank() && isLongEnough && numberOfCases >= MIN_CHAR_SETS
    }

    override fun checkEmailValid(email: String?): Boolean {
        val pattern = Pattern.compile(EMAIL_REGEX)
        return email?.isNotBlank() == true && pattern.matcher(email).matches()
    }

    private companion object {
        private const val MIN_PASSWORD_LENGTH = 8
        private const val EMAIL_REGEX = "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w{2,}([-.]\\w+)*$"
        private const val MIN_CHAR_SETS = 3
    }

    private val Boolean.int
        get() = if (this) 1 else 0
}
