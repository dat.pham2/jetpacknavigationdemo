package cz.synetech.feature.signin

import cz.synetech.base.koin.KoinFeatureModule
import cz.synetech.feature.signin.data.datasource.EmailSignInDatasource
import cz.synetech.feature.signin.data.datasource.EmailSignInDatasourceMocked
import cz.synetech.feature.signin.data.datasource.EmailSignUpDatasource
import cz.synetech.feature.signin.data.datasource.EmailSignUpDatasourceMocked
import cz.synetech.feature.signin.domain.usecase.CheckSignUpPageErrorsUseCase
import cz.synetech.feature.signin.domain.usecase.CheckSignUpPageErrorsUseCaseImpl
import cz.synetech.feature.signin.domain.usecase.LoginRequirementsValidationUseCase
import cz.synetech.feature.signin.domain.usecase.LoginRequirementsValidationUseCaseImpl
import cz.synetech.feature.signin.domain.usecase.SignInEmailUseCase
import cz.synetech.feature.signin.domain.usecase.SignInEmailUseCaseImpl
import cz.synetech.feature.signin.domain.usecase.SignUpUseCase
import cz.synetech.feature.signin.domain.usecase.SignUpUseCaseImpl
import cz.synetech.feature.signin.presentation.viewmodel.SignInFragmentViewModel
import cz.synetech.feature.signin.presentation.viewmodel.SignInWithEmailFragmentViewModel
import cz.synetech.feature.signin.presentation.viewmodel.SignUpFragmentViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module
import org.koin.experimental.builder.factoryBy

// EXTERNAL DEPENDENCIES, that must be provided by Koin:
// - SigninRouter (from this module)

internal object SignInFeatureModule : KoinFeatureModule("signin") {

    override fun provideKoinModules(): List<Module> {
        return listOf(viewModelModule(), internalRouterModule(), dataModule(), domainModule())
    }

    private fun viewModelModule() = module {
        viewModel {
            SignInFragmentViewModel(credentialsRepository = get())
        }

        viewModel {
            SignInWithEmailFragmentViewModel(signInEmailUseCase = get())
        }

        viewModel {
            SignUpFragmentViewModel(signUpUseCase = get(), checkSignUpPageErrorsUseCase = get())
        }
    }

    private fun internalRouterModule() = module {
        single<InternalSignInRouter> { InternalSignInRouterImpl() }
    }

    private fun dataModule() = module {
        factoryBy<EmailSignInDatasource, EmailSignInDatasourceMocked>()
        factoryBy<EmailSignUpDatasource, EmailSignUpDatasourceMocked>()
    }

    private fun domainModule() = module {
        factoryBy<SignInEmailUseCase, SignInEmailUseCaseImpl>()
        factoryBy<SignUpUseCase, SignUpUseCaseImpl>()
        factoryBy<LoginRequirementsValidationUseCase, LoginRequirementsValidationUseCaseImpl>()
        factoryBy<CheckSignUpPageErrorsUseCase, CheckSignUpPageErrorsUseCaseImpl>()
    }
}
