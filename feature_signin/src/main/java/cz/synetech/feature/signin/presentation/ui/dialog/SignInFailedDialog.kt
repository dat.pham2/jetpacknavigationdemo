package cz.synetech.feature.signin.presentation.ui.dialog

import android.content.Context
import cz.synetech.baseapp.ui.dialog.PositiveButtonDialog
import cz.synetech.feature.signin.R

internal class SignInFailedDialog(
    context: Context,
    buttonClickCallback: (() -> Unit)? = null
) : PositiveButtonDialog(
    context = context,
    buttonClickCallback = buttonClickCallback ?: {},
    titleResId = R.string.signin_error_title,
    messageResId = R.string.signin_error_message,
    buttonResId = R.string.common_ok
)
