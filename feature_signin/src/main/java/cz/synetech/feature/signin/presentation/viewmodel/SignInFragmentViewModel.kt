package cz.synetech.feature.signin.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cz.synetech.base.livedata.model.Event
import cz.synetech.base.viewmodel.LifecycleViewModel
import cz.synetech.feature.signin.domain.model.SignUpMethod
import cz.synetech.feature.signin.domain.repository.CredentialsRepository

internal class SignInFragmentViewModel(
    private val credentialsRepository: CredentialsRepository
) : LifecycleViewModel() {
    private val _signInEvent = MutableLiveData<SignInEvent>()
    val signInEvent: LiveData<SignInEvent> = _signInEvent

    fun onSignInWithFacebook() {
        _signInEvent.postValue(SignInEvent.SignInWithFacebookEvent())
    }

    fun onSignInWithGoogle() {
        _signInEvent.postValue(SignInEvent.SignInWithGoogleEvent())
    }

    fun onSignInWithEmail() {
        _signInEvent.postValue(SignInEvent.SignInWithEmailEvent())
    }

    fun onGoogleSignedIn(email: String) {
        credentialsRepository.storeSignUpInfo(SignUpMethod.GOOGLE, email, null)
        _signInEvent.postValue(SignInEvent.OnSignInSuccess())
    }

    fun onFacebookSignedIn(accessToken: String) {
        credentialsRepository.storeSignUpInfo(SignUpMethod.FACEBOOK, accessToken, null)
        _signInEvent.postValue(SignInEvent.OnSignInSuccess())
    }

    sealed class SignInEvent : Event() {
        class SignInWithFacebookEvent : SignInEvent()
        class SignInWithGoogleEvent : SignInEvent()
        class SignInWithEmailEvent : SignInEvent()
        class OnSignInSuccess : SignInEvent()
    }
}
