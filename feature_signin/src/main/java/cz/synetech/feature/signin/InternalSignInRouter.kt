package cz.synetech.feature.signin

import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import cz.synetech.feature.signin.presentation.ui.fragment.SignInFragmentDirections
import cz.synetech.feature.signin.presentation.ui.fragment.SignInWithEmailFragmentDirections
import cz.synetech.feature.signin.presentation.ui.fragment.SignUpFragmentDirections

internal interface InternalSignInRouter {
    fun openSignInWithEmailScreen(caller: Fragment)
    fun toSignUp(caller: Fragment)
    fun toTermsAndConditions(caller: Fragment)
}

internal class InternalSignInRouterImpl : InternalSignInRouter {
    override fun openSignInWithEmailScreen(caller: Fragment) {
        caller.findNavController().navigate(
            SignInFragmentDirections.toEmailSignIn()
        )
    }

    override fun toSignUp(caller: Fragment) {
        caller.findNavController().navigate(SignInWithEmailFragmentDirections.toSignUp())
    }

    override fun toTermsAndConditions(caller: Fragment) {
        caller.findNavController().navigate(SignUpFragmentDirections.toTerms())
    }
}
