package cz.synetech.feature.signin.data.datasource

import io.reactivex.rxjava3.core.Completable
import java.util.concurrent.TimeUnit

interface EmailSignUpDatasource {
    fun signUp(email: String, password: String): Completable
}

class EmailSignUpDatasourceMocked : EmailSignUpDatasource {

    override fun signUp(email: String, password: String): Completable {
        return Completable.timer(REQUEST_DELAY_S, TimeUnit.SECONDS)
    }

    private companion object {
        private const val REQUEST_DELAY_S = 2L
    }
}
