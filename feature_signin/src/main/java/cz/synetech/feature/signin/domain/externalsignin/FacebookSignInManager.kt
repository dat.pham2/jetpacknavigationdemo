package cz.synetech.feature.signin.domain.externalsignin

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import cz.synetech.base.logger.Logger

class FacebookSignInManager(
    private val callback: ExternalSignInCallback
) : ExternalSignInManager {

    private val callbackManager: CallbackManager by lazy { CallbackManager.Factory.create(); }

    init {
        LoginManager.getInstance().registerCallback(callbackManager,
                object : FacebookCallback<LoginResult> {
                    override fun onSuccess(loginResult: LoginResult) {
                        Logger.d("Success $loginResult")
                        val bundle = Bundle()
                        val accessToken = loginResult.accessToken
                        if (accessToken != null && !accessToken.isExpired) {
                            bundle.putString(FACEBOOK_SIGN_IN_TOKEN_KEY, accessToken.token)
                        }
                        callback.onSuccess(bundle)
                    }

                    override fun onCancel() {
                        callback.onCancel()
                    }

                    override fun onError(exception: FacebookException) {
                        Logger.e(exception)
                        callback.onError(exception)
                    }
                })
    }

    override fun signIn(fragment: Fragment, data: Bundle?) {
        LoginManager.getInstance().logOut()
        val defaultPermissions: Collection<String> = listOf("email")
        var permissions = defaultPermissions
        if (data != null && data.containsKey(PERMISSIONS_KEY)) {
            permissions = data.getStringArray(PERMISSIONS_KEY)?.asList() ?: emptyList()
        }
        LoginManager.getInstance().logInWithReadPermissions(fragment, permissions)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        return if (requestCode == FACEBOOK_SIGN_IN_REQUEST_CODE) {
            callbackManager.onActivityResult(requestCode, resultCode, data)
        } else {
            false
        }
    }

    companion object {
        private const val PERMISSIONS_KEY = "PERMISSIONS"
        const val FACEBOOK_SIGN_IN_TOKEN_KEY = "facebook_token"
        const val FACEBOOK_SIGN_IN_REQUEST_CODE = 1001
    }
}
