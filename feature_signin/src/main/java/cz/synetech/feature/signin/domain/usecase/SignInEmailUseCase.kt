package cz.synetech.feature.signin.domain.usecase

import cz.synetech.feature.signin.data.datasource.EmailSignInDatasource
import cz.synetech.feature.signin.domain.model.SignUpMethod
import cz.synetech.feature.signin.domain.repository.CredentialsRepository
import io.reactivex.rxjava3.core.Completable

interface SignInEmailUseCase {
    fun loginWithEmail(email: String, password: String): Completable
}

class SignInEmailUseCaseImpl(
    private val emailSignInDatasource: EmailSignInDatasource,
    private val credentialsRepository: CredentialsRepository
) : SignInEmailUseCase {
    override fun loginWithEmail(email: String, password: String): Completable {
        return emailSignInDatasource.signIn(email, password).doOnComplete {
            credentialsRepository.storeSignUpInfo(SignUpMethod.EMAIL, email, password)
        }
    }
}
