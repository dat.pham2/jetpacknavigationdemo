package cz.synetech.feature.signin.domain.externalsignin

import android.os.Bundle

interface ExternalSignInCallback {
    fun onSuccess(data: Bundle? = null)
    fun onCancel(data: Bundle? = null)
    fun onError(exception: Exception)
}
