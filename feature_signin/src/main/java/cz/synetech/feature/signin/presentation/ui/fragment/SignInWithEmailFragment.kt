package cz.synetech.feature.signin.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import cz.synetech.base.databinding.ext.setupDataBinding
import cz.synetech.base.livedata.observer.SpecificEventObserver
import cz.synetech.feature.signin.InternalSignInRouter
import cz.synetech.feature.signin.R
import cz.synetech.feature.signin.SignInFeatureModule
import cz.synetech.feature.signin.SignInRouter
import cz.synetech.feature.signin.databinding.FragmentSignInWithEmailBinding
import cz.synetech.feature.signin.presentation.ui.dialog.SignInFailedDialog
import cz.synetech.feature.signin.presentation.viewmodel.SignInWithEmailFragmentViewModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class SignInWithEmailFragment : Fragment() {
    private val router: SignInRouter by inject()
    private val internalRouter: InternalSignInRouter by inject()
    private val fragmentViewModel: SignInWithEmailFragmentViewModel by viewModel()

    init {
        SignInFeatureModule.load()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val dataBinding: FragmentSignInWithEmailBinding = setupDataBinding(R.layout.fragment_sign_in_with_email)
        dataBinding.lifecycleOwner = viewLifecycleOwner
        dataBinding.viewModel = fragmentViewModel
        fragmentViewModel.lifecycleOwner = viewLifecycleOwner
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observe()
    }

    private fun observe() {
        fragmentViewModel.signInEvent.observe(viewLifecycleOwner, SpecificEventObserver { event ->
            when (event) {
                is SignInWithEmailFragmentViewModel.NavigationEvent.SignInSuccessful -> {
                    router.toMain(requireActivity())
                }
                is SignInWithEmailFragmentViewModel.NavigationEvent.SignInError -> {
                    showSignInFailedDialog()
                }
                is SignInWithEmailFragmentViewModel.NavigationEvent.ToForgotPassword -> {
                    router.toForgotPassword(this)
                }
                is SignInWithEmailFragmentViewModel.NavigationEvent.ToSignUp -> {
                    internalRouter.toSignUp(this)
                }
            }
            event.consume()
        })
        fragmentViewModel.onBackPressedEvent.observe(viewLifecycleOwner, SpecificEventObserver {
            activity?.onBackPressed()
        })
    }

    private fun showSignInFailedDialog() {
        context?.let {
            SignInFailedDialog(context = it).show()
        }
    }
}
