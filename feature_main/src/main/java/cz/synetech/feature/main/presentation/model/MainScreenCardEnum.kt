package cz.synetech.feature.main.presentation.model

import androidx.annotation.StringRes
import cz.synetech.feature.main.R

enum class MainScreenCardEnum(@StringRes val title: Int) {
    DynamicScreen(R.string.main_card_dynamicscreen),
    MapScreen(R.string.main_card_map)
}
