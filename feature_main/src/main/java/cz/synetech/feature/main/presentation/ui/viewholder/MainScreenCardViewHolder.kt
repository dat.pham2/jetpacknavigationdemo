package cz.synetech.feature.main.presentation.ui.viewholder

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cz.synetech.base.livedataadapter.model.ItemViewHolder
import cz.synetech.feature.main.presentation.model.MainScreenCardEnum

class MainScreenCardViewHolder(view: View, private val onCardClickedCallback: (card: MainScreenCardEnum) -> Unit) :
    ItemViewHolder<MainScreenCardEnum>(view) {
    private val _card = MutableLiveData<MainScreenCardEnum>()
    val card: LiveData<MainScreenCardEnum> = _card

    override fun setData(model: MainScreenCardEnum?) {
        _card.postValue(model)
    }

    fun onCardClicked() {
        _card.value?.let {
            onCardClickedCallback(it)
        }
    }
}
