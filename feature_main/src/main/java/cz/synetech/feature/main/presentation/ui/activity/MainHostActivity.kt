package cz.synetech.feature.main.presentation.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import cz.synetech.base.databinding.ext.setupDataBinding
import cz.synetech.feature.main.R
import cz.synetech.feature.main.databinding.ActivityMainHostBinding
import kotlinx.android.synthetic.main.activity_main_host.*

class MainHostActivity : AppCompatActivity() {
    private val layoutId: Int = R.layout.activity_main_host
    private lateinit var navigationController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDataBinding<ActivityMainHostBinding>(layoutId)
        setupNavController()
    }

    private fun setupNavController() {
        navigationController = Navigation.findNavController(this, R.id.fr_main_host)
        val appBarConfiguration = AppBarConfiguration(setOf(
            R.id.dashboard_fragment, R.id.dynamic_page_fragment, R.id.map_fragment
        ))
        bottom_navigation_view?.inflateMenu(R.menu.menu_bottom_navigation)
        bottom_navigation_view?.setupWithNavController(navigationController)
        NavigationUI.setupActionBarWithNavController(this, navigationController, appBarConfiguration)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navigationController.navigateUp()
    }
}
