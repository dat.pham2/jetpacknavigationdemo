package cz.synetech.feature.main.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cz.synetech.base.livedata.model.Event
import cz.synetech.base.viewmodel.LifecycleViewModel
import cz.synetech.feature.signin.domain.repository.CredentialsRepository

internal class MainFragmentViewModel(
    private val credentialsRepository: CredentialsRepository
) : LifecycleViewModel() {
    private val _mainViewModelEvents = MutableLiveData<MainViewModelEvent>()
    val mainViewModelEvents: LiveData<MainViewModelEvent> = _mainViewModelEvents

    fun onLogoutClicked() {
        credentialsRepository.logoutUser()
        _mainViewModelEvents.postValue(MainViewModelEvent.OnLogout())
    }

    sealed class MainViewModelEvent : Event() {
        class OnLogout : MainViewModelEvent()
    }
}
