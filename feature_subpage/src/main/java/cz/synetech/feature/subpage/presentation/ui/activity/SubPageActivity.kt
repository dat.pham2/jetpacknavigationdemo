package cz.synetech.feature.subpage.presentation.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import cz.synetech.base.databinding.ext.setupDataBinding
import cz.synetech.feature.subpage.R
import cz.synetech.feature.subpage.databinding.ActivitySubpageBinding

class SubPageActivity : AppCompatActivity() {
    private val layoutId: Int = R.layout.activity_subpage
    private lateinit var navigationController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDataBinding<ActivitySubpageBinding>(layoutId)
        setupNavController()
    }

    private fun setupNavController() {
        navigationController = Navigation.findNavController(this, R.id.fr_subpage_host)
        val appBarConfiguration = AppBarConfiguration(setOf())
        NavigationUI.setupActionBarWithNavController(this, navigationController, appBarConfiguration)
    }

    override fun onSupportNavigateUp(): Boolean {
        val up = navigationController.navigateUp()
        if (!up) {
            super.onBackPressed()
        }
        return up
    }
}
