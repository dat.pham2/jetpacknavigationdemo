package cz.synetech.feature.subpage.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import cz.synetech.base.databinding.ext.setupDataBinding
import cz.synetech.feature.subpage.R
import cz.synetech.feature.subpage.databinding.FragmentPage1Binding

class Page1Fragment : Fragment() {
    private var _dataBinding: FragmentPage1Binding? = null
    private val dataBinding get() = _dataBinding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val binding: FragmentPage1Binding = setupDataBinding(R.layout.fragment_page_1)
        binding.tvP1Label.setOnClickListener {
            findNavController().navigate(Page1FragmentDirections.toPage2(null))
        }
        _dataBinding = binding
        return binding.root
    }
}
