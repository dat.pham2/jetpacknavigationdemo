package cz.synetech.feature.subpage.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import cz.synetech.base.databinding.ext.setupDataBinding
import cz.synetech.feature.subpage.R
import cz.synetech.feature.subpage.databinding.FragmentPage2Binding

class Page2Fragment : Fragment() {
    private var _dataBinding: FragmentPage2Binding? = null
    private val dataBinding get() = _dataBinding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val binding: FragmentPage2Binding = setupDataBinding(R.layout.fragment_page_2)
        _dataBinding = binding
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getString("data")?.let { data ->
            Toast.makeText(requireContext(), "page2Data = $data", Toast.LENGTH_SHORT).show()
        } ?: kotlin.run {
            Toast.makeText(requireContext(), "page2 no data", Toast.LENGTH_SHORT).show()
        }
    }
}
