package cz.synetech.feature.onboarding.domain.usecase

import cz.synetech.feature.onboarding.presentation.model.OnboardingPage

interface GetOnboardingPagesUseCase {
    fun getOnboardingPages(): List<OnboardingPage>
}
