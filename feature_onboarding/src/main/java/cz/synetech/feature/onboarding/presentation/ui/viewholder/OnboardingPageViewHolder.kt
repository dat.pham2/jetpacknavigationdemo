package cz.synetech.feature.onboarding.presentation.ui.viewholder

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import cz.synetech.feature.onboarding.presentation.model.OnboardingPage

class OnboardingPageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val _onboardingPage = MutableLiveData<OnboardingPage>()
    val onboardingPage: LiveData<OnboardingPage> = _onboardingPage

    fun setData(onboardingPage: OnboardingPage) {
        _onboardingPage.postValue(onboardingPage)
    }
}
