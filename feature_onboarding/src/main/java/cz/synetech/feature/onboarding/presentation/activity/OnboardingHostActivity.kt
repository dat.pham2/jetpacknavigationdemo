package cz.synetech.feature.onboarding.presentation.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import cz.synetech.feature.onboarding.presentation.ui.fragment.OnboardingFragment

class OnboardingHostActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportFragmentManager.beginTransaction()
            .replace(android.R.id.content, OnboardingFragment())
            .commit()
    }
}
