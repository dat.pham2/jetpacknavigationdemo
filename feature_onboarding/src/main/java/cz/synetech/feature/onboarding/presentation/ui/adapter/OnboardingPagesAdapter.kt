package cz.synetech.feature.onboarding.presentation.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import cz.synetech.feature.onboarding.R
import cz.synetech.feature.onboarding.databinding.LayoutOnboardingPageBinding
import cz.synetech.feature.onboarding.presentation.model.OnboardingPage
import cz.synetech.feature.onboarding.presentation.ui.viewholder.OnboardingPageViewHolder

class OnboardingPagesAdapter(
    private val lifecycleOwner: LifecycleOwner
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), Observer<List<OnboardingPage>> {

    private var pages: List<OnboardingPage> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int {
        return pages.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = DataBindingUtil.inflate<LayoutOnboardingPageBinding>(
            layoutInflater,
            R.layout.layout_onboarding_page,
            parent,
            false
        )
        itemView.lifecycleOwner = this.lifecycleOwner
        return OnboardingPageViewHolder(itemView.root).apply { itemView.viewHolder = this }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as? OnboardingPageViewHolder)?.setData(pages[position])
    }

    override fun onChanged(pages: List<OnboardingPage>?) {
        this.pages = pages ?: emptyList()
    }
}
