package cz.synetech.feature.onboarding.presentation.model

interface OnboardingPage {
    val textResId: Int
    val imageResId: Int
    val rowType: OnboardingPageType
}
