package cz.synetech.feature.onboarding.domain.repository

interface OnboardingSeenRepository {
    var seen: Boolean
}
