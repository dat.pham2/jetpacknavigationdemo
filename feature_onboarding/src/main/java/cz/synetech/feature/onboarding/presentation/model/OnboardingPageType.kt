package cz.synetech.feature.onboarding.presentation.model

enum class OnboardingPageType {
    PAGE_REGULAR, PAGE_FINAL
}
