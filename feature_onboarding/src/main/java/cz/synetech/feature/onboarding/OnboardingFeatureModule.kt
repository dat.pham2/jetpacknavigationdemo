package cz.synetech.feature.onboarding

import cz.synetech.base.koin.KoinFeatureModule
import cz.synetech.feature.onboarding.presentation.viewmodel.OnboardingFragmentViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

// EXTERNAL DEPENDENCIES, that must be provided by Koin:
// - OnboardingRouter (from this module)

internal object OnboardingFeatureModule : KoinFeatureModule("onboarding") {
    override fun provideKoinModules(): List<Module> {
        return listOf(viewModelModule())
    }

    private fun viewModelModule() = module {
        viewModel {
            OnboardingFragmentViewModel(getOnboardingPagesUseCase = get(), onboardingSeenRepository = get())
        }
    }
}
