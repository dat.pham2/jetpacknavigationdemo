package cz.synetech.feature.onboarding.domain.model

import cz.synetech.base.livedata.model.Event

sealed class OnboardingPageEvent : Event() {
    class SkipEvent : OnboardingPageEvent()
    class ProceedEvent : OnboardingPageEvent()
}
