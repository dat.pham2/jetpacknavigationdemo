package cz.synetech.feature.onboarding.presentation.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class FinalOnboardingPageModel(
    @StringRes override val textResId: Int,
    @DrawableRes override val imageResId: Int,
    override val rowType: OnboardingPageType = OnboardingPageType.PAGE_FINAL
) : OnboardingPage
