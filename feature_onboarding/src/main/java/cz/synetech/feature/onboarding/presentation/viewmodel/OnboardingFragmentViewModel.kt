package cz.synetech.feature.onboarding.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cz.synetech.base.viewmodel.LifecycleViewModel
import cz.synetech.feature.onboarding.domain.model.OnboardingPageEvent
import cz.synetech.feature.onboarding.domain.repository.OnboardingSeenRepository
import cz.synetech.feature.onboarding.domain.usecase.GetOnboardingPagesUseCase
import cz.synetech.feature.onboarding.presentation.model.OnboardingPage
import io.reactivex.rxjava3.disposables.CompositeDisposable

class OnboardingFragmentViewModel(
    private val getOnboardingPagesUseCase: GetOnboardingPagesUseCase,
    private val onboardingSeenRepository: OnboardingSeenRepository
) : LifecycleViewModel() {
    private val _pages = MutableLiveData<List<OnboardingPage>>()
    val pages: LiveData<List<OnboardingPage>> = _pages
    val currentPosition = MutableLiveData<Int>()

    private val _onboardingPageEvent = MutableLiveData<OnboardingPageEvent>()
    val onboardingPageEvent: LiveData<OnboardingPageEvent> = _onboardingPageEvent

    override fun onStartStopObserve(disposeBag: CompositeDisposable) {
        super.onStartStopObserve(disposeBag)
        getOnboardingPagesUseCase.getOnboardingPages().let { onboardingPages ->
            _pages.postValue(onboardingPages)
        }
    }

    fun onboardingSeen() {
        onboardingSeenRepository.seen = true
    }

    fun onSkipClick() {
        _onboardingPageEvent.postValue(OnboardingPageEvent.SkipEvent())
    }

    fun onProceedClick() {
        _onboardingPageEvent.postValue(OnboardingPageEvent.ProceedEvent())
    }
}
