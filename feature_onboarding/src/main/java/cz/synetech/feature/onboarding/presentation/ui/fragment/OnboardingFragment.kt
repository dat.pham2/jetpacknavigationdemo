package cz.synetech.feature.onboarding.presentation.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import cz.synetech.base.databinding.ext.setupDataBinding
import cz.synetech.base.livedata.observer.SpecificEventObserver
import cz.synetech.baseapp.ui.actionbar.ActionBarController
import cz.synetech.feature.onboarding.BR
import cz.synetech.feature.onboarding.OnboardingFeatureModule
import cz.synetech.feature.onboarding.OnboardingRouter
import cz.synetech.feature.onboarding.R
import cz.synetech.feature.onboarding.databinding.FragmentOnboardingBinding
import cz.synetech.feature.onboarding.presentation.ui.adapter.OnboardingPagesAdapter
import cz.synetech.feature.onboarding.presentation.viewmodel.OnboardingFragmentViewModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class OnboardingFragment : Fragment() {
    private val router: OnboardingRouter by inject()
    private val fragmentViewModel: OnboardingFragmentViewModel by viewModel()
    private val adapter: OnboardingPagesAdapter by lazy { OnboardingPagesAdapter(this) }
    private val actionBarController: ActionBarController by inject()
    private var _dataBinding: FragmentOnboardingBinding? = null
    private val dataBinding get() = _dataBinding!!

    init {
        OnboardingFeatureModule.load()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity as? AppCompatActivity)?.let { actionBarController.setVisibility(it, false) }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val binding = setupDataBinding<FragmentOnboardingBinding>(
            R.layout.fragment_onboarding,
            BR.viewModel to fragmentViewModel
        )
        _dataBinding = binding
        binding.lifecycleOwner = viewLifecycleOwner
        fragmentViewModel.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observe()
        setupViewPager()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _dataBinding = null
    }

    private fun observe() {
        fragmentViewModel.pages.observe(viewLifecycleOwner, adapter)
        fragmentViewModel.onboardingPageEvent.observe(viewLifecycleOwner, SpecificEventObserver {
            fragmentViewModel.onboardingSeen()
            router.onOnboardingFinished(requireActivity())
        })
    }

    private fun setupViewPager() {
        dataBinding.vpOnboarding.adapter = adapter
        dataBinding.vpOnboarding.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageSelected(position: Int) {
                fragmentViewModel.currentPosition.postValue(position)
                super.onPageSelected(position)
            }
        })
        (dataBinding.vpOnboarding.getChildAt(0) as? RecyclerView)?.overScrollMode = RecyclerView.OVER_SCROLL_NEVER
    }
}
