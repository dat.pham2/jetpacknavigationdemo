package cz.synetech.feature.onboarding.presentation.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class RegularOnboardingPageModel(
    @StringRes override val textResId: Int,
    @DrawableRes override val imageResId: Int,
    override val rowType: OnboardingPageType = OnboardingPageType.PAGE_REGULAR
) : OnboardingPage
