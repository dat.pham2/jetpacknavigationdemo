package cz.synetech.feature.onboarding

import android.app.Activity

interface OnboardingRouter {
    fun onOnboardingFinished(activity: Activity)
}
