package cz.synetech.feature.forgotten.password

import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController

interface ForgottenPasswordRouter {
    fun navigateBackToSignIn(fragment: Fragment)
}

class ForgottenPasswordRouterImpl : ForgottenPasswordRouter {
    override fun navigateBackToSignIn(fragment: Fragment) {
        fragment.findNavController().navigateUp()
    }
}
