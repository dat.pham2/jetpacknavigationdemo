package cz.synetech.feature.forgotten.password

import cz.synetech.base.koin.KoinFeatureModule
import cz.synetech.feature.forgotten.password.data.datasource.ResetPasswordDatasource
import cz.synetech.feature.forgotten.password.data.datasource.ResetPasswordDatasourceMocked
import cz.synetech.feature.forgotten.password.domain.usecase.ResetPasswordUseCase
import cz.synetech.feature.forgotten.password.domain.usecase.ResetPasswordUseCaseImpl
import cz.synetech.feature.forgotten.password.presentation.viewmodel.ForgottenPasswordFragmentViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module
import org.koin.experimental.builder.factoryBy

internal object ForgottenPasswordFeatureModule : KoinFeatureModule("forgotten_password") {
    override fun provideKoinModules(): List<Module> {
        return listOf(viewModelModule(), dataModule(), domainModule(), navigationModule())
    }

    private fun viewModelModule() = module {
        viewModel {
            ForgottenPasswordFragmentViewModel(get())
        }
    }

    private fun dataModule() = module {
        factoryBy<ResetPasswordDatasource, ResetPasswordDatasourceMocked>()
    }

    private fun domainModule() = module {
        factoryBy<ResetPasswordUseCase, ResetPasswordUseCaseImpl>()
    }

    private fun navigationModule() = module {
        factoryBy<ForgottenPasswordRouter, ForgottenPasswordRouterImpl>()
    }
}
