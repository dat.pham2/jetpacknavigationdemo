package cz.synetech.feature.forgotten.password.data.datasource

import io.reactivex.rxjava3.core.Completable
import java.util.concurrent.TimeUnit

interface ResetPasswordDatasource {
    fun resetPassword(email: String): Completable
}

class ResetPasswordDatasourceMocked : ResetPasswordDatasource {

    override fun resetPassword(email: String): Completable {
        return Completable.timer(REQUEST_DELAY_S, TimeUnit.SECONDS)
    }

    private companion object {
        private const val REQUEST_DELAY_S = 2L
    }
}
