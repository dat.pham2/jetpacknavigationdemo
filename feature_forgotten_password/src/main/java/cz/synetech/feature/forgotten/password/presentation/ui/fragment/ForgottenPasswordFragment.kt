package cz.synetech.feature.forgotten.password.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import cz.synetech.base.databinding.ext.setupDataBinding
import cz.synetech.base.livedata.observer.SpecificEventObserver
import cz.synetech.feature.forgotten.password.BR
import cz.synetech.feature.forgotten.password.ForgottenPasswordFeatureModule
import cz.synetech.feature.forgotten.password.ForgottenPasswordRouter
import cz.synetech.feature.forgotten.password.R
import cz.synetech.feature.forgotten.password.databinding.FragmentForgottenPasswordBinding
import cz.synetech.feature.forgotten.password.presentation.viewmodel.ForgottenPasswordFragmentViewModel
import cz.synetech.feature.forgotten.password.presentation.viewmodel.ForgottenPasswordFragmentViewModel.RequestEvent
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class ForgottenPasswordFragment : Fragment() {
    private val router: ForgottenPasswordRouter by inject()
    private val fragmentViewModel: ForgottenPasswordFragmentViewModel by viewModel()

    init {
        ForgottenPasswordFeatureModule.load()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val binding: FragmentForgottenPasswordBinding = setupDataBinding(
            R.layout.fragment_forgotten_password,
            BR.viewModel to fragmentViewModel
        )
        fragmentViewModel.lifecycleOwner = this
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observe()
    }

    private fun observe() {
        fragmentViewModel.resetPasswordResult.observe(this, SpecificEventObserver(::onRequest))
        fragmentViewModel.onBackPressedEvent.observe(this, SpecificEventObserver {
            activity?.onBackPressed()
        })
    }

    private fun onRequest(event: RequestEvent) {
        context?.let { safeContext ->
            when (event) {
                is RequestEvent.OnPasswordReset -> {
                    MaterialAlertDialogBuilder(safeContext, android.R.style.ThemeOverlay_Material_Dialog_Alert)
                        .setTitle(R.string.forgotten_password_reset_success_dialog_title)
                        .setMessage(R.string.forgotten_password_reset_success_dialog_message)
                        .setPositiveButton(android.R.string.ok) { _, _ ->
                            router.navigateBackToSignIn(this)
                        }
                        .setCancelable(false)
                        .show()
                }
                is RequestEvent.OnResetFailed -> {
                    MaterialAlertDialogBuilder(safeContext, android.R.style.ThemeOverlay_Material_Dialog_Alert)
                        .setTitle(R.string.forgotten_password_reset_error_dialog_title)
                        .setMessage(R.string.forgotten_password_reset_error_dialog_message)
                        .setPositiveButton(android.R.string.ok) { _, _ ->
                            // nothing to do
                        }
                        .setCancelable(true)
                        .show()
                }
            }
        }
    }
}
