package cz.synetech.feature.forgotten.password.domain.usecase

import cz.synetech.feature.forgotten.password.data.datasource.ResetPasswordDatasource
import io.reactivex.rxjava3.core.Completable

interface ResetPasswordUseCase {
    fun resetPassword(email: String): Completable
}

class ResetPasswordUseCaseImpl(
    private val resetPasswordDatasource: ResetPasswordDatasource
) : ResetPasswordUseCase {
    override fun resetPassword(email: String): Completable {
        return resetPasswordDatasource.resetPassword(email)
    }
}
