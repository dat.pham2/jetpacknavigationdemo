package cz.synetech.feature.forgotten.password.presentation.viewmodel

import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cz.synetech.base.livedata.model.Event
import cz.synetech.base.logger.Logger
import cz.synetech.baseapp.viewmodel.BackHandlingViewModel
import cz.synetech.feature.forgotten.password.R
import cz.synetech.feature.forgotten.password.domain.usecase.ResetPasswordUseCase
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers

internal class ForgottenPasswordFragmentViewModel(
    private val resetPasswordUseCase: ResetPasswordUseCase
) : BackHandlingViewModel() {
    private val _resetPasswordResult = MutableLiveData<RequestEvent>()
    val resetPasswordResult: LiveData<RequestEvent> = _resetPasswordResult
    val emailText = MutableLiveData<String>()
    val errorHintResId = MutableLiveData<Int>()
    val isLoading = MutableLiveData<Boolean>(false)

    fun onResetPasswordClicked() {
        processEmail()
    }

    private fun showErrorHint() {
        errorHintResId.postValue(R.string.forgotten_password_email_error_hint)
    }

    private fun hideErrorHint() {
        errorHintResId.postValue(0)
    }

    private fun processEmail() {
        if (isLoading.value == false) {
            if (isEmailValid(emailText.value)) {
                hideErrorHint()
                emailText.value?.let { email ->
                    resetPassword(email.trim())
                }
            } else {
                showErrorHint()
            }
        }
    }

    private fun isEmailValid(email: String?): Boolean {
        return email?.isNotBlank() == true && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun resetPassword(email: String) {
        startStopDisposeBag?.let { bag ->
            resetPasswordUseCase.resetPassword(email)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .doOnSubscribe { isLoading.postValue(true) }
                .doFinally { isLoading.postValue(false) }
                .subscribe({
                    _resetPasswordResult.postValue(RequestEvent.OnPasswordReset())
                }, { error ->
                    Logger.e(error)
                    _resetPasswordResult.postValue(RequestEvent.OnResetFailed())
                }).addTo(bag)
        }
    }

    sealed class RequestEvent : Event() {
        class OnPasswordReset : RequestEvent()
        class OnResetFailed : RequestEvent()
    }
}
