import deps.Libs
import deps.Modules

plugins {
    id(deps.Plugins.Android.library)

    kotlin(deps.Plugins.Kotlin.android)
    kotlin(deps.Plugins.Kotlin.androidExtensions)
    kotlin(deps.Plugins.Kotlin.kapt)
}

android {
    compileSdkVersion(Libs.AndroidSDKVersions.compileSdkVersion)

    defaultConfig {
        minSdkVersion(App.minSdkVersion)
        targetSdkVersion(Libs.AndroidSDKVersions.projectTargetSdkVersion)
        versionCode = 1
        versionName = "1.0"
    }

    android {
        buildFeatures{
            dataBinding = true
        }
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(Libs.Kotlin.stdLib)

    // Project wide modules
    implementation(project(Modules.presentation))
    implementation(project(Modules.domain))

    // Base modules
    implementation(project(Modules.Base.app))
    implementation(project(Modules.Base.logger))
    implementation(project(Modules.Base.rx))
    implementation(project(Modules.Base.version))
    implementation(project(Modules.Base.koin))

    // Feature modules
    implementation(project(Modules.Features.splash))
    implementation(project(Modules.Features.main))
    implementation(project(Modules.Features.onboarding))
}