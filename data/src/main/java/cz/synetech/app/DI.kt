package cz.synetech.app

import cz.synetech.app.data.repository.AndroidStringRepository
import cz.synetech.base.app.repository.StringRepository
import org.koin.dsl.module

internal fun appDataModule() = module {
    factory<StringRepository> { AndroidStringRepository(context = get()) }
}
