package cz.synetech.app.data.repository

import android.content.Context
import cz.synetech.base.app.repository.StringRepository

internal class AndroidStringRepository(private val context: Context) : StringRepository {
    override fun getStringById(resId: Int): String = context.getString(resId)
}
