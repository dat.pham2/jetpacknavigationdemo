package cz.synetech

import cz.synetech.app.appDataModule
import cz.synetech.feature.onboarding.onboardingDataModule
import cz.synetech.feature.signin.signInDataModule
import cz.synetech.feature.splash.splashDataModule

fun dataModules() = listOf(
    appDataModule(),
    splashDataModule(),
    onboardingDataModule(),
    signInDataModule()
)
