package cz.synetech.feature.signin.domain.repository

import cz.synetech.feature.signin.domain.model.SignUpMethod

class CredentialsRepositoryImpl(
    private val storageRepository: StorageRepository
) : CredentialsRepository {

    override fun storeSignUpInfo(signUpMethod: SignUpMethod, signUpInfo: String, password: String?) {
        storageRepository.storeValue(SIGN_UP_INFO_STORAGE_KEY, signUpInfo)
        storageRepository.storeValue(SIGN_UP_METHOD_STORAGE_KEY, signUpMethod.name)
        storageRepository.storeValueEncrypted(PASSWORD_STORAGE_KEY, password)
    }

    override fun isUserSignedIn(): Boolean {
        storageRepository.getValue(SIGN_UP_METHOD_STORAGE_KEY)?.let {
            SignUpMethod.fromString(it)?.let { method ->
                return when (method) {
                    SignUpMethod.EMAIL -> validateEmailSignInInfo()
                    SignUpMethod.FACEBOOK -> validateFacebookSignInInfo()
                    SignUpMethod.GOOGLE -> validateGoogleSignInInfo()
                }
            }
        }
        return false
    }

    override fun logoutUser() {
        storageRepository.storeValue(SIGN_UP_INFO_STORAGE_KEY, null)
        storageRepository.storeValue(SIGN_UP_METHOD_STORAGE_KEY, null)
        storageRepository.storeValueEncrypted(PASSWORD_STORAGE_KEY, null)
    }

    private fun validateEmailSignInInfo(): Boolean {
        return storageRepository.getValue(SIGN_UP_INFO_STORAGE_KEY)?.isNotBlank() == true &&
            storageRepository.getEncryptedValue(PASSWORD_STORAGE_KEY)?.isNotBlank() == true
    }

    private fun validateFacebookSignInInfo(): Boolean {
        return storageRepository.getValue(SIGN_UP_INFO_STORAGE_KEY)?.isNotBlank() == true
    }

    private fun validateGoogleSignInInfo(): Boolean {
        return storageRepository.getValue(SIGN_UP_INFO_STORAGE_KEY)?.isNotBlank() == true
    }

    private companion object {
        private const val SIGN_UP_METHOD_STORAGE_KEY = "SIGN_UP_METHOD_STORAGE_KEY"
        private const val SIGN_UP_INFO_STORAGE_KEY = "SIGN_UP_INFO_STORAGE_KEY"
        private const val PASSWORD_STORAGE_KEY = "PASSWORD_STORAGE_KEY"
    }
}
