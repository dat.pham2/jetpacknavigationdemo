package cz.synetech.feature.signin

import android.content.Context
import android.content.SharedPreferences
import cz.synetech.feature.signin.data.datasource.EncryptedStorageDataSource
import cz.synetech.feature.signin.data.datasource.KeystoreEncryptedStorageDataSourceImpl
import cz.synetech.feature.signin.data.datasource.SharedPrefsStorageDataSourceImpl
import cz.synetech.feature.signin.data.datasource.StorageDataSource
import cz.synetech.feature.signin.domain.repository.CredentialsRepository
import cz.synetech.feature.signin.domain.repository.CredentialsRepositoryImpl
import cz.synetech.feature.signin.domain.repository.StorageRepository
import cz.synetech.feature.signin.domain.repository.StorageRepositoryImpl
import org.koin.dsl.module
import org.koin.experimental.builder.factoryBy

private const val SHARED_PREFS_NAME = "base_app_shared_preferences"

internal fun signInDataModule() = module {
    factoryBy<EncryptedStorageDataSource, KeystoreEncryptedStorageDataSourceImpl>()
    factoryBy<StorageDataSource, SharedPrefsStorageDataSourceImpl>()
    factoryBy<StorageRepository, StorageRepositoryImpl>()
    factoryBy<CredentialsRepository, CredentialsRepositoryImpl>()
    factory<SharedPreferences> {
        (get() as Context).getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE)
    }
}
