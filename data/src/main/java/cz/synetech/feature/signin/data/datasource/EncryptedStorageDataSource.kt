package cz.synetech.feature.signin.data.datasource

import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Base64
import java.security.Key
import java.security.KeyStore
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.spec.GCMParameterSpec

interface EncryptedStorageDataSource {
    fun getValue(key: String): String?
    fun storeValue(key: String, value: String?)
    fun clear()
}

class KeystoreEncryptedStorageDataSourceImpl(private val storageManager: StorageDataSource) :
    EncryptedStorageDataSource {
    private val keyStore: KeyStore by lazy {
        val k = KeyStore.getInstance(AndroidKeyStore)
        k.load(null)
        k
    }

    override fun getValue(key: String): String? {
        storageManager.getString(key)?.let { return decrypt(it) }
        return null
    }

    override fun storeValue(key: String, value: String?) {
        if (value != null) {
            val encrypted = encrypt(value.toByteArray())
            storageManager.storeString(key, encrypted)
        } else {
            storageManager.storeString(key, null)
        }
    }

    override fun clear() {
        storageManager.clear()
    }

    private fun init() {
        if (!keyStore.containsAlias(KEY_ALIAS)) {
            val keyGenerator: KeyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, AndroidKeyStore)
            keyGenerator.init(
                KeyGenParameterSpec.Builder(KEY_ALIAS, KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                    .setRandomizedEncryptionRequired(false)
                    .build()
            )
            keyGenerator.generateKey()
        }
    }

    private fun getSecretKey(): Key {
        return keyStore.getKey(KEY_ALIAS, null)
    }

    private fun encrypt(input: ByteArray): String {
        init()
        val c = Cipher.getInstance(AES_MODE)
        c.init(Cipher.ENCRYPT_MODE, getSecretKey(), GCMParameterSpec(AUTHENTICATION_TAG_LENGTH, FIXED_IV.toByteArray()))
        val encodedBytes = c.doFinal(input)
        return Base64.encodeToString(encodedBytes, Base64.DEFAULT)
    }

    private fun decrypt(encryptedString: String): String {
        init()
        val c = Cipher.getInstance(AES_MODE)
        c.init(Cipher.DECRYPT_MODE, getSecretKey(), GCMParameterSpec(AUTHENTICATION_TAG_LENGTH, FIXED_IV.toByteArray()))
        return c.doFinal(Base64.decode(encryptedString, Base64.DEFAULT)).toString(charset("UTF-8"))
    }

    private companion object {
        private const val AES_MODE: String = "AES/GCM/NoPadding"
        private const val AndroidKeyStore = "AndroidKeyStore"
        private const val KEY_ALIAS = "keyAlias"
        private const val FIXED_IV = "XPa8C1Q2x9bS" // Has to be 12B long
        private const val AUTHENTICATION_TAG_LENGTH = 128
    }
}
