package cz.synetech.feature.signin.domain.repository

import cz.synetech.feature.signin.data.datasource.EncryptedStorageDataSource
import cz.synetech.feature.signin.data.datasource.StorageDataSource

class StorageRepositoryImpl(
    private val storageDataSource: StorageDataSource,
    private val encryptedStorageDataSource: EncryptedStorageDataSource
) : StorageRepository {

    override fun getValue(key: String): String? {
        return storageDataSource.getString(key)
    }

    override fun getEncryptedValue(key: String): String? {
        return encryptedStorageDataSource.getValue(key)
    }

    override fun storeValue(key: String, value: String?) {
        storageDataSource.storeString(key, value)
    }

    override fun storeValueEncrypted(key: String, value: String?) {
        encryptedStorageDataSource.storeValue(key, value)
    }

    override fun clear() {
        storageDataSource.clear()
        encryptedStorageDataSource.clear()
    }
}
