package cz.synetech.feature.signin.data.datasource

import android.content.SharedPreferences

interface StorageDataSource {
    fun storeString(key: String, value: String?)
    fun getString(key: String): String?
    fun storeBoolean(key: String, value: Boolean?)
    fun getBoolean(key: String): Boolean?
    fun storeInteger(key: String, value: Int?)
    fun getInteger(key: String): Int?
    fun storeLong(key: String, value: Long?)
    fun getLong(key: String): Long?
    fun storeStringSet(key: String, value: Set<String>?)
    fun getStringSet(key: String): Set<String>?
    fun clear()
}

class SharedPrefsStorageDataSourceImpl(private val sharedPreferences: SharedPreferences) : StorageDataSource {

    override fun storeString(key: String, value: String?) {
        if (value == null) {
            if (sharedPreferences.contains(key)) {
                sharedPreferences.edit().remove(key).commit()
            }
        } else {
            sharedPreferences.edit().putString(key, value).apply()
        }
    }

    override fun getString(key: String): String? {
        return sharedPreferences
            .getString(key, null)
    }

    override fun storeBoolean(key: String, value: Boolean?) {
        if (value == null) {
            if (sharedPreferences.contains(key)) {
                sharedPreferences.edit().remove(key).commit()
            }
        } else {
            sharedPreferences.edit().putBoolean(key, value).apply()
        }
    }

    override fun getBoolean(key: String): Boolean? {
        return if (sharedPreferences.contains(key)) {
            sharedPreferences.getBoolean(key, false)
        } else {
            null
        }
    }

    override fun storeInteger(key: String, value: Int?) {
        if (value == null) {
            if (sharedPreferences.contains(key)) {
                sharedPreferences.edit().remove(key).commit()
            }
        } else {
            sharedPreferences.edit().putInt(key, value).apply()
        }
    }

    override fun getInteger(key: String): Int? {
        return if (sharedPreferences.contains(key)) {
            sharedPreferences.getInt(key, 0)
        } else {
            null
        }
    }

    override fun storeLong(key: String, value: Long?) {
        if (value == null) {
            if (sharedPreferences.contains(key)) {
                sharedPreferences.edit().remove(key).commit()
            }
        } else {
            sharedPreferences.edit().putLong(key, value).apply()
        }
    }

    override fun getLong(key: String): Long? {
        return if (sharedPreferences.contains(key)) {
            sharedPreferences.getLong(key, 0)
        } else {
            null
        }
    }

    override fun storeStringSet(key: String, value: Set<String>?) {
        if (value == null) {
            if (sharedPreferences.contains(key)) {
                sharedPreferences.edit().remove(key).commit()
            }
        } else {
            sharedPreferences.edit().putStringSet(key, value).apply()
        }
    }

    override fun getStringSet(key: String): Set<String>? {
        return if (sharedPreferences.contains(key)) {
            sharedPreferences.getStringSet(key, emptySet())
        } else {
            null
        }
    }

    override fun clear() {
        sharedPreferences.edit().clear().commit()
    }
}
