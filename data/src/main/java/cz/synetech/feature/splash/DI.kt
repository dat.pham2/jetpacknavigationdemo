package cz.synetech.feature.splash

import android.content.Context
import cz.synetech.base.version.SemanticVersionChecker
import cz.synetech.base.version.VersionChecker
import cz.synetech.feature.splash.data.datasource.*
import cz.synetech.feature.splash.data.datasource.AppBuildCurrentVersionDataSource
import cz.synetech.feature.splash.data.datasource.CurrentVersionDataSource
import cz.synetech.feature.splash.data.datasource.RemoteMinimalVersionDataSource
import cz.synetech.feature.splash.data.repository.IgnoredVersionRepositoryImpl
import cz.synetech.feature.splash.data.repository.VersionRepositoryImpl
import cz.synetech.feature.splash.domain.repository.IgnoredVersionRepository
import cz.synetech.feature.splash.domain.repository.VersionRepository
import org.koin.dsl.module

private const val IGNORED_VERSION_PREFERENCES_NAME = "ignored_version_preferences"

internal fun splashDataModule() = module {
    factory<CurrentVersionDataSource> { AppBuildCurrentVersionDataSource(appBuild = get()) }
    factory<MinimalVersionDataSource> { RemoteMinimalVersionDataSource() }
    factory<LatestVersionDataSource> { RemoteLatestVersionDataSource() }
    factory<VersionRepository> {
        VersionRepositoryImpl(
                currentVersionDataSource = get(),
                minimalVersionDataSource = get(),
                latestVersionDataSource = get()
        )
    }
    factory<VersionChecker> { SemanticVersionChecker() }
    factory<IgnoredVersionRepository> {
        val preferences = get<Context>().getSharedPreferences(IGNORED_VERSION_PREFERENCES_NAME, Context.MODE_PRIVATE)
        IgnoredVersionRepositoryImpl(preferences = preferences)
    }
}
