package cz.synetech.feature.splash.data.datasource

import cz.synetech.base.app.AppBuild
import io.reactivex.rxjava3.core.Single

interface CurrentVersionDataSource {
    fun getCurrentVersion(): Single<String>
}

internal class AppBuildCurrentVersionDataSource(
    private val appBuild: AppBuild
) : CurrentVersionDataSource {
    override fun getCurrentVersion(): Single<String> = Single.just(appBuild.versionName)
}
