package cz.synetech.feature.splash.data.repository

import cz.synetech.feature.splash.data.datasource.CurrentVersionDataSource
import cz.synetech.feature.splash.data.datasource.LatestVersionDataSource
import cz.synetech.feature.splash.data.datasource.MinimalVersionDataSource
import cz.synetech.feature.splash.domain.repository.VersionRepository

internal class VersionRepositoryImpl(
    currentVersionDataSource: CurrentVersionDataSource,
    minimalVersionDataSource: MinimalVersionDataSource,
    latestVersionDataSource: LatestVersionDataSource
) : VersionRepository {
    override val currentVersion = currentVersionDataSource.getCurrentVersion()
    override val minimalVersion = minimalVersionDataSource.getMinimalVersion()
    override val latestVersion = latestVersionDataSource.getLatestVersion()
}
