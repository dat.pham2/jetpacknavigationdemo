package cz.synetech.feature.splash.data.datasource

import io.reactivex.rxjava3.core.Single

interface LatestVersionDataSource {
    fun getLatestVersion(): Single<String>
}

internal class RemoteLatestVersionDataSource : LatestVersionDataSource {
    override fun getLatestVersion(): Single<String> = Single.just("0.2.0")
}
