package cz.synetech.feature.splash.data.model

internal data class RemoteVersionEntity(
    val android: String
)
