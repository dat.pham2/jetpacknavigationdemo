package cz.synetech.feature.splash.data.datasource

import io.reactivex.rxjava3.core.Single

interface MinimalVersionDataSource {
    fun getMinimalVersion(): Single<String>
}

internal class RemoteMinimalVersionDataSource : MinimalVersionDataSource {
    override fun getMinimalVersion(): Single<String> = Single.just("0.1.0")
}
