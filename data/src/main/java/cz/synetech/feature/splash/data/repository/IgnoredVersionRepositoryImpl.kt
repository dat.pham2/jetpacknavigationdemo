package cz.synetech.feature.splash.data.repository

import android.content.SharedPreferences
import cz.synetech.feature.splash.domain.repository.IgnoredVersionRepository

class IgnoredVersionRepositoryImpl(private val preferences: SharedPreferences) : IgnoredVersionRepository {
    override var ignoredVersion: String?
        get() {
            return preferences.getString(IGNORED_VERSION_KEY, null)
        }
        set(value) {
            preferences.edit().putString(IGNORED_VERSION_KEY, value).apply()
        }

    companion object {
        private const val IGNORED_VERSION_KEY = "ignored_version"
    }
}
