package cz.synetech.feature.onboarding.domain.repository

import android.content.SharedPreferences

class OnboardingSeenRepositoryImpl(
    private val preferences: SharedPreferences
) : OnboardingSeenRepository {
    override var seen: Boolean
        get() {
            return preferences.getBoolean(ONBOARDING_SEEN_KEY, false)
        }
        set(value) {
            preferences.edit().putBoolean(ONBOARDING_SEEN_KEY, value).apply()
        }

    companion object {
        private const val ONBOARDING_SEEN_KEY = "onboarding_seen"
    }
}
