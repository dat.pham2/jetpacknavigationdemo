package cz.synetech.feature.onboarding

import android.content.Context
import cz.synetech.feature.onboarding.domain.repository.OnboardingSeenRepository
import cz.synetech.feature.onboarding.domain.repository.OnboardingSeenRepositoryImpl
import cz.synetech.feature.onboarding.domain.usecase.GetOnboardingPagesUseCase
import cz.synetech.feature.onboarding.domain.usecase.GetOnboardingPagesUseCaseImpl
import org.koin.dsl.module

private const val ONBOARDING_SEEN_PREFERENCES_NAME = "onboarding_seen_preferences"

internal fun onboardingDataModule() = module {
    factory<GetOnboardingPagesUseCase> { GetOnboardingPagesUseCaseImpl() }
    single<OnboardingSeenRepository> {
        val preferences = get<Context>().getSharedPreferences(ONBOARDING_SEEN_PREFERENCES_NAME, Context.MODE_PRIVATE)
        OnboardingSeenRepositoryImpl(preferences = preferences)
    }
}
