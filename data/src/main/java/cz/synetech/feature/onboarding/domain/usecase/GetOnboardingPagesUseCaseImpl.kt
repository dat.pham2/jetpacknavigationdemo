package cz.synetech.feature.onboarding.domain.usecase

import cz.synetech.data.R
import cz.synetech.feature.onboarding.presentation.model.FinalOnboardingPageModel
import cz.synetech.feature.onboarding.presentation.model.OnboardingPage
import cz.synetech.feature.onboarding.presentation.model.RegularOnboardingPageModel

class GetOnboardingPagesUseCaseImpl : GetOnboardingPagesUseCase {
    override fun getOnboardingPages(): List<OnboardingPage> {
        return listOf(
                RegularOnboardingPageModel(
                        textResId = R.string.onboarding_page_general_text,
                        imageResId = R.drawable.img_onboarding_template
                ),
                RegularOnboardingPageModel(
                        textResId = R.string.onboarding_page_general_text,
                        imageResId = R.drawable.img_onboarding_template
                ),
                RegularOnboardingPageModel(
                        textResId = R.string.onboarding_page_general_text,
                        imageResId = R.drawable.img_onboarding_template
                ),
                FinalOnboardingPageModel(
                        textResId = R.string.onboarding_page_general_text,
                        imageResId = R.drawable.img_onboarding_template
                )
        )
    }
}
