import com.google.firebase.appdistribution.gradle.AppDistributionExtension
import deps.Libs
import deps.Modules

plugins {
    id(deps.Plugins.Android.application)
    id(deps.Plugins.Google.services)
    id(deps.Plugins.Firebase.crashlytics)
    id(deps.Plugins.Firebase.appDistribution)

    kotlin(deps.Plugins.Kotlin.android)
    kotlin(deps.Plugins.Kotlin.androidExtensions)
    kotlin(deps.Plugins.Kotlin.kapt)

    id(deps.Plugins.Android.X.safeArgs)
}

repositories {
    google()
}

buildscript {
    repositories {
        google()
    }
    dependencies {
        classpath(deps.Libs.Firebase.appDistributionGradle)
    }
}

android {
    // When building Android App Bundles, the splits block is ignored.
    splits {}

    // Instead, use the bundle block to control which types of configuration APKs
    // you want your app bundle to support.
    bundle {
        language {
            // Specifies that the app bundle should not support
            // configuration APKs for language resources. These
            // resources are instead packaged with each base and
            // dynamic feature APK.
            enableSplit = false
        }
        density {
            // This property is set to true by default.
            enableSplit = true
        }
        abi {
            // This property is set to true by default.
            enableSplit = true
        }
    }


    compileSdkVersion(Libs.AndroidSDKVersions.compileSdkVersion)
    buildToolsVersion(Libs.AndroidSDKVersions.buildToolsVersion)

    signingConfigs {
        App.Signing.values().forEach { signing ->
            (
                if (names.contains(signing.id)) {
                    getByName(signing.id)
                } else {
                    create(signing.id)
                }).loadSigning(signing)
        }
    }

    defaultConfig {
        // Application config
        applicationId = App.applicationId
        versionCode = App.version.versionCode()
        versionName = App.version.versionName()
        // Android SDK config
        minSdkVersion(App.minSdkVersion)
        targetSdkVersion(Libs.AndroidSDKVersions.projectTargetSdkVersion)
        // Default signing config & security
        signingConfig = signingConfigs[App.Signing.DEBUG.id]
        proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        // Instrumentation Test config
        testInstrumentationRunner = "cz.synetech.test.KoinTestRunner"
        setTestInstrumentationRunnerArguments(mutableMapOf("clearPackageData" to "true"))
        // Crashlytics config
        setupFirebaseAppDistribution(App.firebaseAppDistribution)
        testBuildType = "release"
    }

    buildTypes {
        getByName("debug") {
            isTestCoverageEnabled = true
            isDebuggable = true
            isMinifyEnabled = false
            setupCustomConfigFields(logs = true, reportCrashes = false)
        }
        getByName("release") {
            isTestCoverageEnabled = false
            isDebuggable = false
            isMinifyEnabled = true
            setupCustomConfigFields(logs = false, reportCrashes = true)
        }
    }

    flavorDimensions("default")
    productFlavors {
        App.Flavor.values().forEach {
            create(it.id) {
                setupFlavour(defaultConfig.applicationId ?: "", it)
            }
        }
        getByName(App.Flavor.PROD.id) {
            signingConfig = signingConfigs[App.Signing.STORE.id] // STORE signing for PROD flavour
        }
    }

    lintOptions {
        isAbortOnError = false
    }

    buildFeatures {
        dataBinding = true
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    implementation(Libs.Kotlin.stdLib)

    // Project wide modules
    implementation(project(Modules.presentation))
    implementation(project(Modules.data))
    implementation(project(Modules.domain))

    // Base modules
    implementation(project(Modules.Base.app))
    implementation(project(Modules.Base.logger))
    implementation(project(Modules.Base.debug))
    implementation(project(Modules.Base.rx))
    implementation(project(Modules.Base.androidextensions))
    implementation(project(Modules.Base.viewmodel))

    // Feature modules
    implementation(project(Modules.Features.splash))
    implementation(project(Modules.Features.main))
    implementation(project(Modules.Features.onboarding))
    implementation(project(Modules.Features.signin))
    implementation(project(Modules.Features.terms))

    // Analytics
    implementation(Libs.Firebase.analytics)

    // Crashlytics
    implementation(Libs.Firebase.crashlytics)

    // Leak Canary
    debugImplementation(Libs.Debug.LeakCanary.android)

    // Testing
    testImplementation(Libs.Test.junit)
    androidTestImplementation(Libs.Test.junitExt)
    androidTestImplementation(Libs.Test.runner)
    androidTestImplementation(Libs.Test.espresso)
    androidTestImplementation(Libs.Test.junitExt)
    androidTestImplementation(Libs.Test.junitExtKtx)
    androidTestImplementation(Libs.Test.rules)

    // Data binding
    implementation(project(Modules.Base.databinding))
    annotationProcessor(Libs.AndroidX.databinding_compiler)
    kapt(Libs.AndroidX.databinding_compiler)

    // Navigation
    implementation(Libs.AndroidArch.Navigation.fragment_ktx)
    implementation(Libs.AndroidArch.Navigation.ui_ktx)

    implementation(Libs.AndroidX.constraintLayout)
    implementation(Libs.Facebook.login)
}

project.setupTranslations(App.translationConfig)

// region Extension functions

fun com.android.build.gradle.internal.dsl.SigningConfig.loadSigning(signing: SigningDefinition) {
    val config = rootProject.loadSingingConfiguration(
        rootProject.file("signing.properties"),
        signing.id.toUpperCase()
    )
    config?.let {
        keyAlias = it.keyAlias
        keyPassword = it.keyPassword
        storeFile = rootProject.file(it.storeFile)
        storePassword = it.storePassword
    }
    if (config == null) {
        throw IllegalStateException("Could not find signing.properties in project root folder.")
    }
}

fun setupFirebaseAppDistribution(config: FirebaseAppDistribution) {
    configure<AppDistributionExtension> {
        when (config.testers) {
            is FileParam -> this.testersFile = (config.testers as FileParam).serializedContent
            is EnumerationParam -> this.testers = (config.testers as EnumerationParam).serializedContent
        }
        when (config.groups) {
            is FileParam -> this.groupsFile = (config.groups as FileParam).serializedContent
            is EnumerationParam -> this.groups = (config.groups as EnumerationParam).serializedContent
        }
        config.releaseNotesFilePath?.let { releaseNotesFile = it }
    }
}

fun com.android.build.gradle.internal.dsl.ProductFlavor.setupFlavour(applicationId: String, flavor: FlavourDefinition) {
    applicationIdSuffix = flavor.applicationIdSuffix()
    versionNameSuffix = flavor.versionNameSuffix()
    addStringRes("applicationId", applicationId + applicationIdSuffix)
}

fun com.android.build.gradle.internal.dsl.BaseFlavor.addStringRes(name: String, value: String) {
    this.resValue("string", name, value)
}

fun com.android.build.gradle.internal.dsl.BuildType.setupCustomConfigFields(logs: Boolean, reportCrashes: Boolean) {
    booleanBuildConfigField("LOGS", logs)
    booleanBuildConfigField("REPORT_CRASHES", reportCrashes)
}

fun com.android.build.gradle.internal.dsl.BuildType.booleanBuildConfigField(name: String, value: Boolean) {
    this.buildConfigField("boolean", name, value.toString())
}

// endregion
