# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/vodamiro/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include parentPath and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

# General config ===========================================================================================================================
-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontskipnonpubliclibraryclassmembers
-dontpreverify
-verbose
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*
-flattenpackagehierarchy
-allowaccessmodification
-keepattributes *Annotation*
-renamesourcefileattribute SourceFile
-keepattributes SourceFile,LineNumberTable
-repackageclasses ''
# In most cases these warnings aren't really helpful. Ignore them.
-ignorewarnings
# Android specific config ==================================================================================================================
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class com.android.vending.licensing.ILicensingService
-dontnote com.android.vending.licensing.ILicensingService
# Explicitly preserve all serialization members. The Serializable interface is only a marker interface, so it wouldn't save them.
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}
# Preserve all native method names and the names of their classes.
-keepclasseswithmembers class * {
    native <methods>;
}
-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}
-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}
# Preserve static fields of inner classes of R classes that might be accessed through introspection.
-keepclassmembers class **.R$* {
  public static <fields>;
}
# Preserve the special static methods that are required in all enumeration classes.
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
-keep public class * {
    public protected *;
}
-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}
# App specific config ======================================================================================================================
##---------------Begin: proguard configuration for Retrofit  ----------
# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform
# Platform used when running on RoboVM on iOS. Will not be used at runtime.
-dontnote retrofit2.Platform$IOS$MainThreadExecutor
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions
# Other
-dontwarn rx.internal.util.**
-dontwarn retrofit2.adapter.rxjava.CompletableHelper$**
-dontwarn okio.**
##---------------End: proguard configuration for Retrofit  ----------
##-------------Begin: proguard configuration for Retrolambda  ----------
-dontwarn java.lang.invoke.*
##---------------End: proguard configuration for Retrolambda  ----------
##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature
# For using GSON @Expose annotation
-keepattributes *Annotation*
# Gson specific classes
-keep class sun.misc.Unsafe { *; }
# Prevent proguard from stripping interface information from TypeAdapterFactory,
# JsonSerializer, JsonDeserializer instances (so they can be used in @JsonAdapter)
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer
##---------------End: proguard configuration for Gson  ----------
##---------------Begin: proguard configuration for Okhttp  ----------
-dontwarn okhttp3.**
-dontwarn okio.**
-dontwarn javax.annotation.**
##---------------End: proguard configuration for Okhttp  ----------
##---------------Begin: proguard configuration for Picasso  ----------
-dontwarn com.squareup.okhttp.**
##---------------End: proguard configuration for Picasso  ----------
##---------------Begin: proguard configuration for module Processor  ----------
-dontwarn javax.annotation.processing.AbstractProcessor
-dontwarn javax.annotation.processing.Filer
-dontwarn javax.annotation.processing.FilerException
-dontwarn javax.annotation.processing.Messager
-dontwarn javax.annotation.processing.ProcessingEnvironment
-dontwarn javax.annotation.processing.RoundEnvironment
-dontwarn javax.lang.model.element.Element
-dontwarn javax.lang.model.element.PackageElement
-dontwarn javax.lang.model.element.TypeElement
-dontwarn javax.lang.model.SourceVersion
-dontwarn javax.lang.model.util.Elements
-dontwarn javax.tools.Diagnostic$Kind
-dontwarn javax.tools.JavaFileObject
##---------------End: proguard configuration for module Processsor  ----------
##---------------Begin: proguard configuration for model classes  ----------
## -keep public class cz.synetech.app.CHANGE.THIS.TO.CORRECT.MODEL.PACKAGE.*
##---------------End: proguard configuration for model classes  ----------