package cz.synetech.test.signin.presentation.ui.fragment

import androidx.fragment.app.Fragment
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import cz.synetech.app.R
import cz.synetech.feature.signin.SignInRouter
import cz.synetech.feature.signin.data.datasource.EmailSignInDatasource
import cz.synetech.feature.signin.presentation.ui.fragment.SignInWithEmailFragment
import cz.synetech.test.KoinFragmentTestRule
import cz.synetech.test.signin.mocked.data.datasource.MockedFailedEmailSignInDatasource
import org.junit.After
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.dsl.module
import java.lang.Thread.sleep

@RunWith(AndroidJUnit4::class)
class SignInWithEmailFragmentTest {
    @get:Rule
    var fragmentTestRule = KoinFragmentTestRule(SignInWithEmailFragment(), { SignInWithEmailFragment() })

    @After
    fun reloadModules() {
        fragmentTestRule.reloadModules()
    }

    @Test
    fun checkTitleDisplayed() {
        onView(withId(R.id.tv_screen_title)).check(matches(isDisplayed()))
    }

    @Test
    fun checkSuccessfulLogin() {
        var onSignedInCalled = false
        fragmentTestRule.injectModule(module {
            factory<SignInRouter>(override = true) {
                object : SignInRouter {
                    private val throwable = IllegalStateException("checkSuccessfulLogin failed")

                    override fun onSignedIn(caller: Fragment) {
                        onSignedInCalled = true
                    }

                    override fun onExternallySignedIn(caller: Fragment) {
                        throw throwable
                    }

                    override fun toForgotPassword(caller: Fragment) {
                        throw throwable
                    }

                    override fun onSignedUp(caller: Fragment) {
                        throw throwable
                    }
                }
            }
        })
        onView(withId(R.id.et_email)).perform(replaceText(VALID_EMAIL))
        onView(withId(R.id.et_password)).perform(replaceText(VALID_PASSWORD))
        onView(withId(R.id.btn_login)).perform(click())
        sleep(DELAY_AFTER_LOGIN)
        assertTrue(onSignedInCalled)
    }

    @Test
    fun checkFailedLogin() {
        fragmentTestRule.injectModule(module {
            factory<EmailSignInDatasource>(override = true) {
                MockedFailedEmailSignInDatasource("mocked error")
            }
        })
        onView(withId(R.id.et_email)).perform(replaceText(VALID_EMAIL))
        onView(withId(R.id.et_password)).perform(replaceText(VALID_PASSWORD))
        onView(withId(R.id.btn_login)).perform(click())

        onView(withText(R.string.signin_error_message)).check(matches(isDisplayed()))
    }

    private companion object {
        private const val VALID_EMAIL = "email@test.cz"
        private const val VALID_PASSWORD = "ahojtestovaciheslO1"
        private const val DELAY_AFTER_LOGIN = 3000L
    }
}
