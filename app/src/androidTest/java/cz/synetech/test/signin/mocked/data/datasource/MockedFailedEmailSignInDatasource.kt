package cz.synetech.test.signin.mocked.data.datasource

import cz.synetech.feature.signin.data.datasource.EmailSignInDatasource
import io.reactivex.rxjava3.core.Completable
import java.io.IOException

class MockedFailedEmailSignInDatasource(private val message: String) : EmailSignInDatasource {
    override fun signIn(email: String, password: String): Completable {
        return Completable.error(IOException(message))
    }
}