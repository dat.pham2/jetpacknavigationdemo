package cz.synetech.test.signin.presentation.ui.fragment

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import cz.synetech.app.R
import cz.synetech.feature.signin.domain.usecase.SignUpUseCase
import cz.synetech.feature.signin.presentation.ui.fragment.SignUpFragment
import cz.synetech.test.KoinFragmentTestRule
import cz.synetech.test.signin.mocked.domain.MockedFailedSignUpUseCase
import org.junit.After
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.dsl.module
import java.lang.Thread.sleep

@RunWith(AndroidJUnit4::class)
class SignUpFragmentTest {
    @get:Rule
    var fragmentTestRule = KoinFragmentTestRule(SignUpFragment(), { SignUpFragment() })

    @After
    fun reloadModules() {
        fragmentTestRule.reloadModules()
    }

    @Test
    fun checkTitleDisplayed() {
        onView(withId(R.id.tv_screen_title)).check(matches(isDisplayed()))
    }

    @Test
    fun checkSuccessfulSignUp() {
        onView(withId(R.id.et_email)).perform(replaceText(VALID_EMAIL))
        onView(withId(R.id.et_password)).perform(replaceText(VALID_PASSWORD))
        onView(withId(R.id.et_confirm_password)).perform(replaceText(VALID_PASSWORD))
        onView(withId(R.id.cb_tnc)).perform(click())
        onView(withId(R.id.btn_sign_up)).perform(click())
        sleep(DELAY_AFTER_SIGNUP)
        onView(withText(R.string.signup_successful_dialog_message)).check(matches(isDisplayed()))
    }

    @Test
    fun checkFailedLogin() {
        fragmentTestRule.injectModule(module {
            factory<SignUpUseCase>(override = true) {
                MockedFailedSignUpUseCase("mocked error")
            }
        })
        onView(withId(R.id.et_email)).perform(replaceText(VALID_EMAIL))
        onView(withId(R.id.et_password)).perform(replaceText(VALID_PASSWORD))
        onView(withId(R.id.et_confirm_password)).perform(replaceText(VALID_PASSWORD))
        onView(withId(R.id.cb_tnc)).perform(click())
        onView(withId(R.id.btn_sign_up)).perform(click())
        onView(withText(R.string.common_networking_error_description)).check(matches(isDisplayed()))
    }

    private companion object {
        private const val VALID_EMAIL = "email@test.cz"
        private const val VALID_PASSWORD = "ahojtestovaciheslO1"
        private const val DELAY_AFTER_SIGNUP = 4000L
    }
}
