package cz.synetech.test.signin.mocked.domain

import cz.synetech.feature.signin.domain.usecase.SignUpUseCase
import io.reactivex.rxjava3.core.Completable
import java.io.IOException

class MockedFailedSignUpUseCase(private val message: String) : SignUpUseCase {
    override fun signUp(email: String, password: String): Completable {
        return Completable.error(IOException(message))
    }
}
