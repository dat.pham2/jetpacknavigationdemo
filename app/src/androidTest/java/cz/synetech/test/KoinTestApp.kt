package cz.synetech.test

import android.app.Application
import cz.synetech.app.Application.Companion.appBuild
import cz.synetech.app.di.appModule
import cz.synetech.dataModules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.module.Module
import java.lang.Thread.sleep

class KoinTestApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin()
    }

    internal fun injectModule(module: Module) {
        loadKoinModules(module)
    }

    internal fun reloadModules() {
        stopKoin()
        startKoin()
    }

    private fun startKoin() {
        startKoin {
            androidLogger()
            androidContext(this@KoinTestApp)
            modules(appModule(appBuild))
            modules(dataModules())
        }
    }
}
