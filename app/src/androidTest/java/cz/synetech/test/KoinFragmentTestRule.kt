package cz.synetech.test

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.test.rule.ActivityTestRule
import org.koin.core.module.Module

class KoinFragmentTestRule(private val fragment: Fragment, private val loadFragmentModules: () -> Unit) :
    ActivityTestRule<FragmentActivity>(FragmentActivity::class.java, true) {

    override fun afterActivityLaunched() {
        super.afterActivityLaunched()
        activity.runOnUiThread {
            val fm = activity.supportFragmentManager
            fm.beginTransaction().replace(
                android.R.id.content,
                fragment
            ).commit()
        }
    }

    fun injectModule(module: Module) {
        KoinModuleHelper.injectModule(module)
    }

    fun reloadModules() {
        KoinModuleHelper.reloadModules()
        loadFragmentModules()
        KoinModuleHelper.clearAppData()
    }
}