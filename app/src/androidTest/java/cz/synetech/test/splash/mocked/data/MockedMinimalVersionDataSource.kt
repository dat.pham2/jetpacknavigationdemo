package cz.synetech.test.splash.mocked.data

import cz.synetech.feature.splash.data.datasource.MinimalVersionDataSource
import io.reactivex.rxjava3.core.Single

internal class MockedMinimalVersionDataSource(private val version: String) : MinimalVersionDataSource {
    override fun getMinimalVersion(): Single<String> = Single.just(version)
}
