package cz.synetech.test.splash.mocked.data

import cz.synetech.feature.splash.data.datasource.LatestVersionDataSource
import io.reactivex.rxjava3.core.Single

internal class MockedLatestVersionDataSource(private val version: String) : LatestVersionDataSource {
    override fun getLatestVersion(): Single<String> = Single.just(version)
}
