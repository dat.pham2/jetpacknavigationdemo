package cz.synetech.test.splash.presentation.ui.activity

import android.app.Activity
import androidx.fragment.app.Fragment
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import cz.synetech.app.R
import cz.synetech.app.navigation.AppRouter
import cz.synetech.feature.splash.data.datasource.CurrentVersionDataSource
import cz.synetech.feature.splash.data.datasource.LatestVersionDataSource
import cz.synetech.feature.splash.data.datasource.MinimalVersionDataSource
import cz.synetech.feature.splash.presentation.ui.activity.SplashActivity
import cz.synetech.test.KoinActivityTestRule
import cz.synetech.test.splash.mocked.data.MockedCurrentVersionDataSource
import cz.synetech.test.splash.mocked.data.MockedFailedMinimalVersionDataSource
import cz.synetech.test.splash.mocked.data.MockedLatestVersionDataSource
import cz.synetech.test.splash.mocked.data.MockedMinimalVersionDataSource
import org.junit.After
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.dsl.module
import java.lang.Thread.sleep

@RunWith(AndroidJUnit4::class)
class SplashActivityTest {

    @get:Rule
    var activityScenarioRule = KoinActivityTestRule(SplashActivity::class.java)

    @After
    fun reloadModules() {
        activityScenarioRule.reloadModules()
    }

    @Test
    fun checkVersionDisplayed() {
        sleep(SPLASH_VERSION_DELAY_MS)
        onView(withId(R.id.tv_splash_version)).check(matches(isDisplayed()))
    }

    @Test
    fun checkUpdateRequiredDialog() {
        activityScenarioRule.injectModule(module {
            factory<MinimalVersionDataSource>(override = true) {
                MockedMinimalVersionDataSource(version = "0.0.1")
            }

            factory<CurrentVersionDataSource>(override = true) {
                MockedCurrentVersionDataSource(version = "0.0.0")
            }


            factory<LatestVersionDataSource>(override = true) {
                MockedLatestVersionDataSource(version = "0.0.1")
            }
        })

        sleep(SPLASH_DIALOG_DELAY_MS)
        onView(withText(R.string.splash_version_outdated_message)).check(matches(isDisplayed()))
    }

    @Test
    fun checkUpdateRecommendedDialog() {
        activityScenarioRule.injectModule(module {
            factory<MinimalVersionDataSource>(override = true) {
                MockedMinimalVersionDataSource(version = "0.0.0")
            }

            factory<CurrentVersionDataSource>(override = true) {
                MockedCurrentVersionDataSource(version = "0.0.0")
            }


            factory<LatestVersionDataSource>(override = true) {
                MockedLatestVersionDataSource(version = "0.0.1")
            }
        })
        sleep(SPLASH_DIALOG_DELAY_MS)
        onView(withText(R.string.splash_dialog_update_message)).check(matches(isDisplayed()))
    }

    @Test
    fun checkVersionCheckFailedDialog() {
        activityScenarioRule.injectModule(module {
            factory<MinimalVersionDataSource>(override = true) {
                MockedFailedMinimalVersionDataSource(message = "mocked error")
            }
        })
        sleep(SPLASH_DIALOG_DELAY_MS)
        onView(withText(R.string.splash_version_checkFailed_message)).check(matches(isDisplayed()))
    }

    @Test
    fun checkOnboardingOpened() {
        var onboardingOpened = false
        activityScenarioRule.injectModule(module {
            factory<AppRouter>(override = true) {
                object : AppRouter {
                    private val throwable = IllegalStateException("test checkSplashProceeds failed")

                    override fun openSignInScreen(caller: Fragment) {
                        throw throwable
                    }

                    override fun openMainScreen(caller: Fragment) {
                        throw throwable
                    }

                    override fun openOnboardingScreen(caller: Fragment) {
                        onboardingOpened = true
                    }

                    override fun onOpenPlayStoreRequest(caller: Activity) {
                        throw throwable
                    }

                    override fun onSplashProceedRequest(caller: Activity) {
                        throw throwable
                    }

                    override fun onOnboardingFinished(caller: Fragment) {
                        throw throwable
                    }

                    override fun onSignedIn(caller: Fragment) {
                        throw throwable
                    }

                    override fun onExternallySignedIn(caller: Fragment) {
                        throw throwable
                    }

                    override fun toForgotPassword(caller: Fragment) {
                        throw throwable
                    }

                    override fun onSignedUp(caller: Fragment) {
                        throw throwable
                    }

                    override fun restartApplication(caller: Activity) {
                        throw throwable
                    }
                }
            }
        })
        sleep(SPLASH_PROCEED_DELAY_MS)
        assert(onboardingOpened)
    }

    private companion object {
        private const val SPLASH_DIALOG_DELAY_MS = 2000L
        private const val SPLASH_PROCEED_DELAY_MS = 5000L
        private const val SPLASH_VERSION_DELAY_MS = 1000L
    }
}
