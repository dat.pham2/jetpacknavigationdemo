package cz.synetech.test.splash.mocked.data

import cz.synetech.feature.splash.data.datasource.CurrentVersionDataSource
import io.reactivex.rxjava3.core.Single

internal class MockedCurrentVersionDataSource(private val version: String) : CurrentVersionDataSource {
    override fun getCurrentVersion(): Single<String> = Single.just(version)
}
