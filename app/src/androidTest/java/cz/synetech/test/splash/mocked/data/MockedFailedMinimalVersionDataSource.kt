package cz.synetech.test.splash.mocked.data

import cz.synetech.feature.splash.data.datasource.MinimalVersionDataSource
import io.reactivex.rxjava3.core.Single
import java.io.IOException

internal class MockedFailedMinimalVersionDataSource(private val message: String) : MinimalVersionDataSource {
    override fun getMinimalVersion(): Single<String> = Single.error(IOException(message))
}
