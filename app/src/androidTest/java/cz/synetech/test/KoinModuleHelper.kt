package cz.synetech.test

import androidx.test.platform.app.InstrumentationRegistry
import cz.synetech.feature.onboarding.domain.repository.OnboardingSeenRepository
import cz.synetech.feature.signin.domain.repository.StorageRepository
import org.koin.android.ext.android.get
import org.koin.core.module.Module

internal object KoinModuleHelper {

    fun injectModule(module: Module) {
        getApplication().injectModule(module)
    }

    fun clearAppData() {
        getApplication().get<StorageRepository>().clear()
        getApplication().get<OnboardingSeenRepository>().seen = false
    }

    fun reloadModules() {
        getApplication().reloadModules()
    }

    private fun getApplication(): KoinTestApp {
        return InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as KoinTestApp
    }
}