package cz.synetech.test

import android.app.Activity
import android.content.Intent
import androidx.test.rule.ActivityTestRule
import org.koin.core.module.Module

class KoinActivityTestRule<A : Activity>(clazz: Class<A>) : ActivityTestRule<A>(clazz, true) {

    fun injectModule(module: Module) {
        finishActivity()
        KoinModuleHelper.injectModule(module)
        launchActivity(Intent())
    }

    fun reloadModules() {
        KoinModuleHelper.reloadModules()
        KoinModuleHelper.clearAppData()
    }
}