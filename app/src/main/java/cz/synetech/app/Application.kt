package cz.synetech.app

import com.google.firebase.crashlytics.FirebaseCrashlytics
import cz.synetech.app.di.appModule
import cz.synetech.base.app.AppBuild
import cz.synetech.base.debug.android.StrictModeSetup
import cz.synetech.base.debug.logging.koin.KoinErrorLogToCrashlyticsLogger
import cz.synetech.base.debug.logging.koin.KoinLogger
import cz.synetech.base.logger.Logger
import cz.synetech.base.rx.GlobalErrorHandler
import cz.synetech.dataModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

@Suppress("ConstantConditionIf")
class Application : android.app.Application() {

    override fun onCreate() {
        super.onCreate()
        setStrictModeForDebug()
        initKoin()
        initCrashlytics()
        initLogger()
        initGlobalErrorHandler()
    }

    private fun setStrictModeForDebug() {
        if (BuildConfig.DEBUG) {
            StrictModeSetup.setup()
        }
    }

    private fun initKoin() {
        startKoin {
            logger(if (BuildConfig.LOGS) KoinLogger() else KoinErrorLogToCrashlyticsLogger())
            androidContext(this@Application)
            modules(dataModules())
            modules(appModule(Application.appBuild))
        }
    }

    private fun initCrashlytics() {
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(BuildConfig.REPORT_CRASHES)
    }

    private fun initLogger() {
        val loggerStrategy = when {
            BuildConfig.LOGS -> Logger.LoggerStrategy.LogEverythingToConsole
            BuildConfig.REPORT_CRASHES -> Logger.LoggerStrategy.LogErrorsToCrashlyticsOnly
            else -> Logger.LoggerStrategy.LogNothing
        }
        Logger.setup(loggerStrategy)
    }

    private fun initGlobalErrorHandler() {
        GlobalErrorHandler.setup()
    }

    companion object {
        internal val appBuild = AppBuild(
                applicationId = BuildConfig.APPLICATION_ID,
                versionCode = BuildConfig.VERSION_CODE,
                versionName = BuildConfig.VERSION_NAME,
                flavorName = BuildConfig.FLAVOR,
                buildType = BuildConfig.BUILD_TYPE,
                debug = BuildConfig.DEBUG,
                logs = BuildConfig.LOGS,
                reportCrashes = BuildConfig.REPORT_CRASHES
        )
    }
}
