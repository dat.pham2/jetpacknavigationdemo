package cz.synetech.app.domain.model

enum class AppEntryPoint {
    ONBOARDING, SIGN_IN, MAIN
}
