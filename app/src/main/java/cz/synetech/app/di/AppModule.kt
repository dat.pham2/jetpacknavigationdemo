package cz.synetech.app.di

import cz.synetech.app.navigation.ActionBarControllerImpl
import cz.synetech.app.navigation.AppRouter
import cz.synetech.app.navigation.AppRouterImpl
import cz.synetech.app.navigation.RestartRouter
import cz.synetech.app.presentation.viewmodel.EntryActivityViewModel
import cz.synetech.base.app.AppBuild
import cz.synetech.baseapp.ui.actionbar.ActionBarController
import cz.synetech.feature.onboarding.OnboardingRouter
import cz.synetech.feature.signin.SignInRouter
import cz.synetech.feature.splash.SplashRouter
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.binds
import org.koin.dsl.module

fun appModule(appBuild: AppBuild) = module {
    factory { appBuild }

    single { AppRouterImpl(appBuild) as AppRouter } binds arrayOf(
        SplashRouter::class, OnboardingRouter::class, SignInRouter::class, RestartRouter::class
    )

    single<ActionBarController> { ActionBarControllerImpl() }

    viewModel { EntryActivityViewModel(onboardingSeenRepository = get(), credentialsRepository = get()) }
}
