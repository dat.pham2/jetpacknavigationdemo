package cz.synetech.app.presentation.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import cz.synetech.app.domain.model.AppEntryPoint
import cz.synetech.app.navigation.AppRouter
import cz.synetech.app.presentation.viewmodel.EntryActivityViewModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class EntryActivity : AppCompatActivity() {
    private val router: AppRouter by inject()
    private val activityViewModel: EntryActivityViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        when (activityViewModel.entryPoint) {
            AppEntryPoint.MAIN -> router.openMainScreen(this)
            AppEntryPoint.SIGN_IN -> router.openSignInScreen(this)
            AppEntryPoint.ONBOARDING -> router.openOnboardingScreen(this)
        }
    }
}
