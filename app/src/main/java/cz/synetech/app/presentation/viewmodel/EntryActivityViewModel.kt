package cz.synetech.app.presentation.viewmodel

import cz.synetech.app.domain.model.AppEntryPoint
import cz.synetech.base.viewmodel.LifecycleViewModel
import cz.synetech.feature.onboarding.domain.repository.OnboardingSeenRepository
import cz.synetech.feature.signin.domain.repository.CredentialsRepository

class EntryActivityViewModel(
    private val onboardingSeenRepository: OnboardingSeenRepository,
    private val credentialsRepository: CredentialsRepository
) : LifecycleViewModel() {
    val entryPoint: AppEntryPoint
        get() {
            return if (credentialsRepository.isUserSignedIn()) {
                AppEntryPoint.MAIN
            } else {
                if (onboardingSeenRepository.seen) {
                    AppEntryPoint.SIGN_IN
                } else {
                    AppEntryPoint.ONBOARDING
                }
            }
        }
}
