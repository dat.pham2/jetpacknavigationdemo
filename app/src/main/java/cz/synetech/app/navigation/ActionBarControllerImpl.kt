package cz.synetech.app.navigation

import androidx.appcompat.app.AppCompatActivity
import cz.synetech.baseapp.ui.actionbar.ActionBarController

class ActionBarControllerImpl : ActionBarController {
    override fun setVisibility(activity: AppCompatActivity, shouldBeVisible: Boolean) {
        if (shouldBeVisible) {
            activity.supportActionBar?.show()
        } else {
            activity.supportActionBar?.hide()
        }
    }

    override fun setTitle(activity: AppCompatActivity, title: String) {
        activity.supportActionBar?.title = title
    }
}
