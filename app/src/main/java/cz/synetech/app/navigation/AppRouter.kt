package cz.synetech.app.navigation

import android.app.Activity
import android.content.Intent
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import cz.synetech.app.presentation.ui.activity.EntryActivity
import cz.synetech.base.androidextensions.activity.openGooglePlayStoreWithAppId
import cz.synetech.base.app.AppBuild
import cz.synetech.feature.main.presentation.ui.activity.MainHostActivity
import cz.synetech.feature.onboarding.OnboardingRouter
import cz.synetech.feature.onboarding.presentation.activity.OnboardingHostActivity
import cz.synetech.feature.signin.SignInRouter
import cz.synetech.feature.signin.presentation.ui.activity.SignInHostActivity
import cz.synetech.feature.signin.presentation.ui.fragment.SignInWithEmailFragmentDirections
import cz.synetech.feature.splash.SplashRouter
import cz.synetech.feature.splash.presentation.ui.activity.SplashActivity

interface AppRouter : SplashRouter, OnboardingRouter, SignInRouter, RestartRouter {
    fun openSignInScreen(activity: Activity)
    fun openMainScreen(activity: Activity)
    fun openOnboardingScreen(activity: Activity)
}

class AppRouterImpl(private val appBuild: AppBuild) : AppRouter {

    override fun openSignInScreen(activity: Activity) {
        val intent = Intent(activity, SignInHostActivity::class.java)
        activity.startActivity(intent)
        activity.finish()
    }

    override fun openMainScreen(activity: Activity) {
        val intent = Intent(activity, MainHostActivity::class.java)
        activity.startActivity(intent)
        activity.finish()
    }

    override fun openOnboardingScreen(activity: Activity) {
        val intent = Intent(activity, OnboardingHostActivity::class.java)
        activity.startActivity(intent)
        activity.finish()
    }

    // region Main

    // endregion

    // region Splash

    override fun onOpenPlayStoreRequest(caller: Activity) {
        caller.openGooglePlayStoreWithAppId(appBuild.applicationId)
    }

    override fun onSplashProceedRequest(caller: Activity) {
        caller.startActivity(Intent(caller, EntryActivity::class.java))
    }

    // endregion

    // region Onboarding

    override fun onOnboardingFinished(activity: Activity) {
        openSignInScreen(activity)
    }

    // endregion

    // region SignIn

    override fun toMain(activity: Activity) {
        openMainScreen(activity)
    }

    override fun toForgotPassword(caller: Fragment) {
        caller.findNavController().navigate(SignInWithEmailFragmentDirections.toForgottenPassword())
    }

    // endregion

    // region Restart

    override fun restartApplication(caller: Activity) {
        val intent = Intent(caller, SplashActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        caller.startActivity(intent)
        caller.finish()
    }

    // endregion
}
