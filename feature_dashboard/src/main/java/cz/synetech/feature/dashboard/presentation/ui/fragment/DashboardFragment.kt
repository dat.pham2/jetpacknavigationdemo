 package cz.synetech.feature.dashboard.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.fragment.findNavController
import cz.synetech.base.databinding.ext.setupDataBinding
import cz.synetech.feature.dashboard.BR
import cz.synetech.feature.dashboard.DashboardFeatureModule
import cz.synetech.feature.dashboard.DashboardRouter
import cz.synetech.feature.dashboard.R
import cz.synetech.feature.dashboard.databinding.FragmentDashboardBinding
import cz.synetech.feature.dashboard.presentation.viewmodel.DashboardFragmentViewModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class DashboardFragment : Fragment() {
    private val router: DashboardRouter by inject()
    private val fragmentViewModel: DashboardFragmentViewModel by viewModel()
    private var _dataBinding: FragmentDashboardBinding? = null
    private val dataBinding get() = _dataBinding!!

    init {
        DashboardFeatureModule.load()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val binding: FragmentDashboardBinding = setupDataBinding(
            R.layout.fragment_dashboard,
            BR.viewModel to fragmentViewModel
        )
        binding.btnToDynamicPage.setOnClickListener {
            val request = NavDeepLinkRequest.Builder
                .fromUri("android-app://jetpacknavigationdemo.synetech.cz/dynamic_page_fragment/?data=DashboardTab".toUri())
                .build()
            findNavController().navigate(request)
        }
        binding.btnToDynamic.setOnClickListener {
            val request = NavDeepLinkRequest.Builder
                .fromUri("android-app://jetpacknavigationdemo.synetech.cz/dynamic_screen_fragment/?data=DashboardTab".toUri())
                .build()
            findNavController().navigate(request)
        }
        binding.btnToSubpage.setOnClickListener {
            findNavController().navigate(DashboardFragmentDirections.toSubPage())
        }
        _dataBinding = binding
        fragmentViewModel.lifecycleOwner = this
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _dataBinding = null
    }
}
