package cz.synetech.feature.dashboard

import cz.synetech.base.koin.KoinFeatureModule
import cz.synetech.feature.dashboard.presentation.viewmodel.DashboardFragmentViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

// EXTERNAL DEPENDENCIES, that must be provided by Koin:
// - DashboardRouter (from this module)

internal object DashboardFeatureModule : KoinFeatureModule("dashboard") {
    override fun provideKoinModules(): List<Module> {
        return listOf(viewModelModule())
    }

    private fun viewModelModule() = module {
        viewModel {
            DashboardFragmentViewModel()
        }
    }
}
