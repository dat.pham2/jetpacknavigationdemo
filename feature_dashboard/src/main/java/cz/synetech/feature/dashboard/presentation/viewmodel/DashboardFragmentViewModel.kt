package cz.synetech.feature.dashboard.presentation.viewmodel

import cz.synetech.base.viewmodel.LifecycleViewModel

internal class DashboardFragmentViewModel : LifecycleViewModel()
