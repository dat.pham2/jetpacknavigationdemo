package cz.synetech.baseapp.ui.view

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.databinding.BindingAdapter
import cz.synetech.presentation.R

private const val MARGIN_HORIZONTAL = 10
private const val MARGIN_VERTICAL = 10
private const val DEFAULT_SIZE = 0
private const val DEFAULT_SELECTED = 0

class PagerIndicatorsView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {
    private val view: View =
        LayoutInflater.from(context).inflate(R.layout.view_pager_indicators, this, true)
    private val layout: LinearLayout by lazy { view.findViewById<LinearLayout>(R.id.ll_pager_indicators) }

    var indexSelected: Int = DEFAULT_SELECTED
        set(value) {
            field = value
            setSelectedIndicator(value)
        }

    var size: Int = DEFAULT_SIZE
        set(value) {
            field = value
            setupSize(value)
        }

    private var selectedDrawable: Drawable? = null
    private var unselectedDrawable: Drawable? = null

    init {
        if (attrs != null) {
            val typedArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.PagerIndicatorsView, 0, 0
            )
            selectedDrawable = typedArray.getDrawable(R.styleable.PagerIndicatorsView_selectedDrawable)
            unselectedDrawable = typedArray.getDrawable(R.styleable.PagerIndicatorsView_unselectedDrawable)
            size = typedArray.getInt(
                R.styleable.PagerIndicatorsView_size,
                DEFAULT_SIZE
            )
            typedArray.recycle()
        } else {
            size = DEFAULT_SIZE
            indexSelected = DEFAULT_SELECTED
        }
    }

    private fun setSelectedIndicator(index: Int) {
        if ((index == 0 && size == 0) || index in 0 until size) {
            for (i in 0 until layout.childCount) {
                val element = layout.getChildAt(i) as ImageView
                if (i == index) {
                    selectedDrawable
                } else {
                    unselectedDrawable
                }.let { element.setImageDrawable(it) }
            }
        } else {
            throw IllegalArgumentException("Selected index out of bounds. (size=$size, index=$index")
        }
    }

    private fun setupSize(size: Int) {
        if (size < 0) {
            throw IllegalArgumentException("Size cannot be negative.")
        }

        layout.removeAllViews()
        for (i in 0 until size) {
            val dot = ImageView(context)
            unselectedDrawable?.let { dot.setImageDrawable(it) }
            val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(
                MARGIN_HORIZONTAL,
                MARGIN_VERTICAL,
                MARGIN_HORIZONTAL,
                MARGIN_VERTICAL
            )
            dot.layoutParams = params
            layout.addView(dot)
        }
        setSelectedIndicator(indexSelected)
    }

    companion object {
        @BindingAdapter("selected")
        @JvmStatic
        fun setSelectedIndex(view: PagerIndicatorsView?, value: Int?) {
            value?.let { index ->
                view?.indexSelected = index
            }
        }
    }
}
