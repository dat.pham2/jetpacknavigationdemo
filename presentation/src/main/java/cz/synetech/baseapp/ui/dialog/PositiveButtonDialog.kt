package cz.synetech.baseapp.ui.dialog

import android.content.Context
import androidx.appcompat.app.AlertDialog
import cz.synetech.presentation.R

open class PositiveButtonDialog(
    private val context: Context,
    private val buttonClickCallback: () -> Unit,
    private val titleResId: Int,
    private val messageResId: Int,
    private val buttonResId: Int,
    private val cancelable: Boolean = false
) {
    private val builder: AlertDialog.Builder by lazy {
        AlertDialog.Builder(context)
            .setCancelable(cancelable)
            .setTitle(titleResId)
            .setMessage(messageResId)
            .setPositiveButton(buttonResId) { _, _ ->
                buttonClickCallback()
            }
    }

    init {
        if (cancelable) {
            builder.setNegativeButton(R.string.common_cancel) { _, _ ->
                // nothing to do
            }
        }
    }

    fun show() {
        builder.create().show()
    }
}
