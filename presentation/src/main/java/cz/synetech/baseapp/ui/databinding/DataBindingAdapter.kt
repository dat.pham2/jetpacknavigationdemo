package cz.synetech.baseapp.ui.databinding

import android.app.Activity
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.annotation.StringRes
import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputLayout
import cz.synetech.base.logger.Logger

@BindingAdapter("onEditorEnterAction")
fun EditText.onEditorEnterAction(callback: (() -> Unit)?) {

    if (callback == null) {
        setOnEditorActionListener(null)
    } else {
        setOnEditorActionListener { _, actionId, event ->
            val imeAction = when (actionId) {
                EditorInfo.IME_ACTION_DONE,
                EditorInfo.IME_ACTION_SEND,
                EditorInfo.IME_ACTION_GO -> true
                else -> false
            }

            val keydownEvent = event?.keyCode == KeyEvent.KEYCODE_ENTER &&
                event.action == KeyEvent.ACTION_UP

            if (imeAction or keydownEvent) {
                callback()
                hideKeyboard(this)
                true
            } else {
                false
            }
        }
    }
}

@SuppressWarnings("TooGenericExceptionCaught")
private fun hideKeyboard(view: View) {
    try {
        (view.context.getSystemService(Activity.INPUT_METHOD_SERVICE) as? InputMethodManager)?.hideSoftInputFromWindow(
            view.windowToken,
            0
        )
    } catch (e: RuntimeException) {
        Logger.e(e)
    }
}

@BindingAdapter("errorTextResId")
fun TextInputLayout.setErrorTextResId(@StringRes errorMessageResId: Int?) {
    error = if (errorMessageResId == null || errorMessageResId == 0) {
        null
    } else {
        context.getString(errorMessageResId)
    }
}
