package cz.synetech.baseapp.ui.actionbar

import androidx.appcompat.app.AppCompatActivity

interface ActionBarController {
    fun setVisibility(activity: AppCompatActivity, shouldBeVisible: Boolean)
    fun setTitle(activity: AppCompatActivity, title: String)
}
