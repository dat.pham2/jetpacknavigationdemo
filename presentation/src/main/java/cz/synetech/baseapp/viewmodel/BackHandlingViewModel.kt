package cz.synetech.baseapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cz.synetech.base.livedata.model.Event
import cz.synetech.base.viewmodel.LifecycleViewModel

open class BackHandlingViewModel : LifecycleViewModel() {
    private val _onBackPressedEvent = MutableLiveData<Event>()
    val onBackPressedEvent: LiveData<Event> = _onBackPressedEvent

    fun onBackPressed() {
        _onBackPressedEvent.postValue(Event())
    }
}
