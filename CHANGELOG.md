# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

*Guiding Principles:*
* Changelogs are for humans, not machines.
* There should be an entry for every single version.
* The same types of changes should be grouped.
* Versions and sections should be linkable.
* The latest version comes first.
* The release date of each version is displayed.

*Types of changes:*
* **Added** for new features.
* **Changed** for changes in existing functionality.
* **Deprecated** for soon-to-be removed features.
* **Removed** for now removed features.
* **Fixed** for any bug fixes.
* **Security** in case of vulnerabilities.

## Unreleased
### Added
- login persistence
- Map with POIs
- Map POI detail
- logout button
- Splash UI tests
- SignIn UI tests
- SignUp UI tests
- Map location handling
- DOC - fork and clone TODO, minor changes
- Android instrumented tests in CI
- External sign in doc
- DOC - fork and clone TODO, translations setup specification
- Updated libraries versions - June 2021

### Changed
- Updated dynamic screens, changed content
- SignUp redirects to main screen, persists credentials
- make databinding required

### Deprecated

### Removed

### Fixed
- inverted password visibility toggle icon
- sign in with email validation
- login persistence for facebook and google
- pre-main screens back navigation
- onboarding placeholder image cutoff
- Remove password check from login with email
- display POI type name
- Password validation
- email validation regex
- SignUp UI
- Map location permission flow
- Databinding fragment leaks
- Removed onboarding buttons background so they become visible

## 0.2.0.0 - 15.7.2020
### Added
- Basic DOC structure
- Main screen cards layout
- Onboarding screens
- Forgotten password UI
- Main screen cards navigation
- Dynamic screen
- Screen with sign-in options
- Sign-in with Facebook
- Sign-in with Google
- Forgotten password mocked datasource
- Forgotten password email validation
- Forgotten password loading and dialogs
- Terms and conditions screen
- Sign in with email UI
- Sign up with email navigation to forgotten password
- Sign up screen
- Sign in with email navigation to forgotten password
- Sign in with email mocked datasource
- Sign in with email (secured) repository
- Sign in with email input validation and loading
- Check and update logic for app's latest version

### Changed
- Updated gradle build tools, gradle wrapper and external libraries
- Fastfile - upload to firebase
- First letter of listID is now capitalized when generating new feature
- createFeatureModule.sh now adds module to settings, is detekt compliant

### Fixed
- dynamic screens title now empty at transition
- Swiping of Onboarding pages no longer disabled on last page

### Removed
- Obsolete logic in Main module
- Obsolete About module

## Multimodule repository fork

### Added
- Note to Fastfile informing that PROD flavour cannot be uploaded to Firebase App Distribution

### Changed
- Gradle version (and plugins) updated
- Launcher icons updated
- Fabric migrated to Firebase
- Updated dependecies (stable crashlytics version)

### Fixed
- Fixed Crashlytics setup when it had cached disabling crash reporting

## 0.0.2 - 2019-11-04
### Changed
- Updated dependencies

## 0.0.1 - 2019-03-04
### Added
- New version of Changelog