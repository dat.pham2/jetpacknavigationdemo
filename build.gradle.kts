// Top-level build file where you can add configuration options "common" to all sub-projects/modules.

import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask

apply {
    plugin("com.github.ben-manes.versions")
}

buildscript {
    repositories {
        google()
        mavenCentral()
    }
    dependencies {
        classpath(deps.Libs.Gradle.buildTools)
        classpath(kotlin("gradle-plugin", version = deps.Libs.Kotlin.version))
        classpath(deps.Libs.AndroidArch.Navigation.safe_args)
        classpath(deps.Libs.Gradle.versions)
        classpath(deps.Libs.GoogleServices.lib)  // Google Services plugin
        classpath(deps.Libs.Firebase.appDistributionGradle)
        classpath(deps.Libs.Firebase.crashlytics_gradle)
        classpath(deps.Libs.Koin.gradle_plugin)
    }
    shared.setupBuildScript(this)
}

allprojects {
    repositories {
        google()
        mavenCentral()
        maven(url = "https://jitpack.io")
    }
    shared.setupAllProjects(this)
}

plugins {
    shared.setupPlugins(this)
}

subprojects {
    shared.setupSubProjects(this)
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}

fun isNonStable(version: String): Boolean {
    val nonStableKeyword = listOf("alpha", "beta", "rc")
    return nonStableKeyword.any { version.toLowerCase().contains(it) }
}

tasks.withType<DependencyUpdatesTask> {
    // disallow unstable versions as upgradable versions from stable versions
    rejectVersionIf {
        isNonStable(candidate.version) && !isNonStable(currentVersion)
    }

    // optional parameters
    checkForGradleUpdate = true
}

apply(plugin = "koin")