package cz.synetech.feature.terms.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import cz.synetech.base.databinding.ext.setupDataBinding
import cz.synetech.base.livedata.observer.SpecificEventObserver
import cz.synetech.feature.terms.BR
import cz.synetech.feature.terms.R
import cz.synetech.feature.terms.TermsFeatureModule
import cz.synetech.feature.terms.databinding.FragmentTermsBinding
import cz.synetech.feature.terms.presentation.viewmodel.TermsFragmentViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class TermsFragment : Fragment() {
    private val fragmentViewModel: TermsFragmentViewModel by viewModel()

    init {
        TermsFeatureModule.load()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val dataBinding: FragmentTermsBinding =
            setupDataBinding(R.layout.fragment_terms, BR.viewModel to fragmentViewModel)
        fragmentViewModel.lifecycleOwner = this
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observe()
    }

    private fun observe() {
        fragmentViewModel.onBackPressedEvent.observe(viewLifecycleOwner, SpecificEventObserver {
            activity?.onBackPressed()
        })
    }
}
