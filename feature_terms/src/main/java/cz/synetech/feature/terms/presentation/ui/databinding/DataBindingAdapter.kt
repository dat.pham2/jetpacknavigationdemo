package cz.synetech.feature.terms.presentation.ui.databinding

import android.webkit.WebView
import androidx.databinding.BindingAdapter

@BindingAdapter("htmlFile")
fun WebView.htmlFile(assetFile: String?) {
    if (assetFile?.isNotBlank() == true) {
        loadUrl(assetFile)
    }
}
