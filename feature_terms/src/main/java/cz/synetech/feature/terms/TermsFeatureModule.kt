package cz.synetech.feature.terms

import cz.synetech.base.koin.KoinFeatureModule
import cz.synetech.feature.terms.presentation.viewmodel.TermsFragmentViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

internal object TermsFeatureModule : KoinFeatureModule("terms") {
    override fun provideKoinModules(): List<Module> {
        return listOf(viewModelModule())
    }

    private fun viewModelModule() = module {
        viewModel {
            TermsFragmentViewModel()
        }
    }
}
