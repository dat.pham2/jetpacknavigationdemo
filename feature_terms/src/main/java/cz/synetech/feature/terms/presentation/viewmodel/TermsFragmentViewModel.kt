package cz.synetech.feature.terms.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cz.synetech.base.livedata.ext.default
import cz.synetech.baseapp.viewmodel.BackHandlingViewModel

internal class TermsFragmentViewModel : BackHandlingViewModel() {
    private val _htmlFile = MutableLiveData<String>().default(TAC_HTML_ASSET_FILE)
    val htmlFile: LiveData<String> = _htmlFile

    private companion object {
        private const val TAC_HTML_ASSET_FILE = "file:///android_asset/tac.html"
    }
}
