import deps.Libs
import deps.Modules

plugins {
    id(deps.Plugins.Android.library)

    kotlin(deps.Plugins.Kotlin.android)
    kotlin(deps.Plugins.Kotlin.androidExtensions)
    kotlin(deps.Plugins.Kotlin.kapt)
    id(deps.Plugins.Android.X.safeArgs)
}

android {
    compileSdkVersion(Libs.AndroidSDKVersions.compileSdkVersion)

    defaultConfig {
        minSdkVersion(App.minSdkVersion)
        targetSdkVersion(Libs.AndroidSDKVersions.projectTargetSdkVersion)
        versionCode = 1
        versionName = "1.0"
    }

    buildFeatures {
        dataBinding = true
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(Libs.Kotlin.stdLib)

    // Common modules
    implementation(project(Modules.presentation))

    // Base modules
    implementation(project(Modules.Base.koin))
    implementation(project(Modules.Base.livedata))
    implementation(project(Modules.Base.viewmodel))
    implementation(project(Modules.Base.rx))

    // Data binding
    implementation(project(Modules.Base.databinding))
    annotationProcessor(Libs.AndroidX.databinding_compiler)
    kapt(Libs.AndroidX.databinding_compiler)

    // Navigation
    implementation(Libs.AndroidArch.Navigation.fragment_ktx)
    implementation(Libs.AndroidArch.Navigation.ui_ktx)

    // Networking
    implementation(Libs.Network.Retrofit.lib)
    implementation(Libs.Network.Retrofit.adapter_rxJava3)
    implementation(Libs.Network.Retrofit.converter_gson)
    implementation(Libs.Network.Retrofit.converter_scalars)
    implementation(Libs.Network.OkHttp.lib)
    implementation(Libs.Network.OkHttp.urlConnection)
    implementation(Libs.Network.OkHttp.logging_interceptor)

    // Other libs
    implementation(Libs.AndroidX.constraintLayout)
    implementation(project(Modules.dynamicscreens))
    implementation(Libs.AndroidX.customTabs)
}

project.setupTranslations(App.translationConfig.copy(listID = "Dynamic_screen"))
