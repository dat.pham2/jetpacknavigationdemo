package cz.synetech.feature.dynamicscreen.data.api

import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET

interface DynamicScreensApi {
    @GET("dynamicscreen")
    fun getDynamicScreenContent(): Single<String>
}
