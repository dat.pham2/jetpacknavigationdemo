package cz.synetech.feature.dynamicscreen.di

import android.content.Context
import cz.synetech.base.koin.KoinFeatureModule
import cz.synetech.dynamicscreens.domain.parser.DSParser
import cz.synetech.dynamicscreens.domain.parser.json.DSJsonParser
import cz.synetech.dynamicscreens.domain.usecase.GetDSScreenUseCase
import cz.synetech.feature.dynamicscreen.data.api.DynamicScreensApi
import cz.synetech.feature.dynamicscreen.data.datasource.LocalDsContentDatasource
import cz.synetech.feature.dynamicscreen.data.datasource.RemoteDSContentDatasource
import cz.synetech.feature.dynamicscreen.domain.datasource.DSContentDatasource
import cz.synetech.feature.dynamicscreen.domain.repository.DSScreenRepository
import cz.synetech.feature.dynamicscreen.domain.repository.DSScreenRepositoryImpl
import cz.synetech.feature.dynamicscreen.domain.usecase.GetDSScreenUseCaseImpl
import cz.synetech.feature.dynamicscreen.presentation.viewmodel.DynamicScreenFragmentViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.module
import org.koin.experimental.builder.factoryBy
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory

private const val DYNAMIC_SCREEN_RETROFIT = "ds_retrofit"
private const val DYNAMIC_SCREEN_URL = "https://stub.bbeight.synetech.cz/"

internal object DynamicScreenFeatureModule : KoinFeatureModule("dynamic_screen") {
    override fun provideKoinModules(): List<Module> = listOf(
        NetworkModule.retrofitDependencies(),
        dynamicScreensDependencies(),
        viewModelModule()
    )

    private fun viewModelModule() = module {
        viewModel { DynamicScreenFragmentViewModel(getDSScreenUseCase = get()) }
    }

    private fun dynamicScreensDependencies() = module {
        single(named(DYNAMIC_SCREEN_RETROFIT)) {
            Retrofit.Builder()
                .baseUrl(DYNAMIC_SCREEN_URL)
                .client(get())
                .addCallAdapterFactory(get(named(RX_JAVA_ADAPTER_NAME)))
                .addConverterFactory(ScalarsConverterFactory.create())
                .build()
        }

        single {
            (get(named(DYNAMIC_SCREEN_RETROFIT)) as Retrofit).create(DynamicScreensApi::class.java)
        }

        factory<DSContentDatasource>(named("remote")) {
            RemoteDSContentDatasource(
                api = get()
            )
        }

        factory<DSContentDatasource>(named("local")) {
            LocalDsContentDatasource(get() as Context)
        }

        factory<DSParser> {
            DSJsonParser()
        }

        factory<DSScreenRepository> {
            DSScreenRepositoryImpl(dsContentDatasource = get(named("local")), dsParser = get())
        }
        factoryBy<GetDSScreenUseCase, GetDSScreenUseCaseImpl>()
    }
}
