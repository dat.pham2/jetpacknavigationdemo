package cz.synetech.feature.dynamicscreen.di

import cz.synetech.base.app.AppBuild
import java.util.concurrent.TimeUnit
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.CallAdapter
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory

private const val TIMEOUT_IN_S = 30L

const val LOGGER_NAME = "logger"
const val RX_JAVA_ADAPTER_NAME = "rxJava3adapter"

object NetworkModule {
    internal fun retrofitDependencies() = module {

        single(named(LOGGER_NAME)) {
            val config: AppBuild = get()
            HttpLoggingInterceptor().apply {
                level = if (config.logs) {
                    HttpLoggingInterceptor.Level.BODY
                } else {
                    HttpLoggingInterceptor.Level.NONE
                }
            } as Interceptor
        }

        single {
            OkHttpClient.Builder()
                .connectTimeout(TIMEOUT_IN_S, TimeUnit.SECONDS)
                .callTimeout(TIMEOUT_IN_S, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_IN_S, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_IN_S, TimeUnit.SECONDS)
                .addInterceptor(get(named(LOGGER_NAME)) as Interceptor)
                .build()
        }

        single(named(RX_JAVA_ADAPTER_NAME)) {
            RxJava3CallAdapterFactory.create() as CallAdapter.Factory
        }
    }
}
