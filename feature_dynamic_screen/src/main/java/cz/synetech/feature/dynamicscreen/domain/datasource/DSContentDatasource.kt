package cz.synetech.feature.dynamicscreen.domain.datasource

import io.reactivex.rxjava3.core.Single

interface DSContentDatasource {
    fun getContentJSON(): Single<String>
}
