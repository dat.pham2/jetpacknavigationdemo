package cz.synetech.feature.dynamicscreen.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.fragment.findNavController
import cz.synetech.base.databinding.ext.setupDataBinding
import cz.synetech.feature.dynamicscreen.R
import cz.synetech.feature.dynamicscreen.databinding.FragmentDynamicPageBinding

class DynamicPageFragment : Fragment() {
    private var _dataBinding: FragmentDynamicPageBinding? = null
    private val dataBinding get() = _dataBinding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val binding: FragmentDynamicPageBinding = setupDataBinding(R.layout.fragment_dynamic_page)
        binding.tvDynamicLabel.setOnClickListener {
            val request = NavDeepLinkRequest.Builder
                .fromUri("android-app://jetpacknavigationdemo.synetech.cz/page_2_fragment/?data=DynamicTab".toUri())
                .build()
            findNavController().navigate(request)
        }
        binding.btnToDynamicScreen.setOnClickListener {
            findNavController().navigate(DynamicPageFragmentDirections.toDynamicScreenFragment("DD"))
        }
        _dataBinding = binding
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getString("data")?.let { data ->
            Toast.makeText(requireContext(), "data = $data", Toast.LENGTH_SHORT).show()
        } ?: kotlin.run {
            Toast.makeText(requireContext(), "no data", Toast.LENGTH_SHORT).show()
        }
    }
}
