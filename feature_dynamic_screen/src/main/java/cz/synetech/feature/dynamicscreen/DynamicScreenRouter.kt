package cz.synetech.feature.dynamicscreen

import androidx.fragment.app.Fragment

interface DynamicScreenRouter {
    fun onExampleUrlReceived(fragment: Fragment, url: String)
}
