package cz.synetech.feature.dynamicscreen.domain.usecase

import cz.synetech.dynamicscreens.domain.model.DSScreen
import cz.synetech.dynamicscreens.domain.usecase.GetDSScreenUseCase
import cz.synetech.feature.dynamicscreen.domain.repository.DSScreenRepository
import io.reactivex.rxjava3.core.Single

class GetDSScreenUseCaseImpl(
    private val dsScreenRepository: DSScreenRepository
) : GetDSScreenUseCase {
    override fun getScreenContent(screenId: String): Single<DSScreen> {
        return dsScreenRepository.getDSRoot().map {
            it.dynamicScreens[screenId]
        }
    }
}
