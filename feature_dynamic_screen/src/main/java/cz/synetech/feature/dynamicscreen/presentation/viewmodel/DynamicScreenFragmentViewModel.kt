package cz.synetech.feature.dynamicscreen.presentation.viewmodel

import cz.synetech.dynamicscreens.domain.usecase.GetDSScreenUseCase

internal class DynamicScreenFragmentViewModel(
    getDSScreenUseCase: GetDSScreenUseCase
) : cz.synetech.dynamicscreens.presentation.ui.viewmodel.DynamicScreenFragmentViewModel(getDSScreenUseCase) {
    fun navigateToScreen(screenId: String) {
        loadDynamicScreenData(screenId)
    }
}
