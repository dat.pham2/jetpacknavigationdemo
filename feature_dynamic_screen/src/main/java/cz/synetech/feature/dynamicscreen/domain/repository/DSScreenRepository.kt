package cz.synetech.feature.dynamicscreen.domain.repository

import cz.synetech.dynamicscreens.domain.model.DSRoot
import cz.synetech.dynamicscreens.domain.parser.DSParser
import cz.synetech.feature.dynamicscreen.domain.datasource.DSContentDatasource
import io.reactivex.rxjava3.core.Single

interface DSScreenRepository {
    fun getDSRoot(): Single<DSRoot>
}

class DSScreenRepositoryImpl(
    private val dsContentDatasource: DSContentDatasource,
    private val dsParser: DSParser
) : DSScreenRepository {
    override fun getDSRoot(): Single<DSRoot> {
        return dsContentDatasource.getContentJSON().map {
            dsParser.parse(it)
        }
    }
}
