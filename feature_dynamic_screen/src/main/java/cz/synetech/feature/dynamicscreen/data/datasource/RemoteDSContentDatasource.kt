package cz.synetech.feature.dynamicscreen.data.datasource

import cz.synetech.feature.dynamicscreen.data.api.DynamicScreensApi
import cz.synetech.feature.dynamicscreen.domain.datasource.DSContentDatasource
import io.reactivex.rxjava3.core.Single

class RemoteDSContentDatasource(
    private val api: DynamicScreensApi
) : DSContentDatasource {

    override fun getContentJSON(): Single<String> {
        return api.getDynamicScreenContent()
    }
}
