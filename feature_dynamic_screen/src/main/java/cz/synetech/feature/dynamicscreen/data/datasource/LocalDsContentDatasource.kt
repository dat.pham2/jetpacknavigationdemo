package cz.synetech.feature.dynamicscreen.data.datasource

import android.content.Context
import cz.synetech.feature.dynamicscreen.R
import cz.synetech.feature.dynamicscreen.domain.datasource.DSContentDatasource
import io.reactivex.rxjava3.core.Single
import java.io.InputStream
import java.nio.charset.Charset

class LocalDsContentDatasource(private val context: Context) : DSContentDatasource {
    override fun getContentJSON(): Single<String> {
        return Single.fromCallable {
            val inputStream = context.resources.openRawResource(R.raw.ds_content)
            inputStream.readTextAndClose()
        }
    }

    private fun InputStream.readTextAndClose(charset: Charset = Charsets.UTF_8): String {
        return this.bufferedReader(charset).use { it.readText() }
    }
}
