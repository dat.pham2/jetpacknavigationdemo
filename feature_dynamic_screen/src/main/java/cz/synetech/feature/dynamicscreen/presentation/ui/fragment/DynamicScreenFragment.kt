package cz.synetech.feature.dynamicscreen.presentation.ui.fragment

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import cz.synetech.baseapp.ui.actionbar.ActionBarController
import cz.synetech.feature.dynamicscreen.di.DynamicScreenFeatureModule
import cz.synetech.feature.dynamicscreen.presentation.viewmodel.DynamicScreenFragmentViewModel
import java.net.URI
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class DynamicScreenFragment : cz.synetech.dynamicscreens.presentation.ui.fragment.DynamicScreenFragment() {
    override var screenId: String? = "main_screen"
    override val fragmentViewModel: cz.synetech.dynamicscreens.presentation.ui.viewmodel.DynamicScreenFragmentViewModel
        by viewModel<DynamicScreenFragmentViewModel>()
    private val actionBarController: ActionBarController by inject()

    init {
        DynamicScreenFeatureModule.load()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getString("data")?.let { data ->
            Toast.makeText(requireContext(), "data = $data", Toast.LENGTH_SHORT).show()
        } ?: kotlin.run {
            Toast.makeText(requireContext(), "no data", Toast.LENGTH_SHORT).show()
        }
    }

    override fun observeData() {
        super.observeData()
        fragmentViewModel.dsContent.observe(this, Observer {
            setupActionBarTitle(it.name)
        })
    }

    override fun handleDynamicScreenAction(action: String) {
        val uri = URI.create(action)
        when (uri.scheme) {
            "dynamic-screen" -> {
                (fragmentViewModel as? DynamicScreenFragmentViewModel)?.navigateToScreen(uri.authority)
            }
            else -> {
                super.handleDynamicScreenAction(action)
            }
        }
    }

    private fun setupActionBarTitle(title: String) {
        (activity as? AppCompatActivity)?.let { actionBarController.setTitle(it, title) }
    }
}
