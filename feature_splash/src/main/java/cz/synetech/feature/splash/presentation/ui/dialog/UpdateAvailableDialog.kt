package cz.synetech.feature.splash.presentation.ui.dialog

import android.content.Context
import android.view.LayoutInflater
import android.widget.CheckBox
import androidx.appcompat.app.AlertDialog
import cz.synetech.feature.splash.R

class UpdateAvailableDialog(
    context: Context,
    private val onOk: () -> Unit,
    private val onCancel: (ignoreUpdate: Boolean) -> Unit
) {
    private val dialog: AlertDialog

    init {
        val builder = AlertDialog.Builder(context, R.style.AlertDialogTheme)
        builder.setTitle(R.string.splash_dialog_update_title)
        val dialogLayout = LayoutInflater.from(context).inflate(R.layout.dialog_update_available, null)

        builder.setView(dialogLayout)
        builder.setPositiveButton(cz.synetech.presentation.R.string.common_ok) { dialog, _ ->
            onOk.invoke()
            dialog.dismiss()
        }
        builder.setNegativeButton(cz.synetech.presentation.R.string.common_cancel) { dialog, _ ->
            val doNotAskAgain: Boolean =
                (dialog as? AlertDialog)?.findViewById<CheckBox>(R.id.cb_ignore_update)?.isChecked ?: false
            onCancel.invoke(doNotAskAgain)
            dialog.dismiss()
        }
        builder.setCancelable(false)
        dialog = builder.create()

        dialogLayout.findViewById<CheckBox>(R.id.cb_ignore_update)?.setOnCheckedChangeListener { _, isChecked ->
            dialog.getButton(AlertDialog.BUTTON_POSITIVE)?.isEnabled = !isChecked
        }
    }

    fun show() {
        dialog.show()
    }
}
