package cz.synetech.feature.splash.presentation.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import cz.synetech.base.livedata.observer.SpecificEventObserver
import cz.synetech.feature.splash.BR
import cz.synetech.feature.splash.R
import cz.synetech.feature.splash.SplashFeatureModule
import cz.synetech.feature.splash.SplashRouter
import cz.synetech.feature.splash.databinding.ActivitySplashBinding
import cz.synetech.feature.splash.presentation.ui.dialog.CheckVersionFailedDialog
import cz.synetech.feature.splash.presentation.ui.dialog.OutdatedVersionDialog
import cz.synetech.feature.splash.presentation.ui.dialog.UpdateAvailableDialog
import cz.synetech.feature.splash.presentation.viewmodel.SplashActivityViewModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashActivity : AppCompatActivity() {
    private val router: SplashRouter by inject()
    private val activityViewModel: SplashActivityViewModel by viewModel()

    init {
        SplashFeatureModule.load()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivitySplashBinding>(
            this, R.layout.activity_splash
        ).apply {
            setVariable(BR.viewModel, activityViewModel)
            lifecycleOwner = this@SplashActivity
        }
        activityViewModel.lifecycleOwner = this
        observe()
    }

    private fun observe() {
        activityViewModel.result.observe(this, SpecificEventObserver(::onSplashResult))
    }

    private fun onSplashResult(event: SplashActivityViewModel.Result) {
        when (event) {
            is SplashActivityViewModel.Result.CanProceed -> {
                router.onSplashProceedRequest(this)
            }
            is SplashActivityViewModel.Result.UpdateRequired -> showUpdateRequiredDialog()
            is SplashActivityViewModel.Result.UpdateAvailable -> showUpdateAvailableDialog()
            is SplashActivityViewModel.Result.CheckFailed -> showCheckFailedDialog()
        }
    }

    private fun showUpdateRequiredDialog() {
        OutdatedVersionDialog(this) { router.onOpenPlayStoreRequest(this) }.show()
    }

    private fun showUpdateAvailableDialog() {
        UpdateAvailableDialog(
            context = this,
            onOk = {
                router.onOpenPlayStoreRequest(this)
                activityViewModel.ignoreCurrentlyLatestVersion()
            },
            onCancel = { ignoreUpdate ->
                router.onSplashProceedRequest(this)
                if (ignoreUpdate) {
                    activityViewModel.ignoreCurrentlyLatestVersion()
                }
            })
            .show()
    }

    private fun showCheckFailedDialog() {
        CheckVersionFailedDialog(this) { activityViewModel.retryVersionCheck() }.show()
    }
}
