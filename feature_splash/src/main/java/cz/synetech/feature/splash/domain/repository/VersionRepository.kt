package cz.synetech.feature.splash.domain.repository

import io.reactivex.rxjava3.core.Single

interface VersionRepository {
    val currentVersion: Single<String>
    val minimalVersion: Single<String>
    val latestVersion: Single<String>
}
