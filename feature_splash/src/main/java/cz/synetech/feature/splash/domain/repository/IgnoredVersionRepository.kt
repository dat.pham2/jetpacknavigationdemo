package cz.synetech.feature.splash.domain.repository

interface IgnoredVersionRepository {
    var ignoredVersion: String?
}
