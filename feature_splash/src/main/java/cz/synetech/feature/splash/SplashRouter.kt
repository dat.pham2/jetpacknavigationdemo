package cz.synetech.feature.splash

import android.app.Activity

interface SplashRouter {
    fun onOpenPlayStoreRequest(caller: Activity)
    fun onSplashProceedRequest(caller: Activity)
}
