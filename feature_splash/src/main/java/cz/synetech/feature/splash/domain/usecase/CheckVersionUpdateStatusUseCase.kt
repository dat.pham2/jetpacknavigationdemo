package cz.synetech.feature.splash.domain.usecase

import cz.synetech.base.version.VersionChecker
import cz.synetech.feature.splash.domain.model.InvalidVersionException
import cz.synetech.feature.splash.domain.model.VersionUpdateStatusEnum
import cz.synetech.feature.splash.domain.repository.VersionRepository
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.functions.Function3

internal interface CheckVersionUpdateStatusUseCase {
    fun checkVersionUpdateStatus(): Single<VersionUpdateStatusEnum>
}

internal class CheckVersionUpdateStatusUseCaseImpl(
    private val repository: VersionRepository,
    private val checker: VersionChecker,
    private val updateRequiredOnError: Boolean
) : CheckVersionUpdateStatusUseCase {
    override fun checkVersionUpdateStatus(): Single<VersionUpdateStatusEnum> {
        return Single.zip(
                repository.currentVersion,
                repository.minimalVersion,
                repository.latestVersion,
                Function3<String, String, String, VersionUpdateStatusEnum> { current, minimal, latest ->
                    when (checker.compareVersions(current, minimal)) {
                        VersionChecker.Result.InvalidInput -> {
                            throw InvalidVersionException()
                        }
                        is VersionChecker.Result.SecondIsNewer -> {
                            VersionUpdateStatusEnum.UPDATE_REQUIRED
                        }
                        else -> {
                            when (checker.compareVersions(current, latest)) {
                                VersionChecker.Result.InvalidInput -> {
                                    throw InvalidVersionException()
                                }
                                is VersionChecker.Result.SecondIsNewer -> {
                                    VersionUpdateStatusEnum.UPDATE_AVAILABLE
                                }
                                else -> VersionUpdateStatusEnum.NO_UPDATE
                            }
                        }
                    }
                })
                .onErrorReturn { e ->
                    if (updateRequiredOnError) {
                        throw e
                    } else {
                        VersionUpdateStatusEnum.NO_UPDATE
                    }
                }
    }
}
