package cz.synetech.feature.splash.domain.model

class InvalidVersionException : IllegalStateException("One of provided versions is invalid.")
