package cz.synetech.feature.splash.presentation.ui.dialog

import android.content.Context
import cz.synetech.baseapp.ui.dialog.PositiveButtonDialog
import cz.synetech.feature.splash.R

internal class CheckVersionFailedDialog(
    context: Context,
    buttonClickCallback: () -> Unit
) : PositiveButtonDialog(
    context = context,
    buttonClickCallback = buttonClickCallback,
    titleResId = R.string.splash_version_checkFailed_title,
    messageResId = R.string.splash_version_checkFailed_message,
    buttonResId = R.string.splash_version_checkFailed_button
)
