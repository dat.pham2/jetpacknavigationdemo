package cz.synetech.feature.splash.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cz.synetech.base.livedata.ext.postValueTo
import cz.synetech.base.livedata.model.Event
import cz.synetech.base.rx.ext.subscribeIgnoringError
import cz.synetech.base.viewmodel.LifecycleViewModel
import cz.synetech.feature.splash.domain.model.VersionUpdateStatusEnum
import cz.synetech.feature.splash.domain.repository.IgnoredVersionRepository
import cz.synetech.feature.splash.domain.repository.VersionRepository
import cz.synetech.feature.splash.domain.usecase.CheckVersionUpdateStatusDelayedUseCase
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers

private const val MIN_SPLASH_SCREEN_DURATION_IN_MS = 2000L

internal class SplashActivityViewModel(
    private val versionRepository: VersionRepository,
    private val checkVersionUpdateStatusDelayedUseCase: CheckVersionUpdateStatusDelayedUseCase,
    private val ignoredVersionRepository: IgnoredVersionRepository
) : LifecycleViewModel() {
    private val _result = MutableLiveData<Result>()
    private val _currentVersion = MutableLiveData<String>()
    private val _latestVersion = MutableLiveData<String>()
    val currentVersion: LiveData<String>
        get() = _currentVersion
    val result: LiveData<Result>
        get() = _result

    override fun onResumePauseObserve(disposeBag: CompositeDisposable) {
        super.onResumePauseObserve(disposeBag)

        versionRepository.currentVersion
                .postValueTo(_currentVersion)
                .observeOn(Schedulers.io())
                .subscribeIgnoringError()
                .addTo(disposeBag)

        versionRepository.latestVersion
                .postValueTo(_latestVersion)
                .observeOn(Schedulers.io())
                .subscribeIgnoringError()
                .addTo(disposeBag)

        performCheck(disposeBag, delayResultInMs = MIN_SPLASH_SCREEN_DURATION_IN_MS)
    }

    fun retryVersionCheck() {
        resumePauseDisposeBag?.let { performCheck(it) }
    }

    private fun performCheck(disposeBag: CompositeDisposable, delayResultInMs: Long = 0) {
        checkVersionUpdateStatusDelayedUseCase.isUpdateRequired(delayResultInMs)
                .map { updateStatus ->
                    when (updateStatus) {
                        VersionUpdateStatusEnum.UPDATE_REQUIRED -> Result.UpdateRequired()
                        VersionUpdateStatusEnum.UPDATE_AVAILABLE -> {
                            if (_latestVersion.value != ignoredVersionRepository.ignoredVersion) {
                                Result.UpdateAvailable()
                            } else {
                                Result.CanProceed()
                            }
                        }
                        else -> Result.CanProceed()
                    }
                }
                .onErrorReturn { Result.CheckFailed() }
                .postValueTo(_result)
                .observeOn(Schedulers.io())
                .subscribeIgnoringError()
                .addTo(disposeBag)
    }

    fun ignoreCurrentlyLatestVersion() {
        _latestVersion.value?.let { ignoredVersionRepository.ignoredVersion = it }
    }

    sealed class Result : Event() {
        class CheckFailed : Result()
        class UpdateRequired : Result()
        class UpdateAvailable : Result()
        class CanProceed : Result()
    }
}
