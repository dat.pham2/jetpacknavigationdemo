package cz.synetech.feature.splash.domain.usecase

import cz.synetech.feature.splash.domain.model.VersionUpdateStatusEnum
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.functions.BiFunction
import java.util.concurrent.TimeUnit

internal interface CheckVersionUpdateStatusDelayedUseCase {
    fun isUpdateRequired(resultDelayInMs: Long): Single<VersionUpdateStatusEnum>
}

internal class CheckVersionUpdateStatusDelayedUseCaseImpl(
    private val checkVersionUpdateStatusUseCase: CheckVersionUpdateStatusUseCase
) : CheckVersionUpdateStatusDelayedUseCase {
    override fun isUpdateRequired(resultDelayInMs: Long): Single<VersionUpdateStatusEnum> {
        return Single.zip(
                checkVersionUpdateStatusUseCase.checkVersionUpdateStatus(),
                Single.just(Unit).delay(resultDelayInMs, TimeUnit.MILLISECONDS),
                BiFunction<VersionUpdateStatusEnum, Unit, VersionUpdateStatusEnum> { updateStatus, _ -> updateStatus }
        )
    }
}
