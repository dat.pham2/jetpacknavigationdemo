package cz.synetech.feature.splash

import cz.synetech.base.koin.KoinFeatureModule
import cz.synetech.feature.splash.domain.usecase.*
import cz.synetech.feature.splash.presentation.viewmodel.SplashActivityViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

// EXTERNAL DEPENDENCIES, that must be provided by Koin:
// - SplashRouter (from this module)
// - VersionRepository (from this module)
// - VersionChecker (from b.version)

object SplashFeatureModule : KoinFeatureModule("splash") {
    override fun provideKoinModules(): List<Module> {
        return listOf(domainModule(updateRequiredOnError = true), viewModelModule())
    }

    private fun domainModule(updateRequiredOnError: Boolean) = module {
        factory<CheckVersionUpdateStatusUseCase> {
            CheckVersionUpdateStatusUseCaseImpl(
                    repository = get(),
                    checker = get(),
                    updateRequiredOnError = updateRequiredOnError
            )
        }
        factory<CheckVersionUpdateStatusDelayedUseCase> {
            CheckVersionUpdateStatusDelayedUseCaseImpl(
                    checkVersionUpdateStatusUseCase = get()
            )
        }
    }

    private fun viewModelModule() = module {
        viewModel {
            SplashActivityViewModel(
                    versionRepository = get(),
                    checkVersionUpdateStatusDelayedUseCase = get(),
                    ignoredVersionRepository = get()
            )
        }
    }
}
