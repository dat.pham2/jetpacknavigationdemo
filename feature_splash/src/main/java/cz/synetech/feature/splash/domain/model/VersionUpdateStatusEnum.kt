package cz.synetech.feature.splash.domain.model

enum class VersionUpdateStatusEnum {
    UPDATE_REQUIRED, UPDATE_AVAILABLE, NO_UPDATE
}
