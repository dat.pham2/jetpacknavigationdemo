#!/bin/bash
if [ "$#" -lt 1 ]; then
    echo "E: No name parameter provided"
    echo ""
    echo "USAGE:"
    echo "createModule.sh [OPTIONAL_PREFIX] FEATURE_MODULE_NAME (for example: main OR v2 main WITH PREFIX)"
    exit 1
fi

if [ "$#" -eq 2 ]; then 
    FEATURE_NAME="$2"
    FEATURE_PREFIX="$1"
    FULL_FEATURE_NAME="${FEATURE_PREFIX}_${FEATURE_NAME}" # v2_main
    PACKAGE_NAME="cz.synetech.feature.$FEATURE_PREFIX.$FEATURE_NAME"
else 
    FEATURE_NAME="$1"
    FEATURE_PREFIX=""
    FULL_FEATURE_NAME="$FEATURE_NAME" # main
    PACKAGE_NAME="cz.synetech.feature.$FEATURE_NAME"
fi
UPPERCASED_FEATURE_NAME="$(tr '[:lower:]' '[:upper:]' <<< ${FEATURE_NAME:0:1})${FEATURE_NAME:1}"
MODULE_NAME="feature_$FULL_FEATURE_NAME"
SRC_DIR="$MODULE_NAME/src/main"
TOP_PACKAGE_DIR="$SRC_DIR/java/cz/synetech/feature/$FEATURE_PREFIX/$FEATURE_NAME"
RES_DIR="$MODULE_NAME/src/main/res"
ANDROID_MANIFEST_FILE="$SRC_DIR/AndroidManifest.xml"
BUILD_GRADLE_FILE="$MODULE_NAME/build.gradle.kts"

mkdir -p "$MODULE_NAME/"
mkdir -p "$RES_DIR/"
mkdir -p "$RES_DIR/layout/"
mkdir -p "$TOP_PACKAGE_DIR/"
mkdir -p "$TOP_PACKAGE_DIR/data/datasource/"
mkdir -p "$TOP_PACKAGE_DIR/data/api/"
mkdir -p "$TOP_PACKAGE_DIR/data/model/"
mkdir -p "$TOP_PACKAGE_DIR/domain/model/"
mkdir -p "$TOP_PACKAGE_DIR/domain/repository/"
mkdir -p "$TOP_PACKAGE_DIR/domain/controller/"
mkdir -p "$TOP_PACKAGE_DIR/domain/usecase/"
mkdir -p "$TOP_PACKAGE_DIR/presentation/viewmodel/"
mkdir -p "$TOP_PACKAGE_DIR/presentation/ui/fragment/"
mkdir -p "$TOP_PACKAGE_DIR/presentation/ui/adapter/"
mkdir -p "$TOP_PACKAGE_DIR/presentation/ui/activity/"
mkdir -p "$TOP_PACKAGE_DIR/presentation/ui/view/"
mkdir -p "$TOP_PACKAGE_DIR/presentation/ui/viewholder/"

echo "<manifest xmlns:android=\"http://schemas.android.com/apk/res/android\"
  xmlns:tools=\"http://schemas.android.com/tools\"
  package=\"$PACKAGE_NAME\">
  <application/>

</manifest>" >> "$ANDROID_MANIFEST_FILE"

echo "import deps.Libs
import deps.Modules

plugins {
    id(deps.Plugins.Android.library)

    kotlin(deps.Plugins.Kotlin.android)
    kotlin(deps.Plugins.Kotlin.androidExtensions)
    kotlin(deps.Plugins.Kotlin.kapt)
}

android {
    compileSdkVersion(Libs.AndroidSDKVersions.compileSdkVersion)

    defaultConfig {
        minSdkVersion(App.minSdkVersion)
        targetSdkVersion(Libs.AndroidSDKVersions.projectTargetSdkVersion)
        versionCode = 1
        versionName = \"1.0\"
    }

    buildFeatures {
        dataBinding = true
    }
}

dependencies {
    implementation(fileTree(mapOf(\"dir\" to \"libs\", \"include\" to listOf(\"*.jar\"))))
    implementation(Libs.Kotlin.stdLib)

    // Common modules
    implementation(project(Modules.presentation))

    // Base modules
    implementation(project(Modules.Base.koin))
    implementation(project(Modules.Base.livedata))
    implementation(project(Modules.Base.viewmodel))
    implementation(project(Modules.Base.rx))

    // Data binding
    implementation(project(Modules.Base.databinding))
    annotationProcessor(Libs.AndroidX.databinding_compiler)
    kapt(Libs.AndroidX.databinding_compiler)

    // Other libs
    implementation(Libs.AndroidX.constraintLayout)
}

project.setupTranslations(App.translationConfig.copy(listID = \"${FULL_FEATURE_NAME^}\"))" >> $BUILD_GRADLE_FILE

echo "package $PACKAGE_NAME

import cz.synetech.base.koin.KoinFeatureModule
import $PACKAGE_NAME.presentation.viewmodel.${UPPERCASED_FEATURE_NAME}FragmentViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

// EXTERNAL DEPENDENCIES, that must be provided by Koin:
// - ${UPPERCASED_FEATURE_NAME}Router (from this module)

internal object ${UPPERCASED_FEATURE_NAME}FeatureModule : KoinFeatureModule(\"$FULL_FEATURE_NAME\") {
    override fun provideKoinModules(): List<Module> {
        return listOf(viewModelModule())
    }

    private fun viewModelModule() = module {
        viewModel {
            ${UPPERCASED_FEATURE_NAME}FragmentViewModel()
        }
    }
}" >> "$TOP_PACKAGE_DIR/${UPPERCASED_FEATURE_NAME}FeatureModule.kt"

echo "package $PACKAGE_NAME

import androidx.fragment.app.Fragment

interface ${UPPERCASED_FEATURE_NAME}Router {
    fun onExampleUrlReceived(fragment: Fragment, url: String)
}" >> "$TOP_PACKAGE_DIR/${UPPERCASED_FEATURE_NAME}Router.kt"

echo " package $PACKAGE_NAME.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import cz.synetech.base.databinding.ext.setupDataBinding
import cz.synetech.base.livedata.observer.SpecificEventObserver
import $PACKAGE_NAME.${UPPERCASED_FEATURE_NAME}FeatureModule
import $PACKAGE_NAME.${UPPERCASED_FEATURE_NAME}Router
import $PACKAGE_NAME.BR
import $PACKAGE_NAME.R
import $PACKAGE_NAME.databinding.Fragment${UPPERCASED_FEATURE_NAME}Binding
import $PACKAGE_NAME.presentation.viewmodel.${UPPERCASED_FEATURE_NAME}FragmentViewModel
import $PACKAGE_NAME.presentation.viewmodel.${UPPERCASED_FEATURE_NAME}FragmentViewModel.RequestEvent
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class ${UPPERCASED_FEATURE_NAME}Fragment : Fragment() {
    private val router: ${UPPERCASED_FEATURE_NAME}Router by inject()
    private val fragmentViewModel: ${UPPERCASED_FEATURE_NAME}FragmentViewModel by viewModel()
    private var _dataBinding: Fragment${UPPERCASED_FEATURE_NAME}Binding? = null
    private val dataBinding get() = _dataBinding!!

    init {
        ${UPPERCASED_FEATURE_NAME}FeatureModule.load()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val binding: Fragment${UPPERCASED_FEATURE_NAME}Binding = setupDataBinding(
            R.layout.fragment_${FEATURE_NAME},
            BR.viewModel to fragmentViewModel
        )
        _dataBinding = binding
        fragmentViewModel.lifecycleOwner = this
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _dataBinding = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observe()
    }

    private fun observe() {
        fragmentViewModel.request.observe(this, SpecificEventObserver(::onRequest))
    }

    private fun onRequest(event: RequestEvent) {
        when (event) {
            is RequestEvent.Example -> router.onExampleUrlReceived(this, event.url)
        }.let {}
    }
}" >> "$TOP_PACKAGE_DIR/presentation/ui/fragment/${UPPERCASED_FEATURE_NAME}Fragment.kt"

echo "package $PACKAGE_NAME.presentation.viewmodel


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cz.synetech.base.livedata.ext.default
import cz.synetech.base.livedata.model.Event
import cz.synetech.base.viewmodel.LifecycleViewModel

internal class ${UPPERCASED_FEATURE_NAME}FragmentViewModel : LifecycleViewModel() {
    private val _request = MutableLiveData<RequestEvent>()
    private val _exampleString = MutableLiveData<String>().default(\"Hello\")
    val request: LiveData<RequestEvent> = _request
    val exampleString: LiveData<String> = _exampleString

    fun onClickMeClick() {
        _exampleString.postValue(\"Bye..\")
    }

    sealed class RequestEvent : Event() {
        class Example(val url: String) : RequestEvent()
    }
}" >> "$TOP_PACKAGE_DIR/presentation/viewmodel/${UPPERCASED_FEATURE_NAME}FragmentViewModel.kt"

echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<layout xmlns:android=\"http://schemas.android.com/apk/res/android\"
  xmlns:app=\"http://schemas.android.com/apk/res-auto\"
  xmlns:tools=\"http://schemas.android.com/tools\">
  <data>

    <variable
      name=\"viewModel\"
      type=\"${PACKAGE_NAME}.presentation.viewmodel.${UPPERCASED_FEATURE_NAME}FragmentViewModel\"/>

  </data>

  <androidx.constraintlayout.widget.ConstraintLayout
    android:layout_width=\"match_parent\"
    android:layout_height=\"match_parent\">

    <TextView
      android:id=\"@+id/tv_example\"
      android:layout_width=\"match_parent\"
      android:layout_height=\"wrap_content\"
      android:gravity=\"center_horizontal\"
      android:text=\"@{ viewModel.exampleString }\"
      app:layout_constraintBottom_toTopOf=\"@+id/btn_example\"
      app:layout_constraintHorizontal_bias=\"0.5\"
      app:layout_constraintVertical_bias=\"0.5\"
      app:layout_constraintLeft_toLeftOf=\"parent\"
      app:layout_constraintRight_toRightOf=\"parent\"
      app:layout_constraintTop_toTopOf=\"parent\"
      app:visibleOrGone=\"@{viewModel.exampleString != null }\"
      tools:text=\"Demo App\" />

    <Button
      android:id=\"@+id/btn_example\"
      android:layout_width=\"match_parent\"
      android:layout_height=\"wrap_content\"
      android:onClick=\"@{ () -> viewModel.onClickMeClick() }\"
      android:text=\"Click Here\"
      app:layout_constraintHorizontal_bias=\"0.5\"
      app:layout_constraintBottom_toBottomOf=\"parent\"
      app:layout_constraintLeft_toLeftOf=\"parent\"
      app:layout_constraintRight_toRightOf=\"parent\"
      app:layout_constraintTop_toTopOf=\"parent\" />
  </androidx.constraintlayout.widget.ConstraintLayout>
</layout>" >> "$RES_DIR/layout/fragment_$FEATURE_NAME.xml"

# add module to dependencies
rm -f buildSrc/src/main/kotlin/Dependencies.kt.bak
mv buildSrc/src/main/kotlin/Dependencies.kt buildSrc/src/main/kotlin/Dependencies.kt.bak
cat buildSrc/src/main/kotlin/Dependencies.kt.bak | awk  '/.*object Features.*/{print;print "        const val '$FULL_FEATURE_NAME' = \":'$MODULE_NAME'\"";next}1' >> buildSrc/src/main/kotlin/Dependencies.kt
rm buildSrc/src/main/kotlin/Dependencies.kt.bak

# add module to settings.gradle
rm -f settings.gradle.kts.bak
mv settings.gradle.kts settings.gradle.kts.bak
cat settings.gradle.kts.bak | awk  '/.*include\(.*/{print;print "    \"'$MODULE_NAME'\",";next}1' >> settings.gradle.kts
rm settings.gradle.kts.bak

echo "Module $FULL_FEATURE_NAME was created."
