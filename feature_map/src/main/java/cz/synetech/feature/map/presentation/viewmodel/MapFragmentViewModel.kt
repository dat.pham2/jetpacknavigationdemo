package cz.synetech.feature.map.presentation.viewmodel

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.VisibleRegion
import cz.synetech.base.livedata.model.DataEvent
import cz.synetech.base.livedata.model.Event
import cz.synetech.base.rx.ext.subscribeLoggingE
import cz.synetech.base.viewmodel.LifecycleViewModel
import cz.synetech.feature.map.domain.repository.CameraPositionRepository
import cz.synetech.feature.map.domain.usecase.GetCameraUpdateByDeviceLocationUseCase
import cz.synetech.feature.map.domain.usecase.GetPOIsUseCase
import cz.synetech.feature.map.presentation.model.CameraPositionPresentationModel
import cz.synetech.feature.map.presentation.model.POIClusterItem
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.BehaviorSubject
import java.util.concurrent.TimeUnit

internal class MapFragmentViewModel(
    private val getPOIsUseCase: GetPOIsUseCase,
    private val cameraPositionRepository: CameraPositionRepository,
    private val getCameraUpdateByDeviceLocationUseCase: GetCameraUpdateByDeviceLocationUseCase
) : LifecycleViewModel() {
    private val _pointsOfInterest = MutableLiveData<DataEvent<Set<POIClusterItem>>>()
    val pointsOfInterest: LiveData<DataEvent<Set<POIClusterItem>>> = _pointsOfInterest
    private var poisDisposable: Disposable? = null

    private val _mapNavigationEvent = MutableLiveData<MapNavigationEvent>()
    val mapNavigationEvent: LiveData<MapNavigationEvent> = _mapNavigationEvent

    private val _cameraPosition = MutableLiveData<CameraPositionPresentationModel>()
    val cameraPosition: LiveData<CameraPositionPresentationModel> = _cameraPosition

    private val _locationPermission = MutableLiveData<Boolean>(false)
    val locationPermission: LiveData<Boolean> = _locationPermission

    private var onCameraMovedObservable = BehaviorSubject.create<VisibleRegion>()

    override fun onStartStopObserve(disposeBag: CompositeDisposable) {
        super.onStartStopObserve(disposeBag)
        onCameraMovedObservable = BehaviorSubject.create<VisibleRegion>()
        onCameraMovedObservable
            .throttleLast(REFRESH_POI_THROTTLE_TIMEOUT_S, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .distinct()
            .subscribeLoggingE { visibleRegion ->
                refreshPOIs(
                    topLeft = visibleRegion.farLeft,
                    topRight = visibleRegion.farRight,
                    bottomLeft = visibleRegion.nearLeft,
                    bottomRight = visibleRegion.nearRight
                )
            }.addTo(disposeBag)
        cameraPositionRepository.getLastCameraPosition()
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribeLoggingE { positionModel ->
                _cameraPosition.postValue(
                    CameraPositionPresentationModel(position = positionModel, isDeviceLocation = false)
                )
            }.addTo(disposeBag)
    }

    fun onCameraMoved(visibleRegion: VisibleRegion?) {
        visibleRegion ?: return
        onCameraMovedObservable.onNext(visibleRegion)
    }

    private fun refreshPOIs(topLeft: LatLng, topRight: LatLng, bottomLeft: LatLng, bottomRight: LatLng) {
        poisDisposable?.dispose()
        poisDisposable = getPOIsUseCase.getPOIs(topLeft, topRight, bottomLeft, bottomRight)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .distinct()
            .subscribeLoggingE { set ->
                _pointsOfInterest.postValue(DataEvent(set.map {
                    POIClusterItem(it)
                }.toSet()))
            }

        poisDisposable?.let {
            startStopDisposeBag?.add(it)
        }
    }

    fun checkLocationPermission(applicationContext: Context?) {
        applicationContext ?: return
        val permissionGranted = ContextCompat.checkSelfPermission(
            applicationContext, Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED

        _locationPermission.value = permissionGranted
        if (!permissionGranted) {
            requestLocationPermission()
        }
    }

    fun notifyLocationPermissionResult(granted: Boolean) {
        _locationPermission.value = granted
    }

    fun notifyLocationEnabled() {
        startStopDisposeBag?.let { bag ->
            getCameraUpdateByDeviceLocationUseCase.getCameraUpdates()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .firstOrError()
                .subscribeLoggingE { positionModel ->
                    _cameraPosition.postValue(
                        CameraPositionPresentationModel(position = positionModel, isDeviceLocation = true)
                    )
                }.addTo(bag)
        }
    }

    fun requestLocationPermission() {
        _mapNavigationEvent.postValue(MapNavigationEvent.OnAllowLocationRequest())
    }

    fun saveCameraPosition(cameraPosition: CameraPosition?) {
        cameraPosition ?: return
        startStopDisposeBag?.let { bag ->
            cameraPositionRepository.saveCameraPosition(cameraPosition)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribeLoggingE()
                .addTo(bag)
        }
    }

    sealed class MapNavigationEvent : Event() {
        class OnAllowLocationRequest : MapNavigationEvent()
    }

    private companion object {
        private const val REFRESH_POI_THROTTLE_TIMEOUT_S = 2L
    }
}
