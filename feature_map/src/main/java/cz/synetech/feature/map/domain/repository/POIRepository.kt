package cz.synetech.feature.map.domain.repository

import cz.synetech.feature.map.data.datasource.local.POILocalDatasource
import cz.synetech.feature.map.data.model.toDomainModel
import cz.synetech.feature.map.domain.model.POIModel
import io.reactivex.rxjava3.core.Single

interface POIRepository {
    fun getPOI(poiId: String): Single<POIModel>
}

class POIRepositoryImpl(
    private val poiLocalDatasource: POILocalDatasource
) : POIRepository {
    override fun getPOI(poiId: String): Single<POIModel> {
        return poiLocalDatasource.getPOIById(poiId).map { it.toDomainModel() }
    }
}
