package cz.synetech.feature.map.data.datasource.local

import cz.synetech.feature.map.data.datasource.local.db.POIDao
import cz.synetech.feature.map.data.model.POIDbModel
import cz.synetech.feature.map.data.model.POIGeohashDbModel
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single

interface POILocalDatasource {
    fun insertPOIs(pois: Set<POIDbModel>): Completable
    fun insertPoiGeohash(geohash: POIGeohashDbModel): Completable
    fun getGeoHashDate(geohash: String): Maybe<POIGeohashDbModel>
    fun getPOIsForGeoHash(geohash: String): Observable<List<POIDbModel>>
    fun getPOIsForGeoHashes(geohashes: Set<String>): Observable<List<POIDbModel>>
    fun deleteAllPOIsForGeoHash(geohash: String): Completable
    fun getPOIById(id: String): Single<POIDbModel>
}

class POILocalDatasourceImpl(private val poiDao: POIDao) :
    POILocalDatasource {
    override fun insertPOIs(pois: Set<POIDbModel>): Completable {
        return poiDao.insertPOIs(pois)
    }

    override fun insertPoiGeohash(geohash: POIGeohashDbModel): Completable {
        return poiDao.insertPoiGeohash(geohash)
    }

    override fun getGeoHashDate(geohash: String): Maybe<POIGeohashDbModel> {
        return poiDao.getGeoHashDate(geohash)
    }

    override fun getPOIsForGeoHash(geohash: String): Observable<List<POIDbModel>> {
        return poiDao.getPOIsForGeoHash(geohash)
    }

    override fun getPOIsForGeoHashes(geohashes: Set<String>): Observable<List<POIDbModel>> {
        return poiDao.getPOIsForGeoHashes(geohashes)
    }

    override fun getPOIById(id: String): Single<POIDbModel> {
        return poiDao.getPOIById(id)
    }

    override fun deleteAllPOIsForGeoHash(geohash: String): Completable {
        return poiDao.deleteAllPOIsForGeoHash(geohash)
    }
}
