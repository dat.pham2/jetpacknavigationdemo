package cz.synetech.feature.map.data.datasource.local.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import cz.synetech.feature.map.data.model.POIDbModel
import cz.synetech.feature.map.data.model.POIGeohashDbModel
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single

@Dao
interface POIDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPOIs(pois: Set<POIDbModel>): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPoiGeohashes(geohashes: Set<POIGeohashDbModel>): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPoiGeohash(geohash: POIGeohashDbModel): Completable

    @Query("SELECT * FROM POIGeohashDbModel WHERE geoHash = :geohash")
    fun getGeoHashDate(geohash: String): Maybe<POIGeohashDbModel>

    @Query("SELECT * FROM POIDbModel WHERE geoHash = :geoHash")
    fun getPOIsForGeoHash(geoHash: String): Observable<List<POIDbModel>>

    @Query("SELECT * FROM POIDbModel WHERE geoHash IN (:geoHashes)")
    fun getPOIsForGeoHashes(geoHashes: Set<String>): Observable<List<POIDbModel>>

    @Query("SELECT * FROM POIDbModel WHERE id = :id")
    fun getPOIById(id: String): Single<POIDbModel>

    @Query("DELETE FROM POIDbModel WHERE geoHash = :geoHash")
    fun deleteAllPOIsForGeoHash(geoHash: String): Completable
}
