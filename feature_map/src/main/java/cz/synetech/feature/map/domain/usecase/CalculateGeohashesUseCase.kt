package cz.synetech.feature.map.domain.usecase

import ch.hsr.geohash.GeoHash
import com.google.android.gms.maps.model.LatLng
import cz.synetech.feature.map.domain.model.MapViewPortModel
import cz.synetech.feature.map.domain.repository.MapPOIRepository.Companion.GEOHASH_PRECISION_STRING_LENGTH
import kotlin.math.abs

interface CalculateGeohashesUseCase {
    fun calculateGeohashes(
        topLeft: LatLng,
        topRight: LatLng,
        bottomLeft: LatLng,
        bottomRight: LatLng,
        stringPrecision: Int = DEFAULT_GEOHASH_STRING_PRECISION
    ): Set<GeoHash>

    private companion object {
        private const val DEFAULT_GEOHASH_STRING_PRECISION = GEOHASH_PRECISION_STRING_LENGTH
    }
}

internal class CalculateGeohashesUseCaseImpl : CalculateGeohashesUseCase {

    private var iterationsCounter = 0

    /**
     * The goal of this function is to calculate all geohashes in given viewport.
     * The algorithm starts at top-left corner and walks east until top-right corner is reached.
     * This results in a full row of geohashes.
     * The row is then walked south, until a bottom geohash is reached.
     * Meridian is checked for by detectLongitudeDivider
     */
    override fun calculateGeohashes(
        topLeft: LatLng,
        topRight: LatLng,
        bottomLeft: LatLng,
        bottomRight: LatLng,
        stringPrecision: Int
    ): Set<GeoHash> {
        iterationsCounter = 0
        val viewPorts = boxPoints(topLeft, topRight, bottomLeft, bottomRight)
        return viewPorts.map { walkViewPort(it, stringPrecision) }.flatten().toSet()
    }

    private fun walkViewPort(viewPort: MapViewPortModel, stringPrecision: Int): Set<GeoHash> {
        val topLeftGeohash =
            GeoHash.withCharacterPrecision(viewPort.topLeft.latitude, viewPort.topLeft.longitude, stringPrecision)
        val topRightGeohash =
            GeoHash.withCharacterPrecision(viewPort.topRight.latitude, viewPort.topRight.longitude, stringPrecision)
        val bottomLeftGeohash =
            GeoHash.withCharacterPrecision(viewPort.bottomLeft.latitude, viewPort.bottomLeft.longitude, stringPrecision)
        val topRowGeohashes = walkEastUntil(topLeftGeohash, topRightGeohash)
        val allGeohashes = walkSouthUntil(topRowGeohashes, bottomLeftGeohash)
        return if (allGeohashes.size > MAX_GEOHASHES_FAILSAFE) {
            allGeohashes.toList().subList(0, MAX_GEOHASHES_FAILSAFE).toSet()
        } else {
            allGeohashes
        }
    }

    /**
     * Helper function to account for map rotation.
     */
    private fun boxPoints(
        topLeft: LatLng,
        topRight: LatLng,
        bottomLeft: LatLng,
        bottomRight: LatLng
    ): List<MapViewPortModel> {
        val latLngList = listOf(topLeft, topRight, bottomLeft, bottomRight)
        val minLat = latLngList.minByOrNull { it.latitude }?.latitude ?: bottomLeft.latitude
        val minLng = latLngList.minByOrNull { it.longitude }?.longitude ?: topLeft.longitude
        val maxLat = latLngList.minByOrNull { it.latitude }?.latitude ?: topLeft.latitude
        val maxLng = latLngList.minByOrNull { it.longitude }?.longitude ?: topRight.longitude
        return if (detectLongitudeDivider(minLng, maxLng)) {
            divideViewPortByLongitude(minLat, minLng, maxLat, maxLng)
        } else {
            val newTopLeft = LatLng(maxLat, minLng)
            val newTopRight = LatLng(maxLat, maxLng)
            val newBottomLeft = LatLng(minLat, minLng)
            listOf(MapViewPortModel(topLeft = newTopLeft, topRight = newTopRight, bottomLeft = newBottomLeft))
        }
    }

    /**
     * Detect secondary meridian - hemisphere divider
     */
    private fun detectLongitudeDivider(minLng: Double, maxLng: Double): Boolean {
        return abs(minLng) + abs(maxLng) > MAX_LONGITUDE && ((minLng < 0 && maxLng > 0))
    }

    /**
     * Divide viewport into two in different hemispheres
     */
    private fun divideViewPortByLongitude(
        minLat: Double,
        minLng: Double,
        maxLat: Double,
        maxLng: Double
    ): List<MapViewPortModel> {
        val positiveTopLeft = LatLng(maxLat, maxLng)
        val positiveTopRight = LatLng(maxLat, MAX_LONGITUDE)
        val positiveBottomLeft = LatLng(minLat, maxLng)

        val positiveViewPortModel =
            MapViewPortModel(topLeft = positiveTopLeft, topRight = positiveTopRight, bottomLeft = positiveBottomLeft)

        val negativeTopLeft = LatLng(maxLat, -MAX_LONGITUDE)
        val negativeTopRight = LatLng(maxLat, minLng)
        val negativeBottomLeft = LatLng(minLat, -MAX_LONGITUDE)
        val negativeViewPortModel =
            MapViewPortModel(topLeft = negativeTopLeft, topRight = negativeTopRight, bottomLeft = negativeBottomLeft)

        return listOf(positiveViewPortModel, negativeViewPortModel)
    }

    private fun walkEastUntil(start: GeoHash, end: GeoHash): Set<GeoHash> {
        val geohashes = HashSet<GeoHash>().apply { add(start) }
        var current = start
        while (current != end) {
            iterationsCounter++
            current = current.easternNeighbour
            geohashes.add(current)
            if (failsafeReached(geohashes.size)) {
                return geohashes
            }
        }
        return geohashes
    }

    private fun walkSouthUntil(topRowGeohashes: Set<GeoHash>, bottomLeftGeohash: GeoHash?): Set<GeoHash> {
        val allGeohashes = HashSet<GeoHash>()

        var newRow = topRowGeohashes
        while (!newRow.contains(bottomLeftGeohash)) {
            iterationsCounter
            allGeohashes.addAll(newRow)
            val oldRow = newRow
            newRow = HashSet<GeoHash>()
            oldRow.forEach { geohash ->
                newRow.add(geohash.southernNeighbour)
            }
            if (failsafeReached(allGeohashes.size)) {
                return allGeohashes
            }
        }

        allGeohashes.addAll(newRow)
        return allGeohashes
    }

    private fun failsafeReached(geohashesSize: Int): Boolean {
        return geohashesSize >= MAX_GEOHASHES_FAILSAFE || iterationsCounter >= MAX_ITERATIONS_FAILSAFE
    }

    companion object {
        const val MAX_GEOHASHES_FAILSAFE = 300
        const val MAX_ITERATIONS_FAILSAFE = 300
        private const val MAX_LONGITUDE = 180.0
    }
}
