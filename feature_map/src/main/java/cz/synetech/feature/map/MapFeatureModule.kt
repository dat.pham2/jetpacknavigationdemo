package cz.synetech.feature.map

import android.content.Context
import androidx.room.Room
import cz.synetech.base.koin.KoinFeatureModule
import cz.synetech.feature.map.data.datasource.local.DefaultCameraPositionDatasource
import cz.synetech.feature.map.data.datasource.local.DefaultCameraPositionDatasourceImpl
import cz.synetech.feature.map.data.datasource.local.DeviceLocationDatasource
import cz.synetech.feature.map.data.datasource.local.DeviceLocationDatasourceImpl
import cz.synetech.feature.map.data.datasource.local.LastCameraPositionDatasource
import cz.synetech.feature.map.data.datasource.local.LastCameraPositionDatasourceImpl
import cz.synetech.feature.map.data.datasource.local.POILocalDatasource
import cz.synetech.feature.map.data.datasource.local.POILocalDatasourceImpl
import cz.synetech.feature.map.data.datasource.local.db.LastMapCameraPositionDao
import cz.synetech.feature.map.data.datasource.local.db.MapDatabase
import cz.synetech.feature.map.data.datasource.local.db.POIDao
import cz.synetech.feature.map.data.datasource.remote.POIRemoteDatasource
import cz.synetech.feature.map.data.datasource.remote.POIRemoteDatasourceImpl
import cz.synetech.feature.map.domain.repository.CameraPositionRepository
import cz.synetech.feature.map.domain.repository.CameraPositionRepositoryImpl
import cz.synetech.feature.map.domain.repository.MapPOIRepository
import cz.synetech.feature.map.domain.repository.MapPOIRepositoryImpl
import cz.synetech.feature.map.domain.repository.POIRepository
import cz.synetech.feature.map.domain.repository.POIRepositoryImpl
import cz.synetech.feature.map.domain.usecase.CalculateGeohashesUseCase
import cz.synetech.feature.map.domain.usecase.CalculateGeohashesUseCaseImpl
import cz.synetech.feature.map.domain.usecase.GetCameraUpdateByDeviceLocationUseCase
import cz.synetech.feature.map.domain.usecase.GetCameraUpdateByDeviceLocationUseCaseImpl
import cz.synetech.feature.map.domain.usecase.GetPOIsUseCase
import cz.synetech.feature.map.domain.usecase.GetPOIsUseCaseImpl
import cz.synetech.feature.map.presentation.usecase.InitMapFragmentUseCase
import cz.synetech.feature.map.presentation.usecase.InitMapFragmentUseCaseImpl
import cz.synetech.feature.map.presentation.viewmodel.MapFragmentViewModel
import cz.synetech.feature.map.presentation.viewmodel.POIDetailFragmentViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module
import org.koin.experimental.builder.factoryBy
import org.koin.experimental.builder.singleBy

internal object MapFeatureModule : KoinFeatureModule("map") {
    private const val MAP_DB_NAME = "BASE_APP_MAP_DATABASE"

    override fun provideKoinModules(): List<Module> {
        return listOf(
            viewModelModule(),
            domainModule(),
            dataModule(),
            databaseModule(),
            navigationModule(),
            presentationModule()
        )
    }

    private fun viewModelModule() = module {
        viewModel {
            MapFragmentViewModel(
                getPOIsUseCase = get(), cameraPositionRepository = get(), getCameraUpdateByDeviceLocationUseCase = get()
            )
        }
        viewModel {
            POIDetailFragmentViewModel(poiRepository = get())
        }
    }

    private fun dataModule() = module {
        factory<POIRemoteDatasource> {
            POIRemoteDatasourceImpl(
                get() as Context
            )
        }
        factoryBy<POILocalDatasource, POILocalDatasourceImpl>()
        factoryBy<DefaultCameraPositionDatasource, DefaultCameraPositionDatasourceImpl>()
        singleBy<DeviceLocationDatasource, DeviceLocationDatasourceImpl>()
        singleBy<LastCameraPositionDatasource, LastCameraPositionDatasourceImpl>()
    }

    private fun databaseModule() = module {
        single<MapDatabase> {
            Room.databaseBuilder(get() as Context, MapDatabase::class.java, MAP_DB_NAME)
                .fallbackToDestructiveMigration()
                .build()
        }

        factory<POIDao> {
            (get() as MapDatabase).poiDao()
        }
        factory<LastMapCameraPositionDao> {
            (get() as MapDatabase).lastMapCameraViewportDao()
        }
    }

    private fun domainModule() = module {
        factoryBy<MapPOIRepository, MapPOIRepositoryImpl>()
        factoryBy<GetPOIsUseCase, GetPOIsUseCaseImpl>()
        factoryBy<POIRepository, POIRepositoryImpl>()
        factoryBy<CalculateGeohashesUseCase, CalculateGeohashesUseCaseImpl>()
        factoryBy<CameraPositionRepository, CameraPositionRepositoryImpl>()
        factoryBy<GetCameraUpdateByDeviceLocationUseCase, GetCameraUpdateByDeviceLocationUseCaseImpl>()
    }

    private fun navigationModule() = module {
        factoryBy<MapRouter, MapRouterImpl>()
    }

    private fun presentationModule() = module {
        factoryBy<InitMapFragmentUseCase, InitMapFragmentUseCaseImpl>()
    }
}
