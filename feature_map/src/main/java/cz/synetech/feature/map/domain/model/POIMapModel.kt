package cz.synetech.feature.map.domain.model

import com.google.android.gms.maps.model.LatLng

data class POIMapModel(
    val id: String,
    val location: LatLng,
    val poiType: POIType
)
