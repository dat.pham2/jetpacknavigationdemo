package cz.synetech.feature.map.domain.model

data class CameraPositionModel(
    val latitude: Double,
    val longitude: Double,
    val zoom: Float = DEFAULT_ZOOM
) {
    companion object {
        const val DEFAULT_ZOOM = 14f
    }
}
