package cz.synetech.feature.map.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import cz.synetech.feature.map.domain.model.CameraPositionModel

@Entity(tableName = "cameraPosition")
data class CameraPositionDbModel(
    @PrimaryKey(autoGenerate = false) val id: Int = 1,
    val latitude: Double,
    val longitude: Double,
    val zoom: Float
)

fun CameraPositionDbModel.toDomainModel(): CameraPositionModel {
    return CameraPositionModel(
        latitude = latitude,
        longitude = longitude,
        zoom = zoom
    )
}
