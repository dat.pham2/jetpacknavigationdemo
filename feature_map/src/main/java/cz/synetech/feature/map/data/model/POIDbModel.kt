package cz.synetech.feature.map.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.android.gms.maps.model.LatLng
import cz.synetech.feature.map.domain.model.POIMapModel
import cz.synetech.feature.map.domain.model.POIModel
import cz.synetech.feature.map.domain.model.POIType

@Entity(tableName = "POIDbModel")
data class POIDbModel(
    @PrimaryKey var id: String,
    var name: String,
    var latitude: Double,
    var longitude: Double,
    var poiType: Int,
    var geoHash: String
)

fun POIDbModel.toMapModel(): POIMapModel {
    return POIMapModel(
        id = id,
        location = LatLng(latitude, longitude),
        poiType = POIType.intToPOIType(poiType)
    )
}

fun POIDbModel.toDomainModel(): POIModel {
    return POIModel(
        id = id,
        name = name,
        location = LatLng(latitude, longitude),
        poiType = POIType.intToPOIType(poiType)
    )
}
