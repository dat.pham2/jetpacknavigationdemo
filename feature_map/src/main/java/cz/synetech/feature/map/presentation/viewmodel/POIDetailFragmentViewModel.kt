package cz.synetech.feature.map.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cz.synetech.base.rx.ext.subscribeLoggingE
import cz.synetech.base.viewmodel.LifecycleViewModel
import cz.synetech.feature.map.domain.repository.POIRepository
import cz.synetech.feature.map.presentation.mapper.POIMapper.poiTypeToPOITypeRes
import cz.synetech.feature.map.presentation.model.POIPresentationModel
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers

internal class POIDetailFragmentViewModel(
    private val poiRepository: POIRepository
) : LifecycleViewModel() {

    private val _poiModel = MutableLiveData<POIPresentationModel>()
    val poiModel: LiveData<POIPresentationModel> = _poiModel

    fun loadPOIDetail(poiId: String) {
        startStopDisposeBag?.let { bag ->
            poiRepository.getPOI(poiId)
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribeLoggingE { poi ->
                    val poiTypeRes = poiTypeToPOITypeRes(poi.poiType)
                    _poiModel.postValue(
                        POIPresentationModel(
                            id = poi.id,
                            typeName = poiTypeRes?.poiTypeName,
                            name = poi.name,
                            location = poi.location,
                            icon = poiTypeRes?.iconResId
                        )
                    )
                }.addTo(bag)
        }
    }
}
