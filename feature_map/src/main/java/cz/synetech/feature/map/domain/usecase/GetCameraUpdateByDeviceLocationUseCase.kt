package cz.synetech.feature.map.domain.usecase

import cz.synetech.feature.map.data.datasource.local.DeviceLocationDatasource
import cz.synetech.feature.map.domain.model.CameraPositionModel
import io.reactivex.rxjava3.core.Observable

interface GetCameraUpdateByDeviceLocationUseCase {
    fun getCameraUpdates(): Observable<CameraPositionModel>
}

class GetCameraUpdateByDeviceLocationUseCaseImpl(
    private val deviceLocationDatasource: DeviceLocationDatasource
) : GetCameraUpdateByDeviceLocationUseCase {

    override fun getCameraUpdates(): Observable<CameraPositionModel> {
        return deviceLocationDatasource.getLocationUpdates().filter { location ->
            location.latitude != 0.0 || location.longitude != 0.0
        }.map { location ->
            CameraPositionModel(latitude = location.latitude, longitude = location.longitude)
        }
    }
}
