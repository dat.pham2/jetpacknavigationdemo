package cz.synetech.feature.map

import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import cz.synetech.feature.map.presentation.ui.fragment.MapFragmentDirections

interface MapRouter {
    fun openPOIDetail(caller: Fragment, poiId: String)
}

class MapRouterImpl : MapRouter {
    override fun openPOIDetail(caller: Fragment, poiId: String) {
        caller.findNavController().navigate(MapFragmentDirections.toPoiDetail(poiId))
    }
}
