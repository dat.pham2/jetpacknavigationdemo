package cz.synetech.feature.map.presentation.model

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem
import cz.synetech.feature.map.domain.model.POIMapModel

data class POIClusterItem(val data: POIMapModel) : ClusterItem {

    override fun getSnippet(): String {
        return ""
    }

    override fun getTitle(): String {
        return ""
    }

    override fun getPosition(): LatLng {
        return LatLng(data.location.latitude, data.location.longitude)
    }

    override fun hashCode(): Int {
        return data.id.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as POIClusterItem

        if (data.id != other.data.id) return false

        return true
    }
}
