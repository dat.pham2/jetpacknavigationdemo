package cz.synetech.feature.map.domain.repository

import com.google.android.gms.maps.model.CameraPosition
import cz.synetech.feature.map.data.datasource.local.DefaultCameraPositionDatasource
import cz.synetech.feature.map.data.datasource.local.LastCameraPositionDatasource
import cz.synetech.feature.map.data.model.CameraPositionDbModel
import cz.synetech.feature.map.data.model.toDomainModel
import cz.synetech.feature.map.domain.model.CameraPositionModel
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

interface CameraPositionRepository {
    fun getLastCameraPosition(): Single<CameraPositionModel>
    fun saveCameraPosition(cameraPosition: CameraPosition): Completable
}

class CameraPositionRepositoryImpl(
    private val defaultCameraPositionDatasource: DefaultCameraPositionDatasource,
    private val lastCameraPositionDatasource: LastCameraPositionDatasource
) : CameraPositionRepository {
    override fun getLastCameraPosition(): Single<CameraPositionModel> {
        val defaultPosition = defaultCameraPositionDatasource.getDefaultCameraPosition()
        return lastCameraPositionDatasource.getCameraPosition().map {
            it.toDomainModel()
        }.defaultIfEmpty(defaultPosition)
    }

    override fun saveCameraPosition(cameraPosition: CameraPosition): Completable {
        return lastCameraPositionDatasource.insertCameraPosition(
            CameraPositionDbModel(
                latitude = cameraPosition.target.latitude,
                longitude = cameraPosition.target.longitude,
                zoom = cameraPosition.zoom
            )
        )
    }
}
