package cz.synetech.feature.map.presentation.model

import androidx.annotation.DrawableRes
import com.google.android.gms.maps.model.LatLng

data class POIPresentationModel(
    val id: String,
    val typeName: String?,
    val name: String,
    val location: LatLng,
    @DrawableRes val icon: Int?
)
