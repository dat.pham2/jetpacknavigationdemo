package cz.synetech.feature.map.data.datasource.local

import android.content.Context
import android.location.Location
import android.os.Looper
import com.google.android.gms.location.LocationAvailability
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import io.reactivex.rxjava3.core.Observable

interface DeviceLocationDatasource {
    fun getLocationUpdates(): Observable<Location>
}

class DeviceLocationDatasourceImpl(private val context: Context) : DeviceLocationDatasource {

    private val fusedLocationProviderClient by lazy {
        LocationServices.getFusedLocationProviderClient(context)
    }

    override fun getLocationUpdates(): Observable<Location> {
        val request = LocationRequest.create().apply {
            interval = LOCATION_UPDATE_INTERVAL
            fastestInterval = LOCATION_UPDATE_FASTEST_INTERVAL
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        return Observable.create { emitter ->
            val locationCallback = object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult?) {
                    locationResult ?: return
                    emitter.onNext(locationResult.lastLocation)
                }

                override fun onLocationAvailability(locationAvailability: LocationAvailability?) {
                    super.onLocationAvailability(locationAvailability)
                    if (locationAvailability?.isLocationAvailable == false) {
                        emitter.onError(LocationUnavailableException())
                    }
                }
            }

            try {
                fusedLocationProviderClient.requestLocationUpdates(
                    request,
                    locationCallback,
                    Looper.getMainLooper()
                )
            } catch (e: SecurityException) {
                emitter.onError(e)
            }

            // on dispose
            emitter.setCancellable {
                fusedLocationProviderClient.removeLocationUpdates(locationCallback)
            }
        }
    }

    private companion object {
        private const val LOCATION_UPDATE_INTERVAL = 10000L
        private const val LOCATION_UPDATE_FASTEST_INTERVAL = 5000L
    }

    class LocationUnavailableException(message: String = "Location is not available") : Exception(message)
}
