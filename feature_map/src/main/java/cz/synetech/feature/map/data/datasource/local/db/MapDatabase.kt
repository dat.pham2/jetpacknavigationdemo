package cz.synetech.feature.map.data.datasource.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import cz.synetech.feature.map.data.model.CameraPositionDbModel
import cz.synetech.feature.map.data.model.POIDbModel
import cz.synetech.feature.map.data.model.POIGeohashDbModel

@Database(
    entities = [
        POIDbModel::class,
        POIGeohashDbModel::class,
        CameraPositionDbModel::class
    ],
    version = 2,
    exportSchema = false
)
abstract class MapDatabase : RoomDatabase() {
    abstract fun poiDao(): POIDao
    abstract fun lastMapCameraViewportDao(): LastMapCameraPositionDao
}
