package cz.synetech.feature.map.data.datasource.local

import cz.synetech.feature.map.domain.model.CameraPositionModel

interface DefaultCameraPositionDatasource {
    fun getDefaultCameraPosition(): CameraPositionModel
}

class DefaultCameraPositionDatasourceImpl : DefaultCameraPositionDatasource {
    override fun getDefaultCameraPosition(): CameraPositionModel {
        return CameraPositionModel(DEFAULT_LAT, DEFAULT_LNG)
    }

    private companion object {
        private const val DEFAULT_LAT = 50.073658
        private const val DEFAULT_LNG = 14.418540
    }
}
