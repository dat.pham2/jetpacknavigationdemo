package cz.synetech.feature.map.domain.model

import com.google.android.gms.maps.model.LatLng

data class POIModel(
    val id: String,
    val name: String,
    val location: LatLng,
    val poiType: POIType
)
