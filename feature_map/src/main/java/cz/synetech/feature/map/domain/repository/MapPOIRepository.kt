package cz.synetech.feature.map.domain.repository

import ch.hsr.geohash.GeoHash
import cz.synetech.feature.map.data.datasource.local.POILocalDatasource
import cz.synetech.feature.map.data.datasource.remote.POIRemoteDatasource
import cz.synetech.feature.map.data.model.POIDbModel
import cz.synetech.feature.map.data.model.POIGeohashDbModel
import cz.synetech.feature.map.data.model.toMapModel
import cz.synetech.feature.map.domain.model.POIMapModel
import cz.synetech.feature.map.domain.repository.MapPOIRepository.Companion.GEOHASH_PRECISION_STRING_LENGTH
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable

interface MapPOIRepository {
    fun getPOIs(geohashes: Set<GeoHash>): Observable<Set<POIMapModel>>

    companion object {
        const val GEOHASH_PRECISION_STRING_LENGTH = 4
    }
}

class MapPOIRepositoryImpl(
    private val poiRemoteDatasource: POIRemoteDatasource,
    private val poiLocalDatasource: POILocalDatasource
) : MapPOIRepository {
    override fun getPOIs(geohashes: Set<GeoHash>): Observable<Set<POIMapModel>> {
        val stringHashes = convertGeohashesToStringGeohashes(geohashes)
        return getRemoteRequests(stringHashes).andThen(getLocalRequests(stringHashes)).map { list ->
            list.map { entity -> entity.toMapModel() }.toSet()
        }
    }

    private fun getLocalRequests(geohashes: Set<String>): Observable<List<POIDbModel>> {
        return poiLocalDatasource.getPOIsForGeoHashes(geohashes)
    }

    private fun getRemoteRequests(geohashes: Set<String>): Completable {
        val remoteRequests = mutableListOf<Completable>()
        geohashes.forEach { geohash ->
            remoteRequests.add(poiLocalDatasource.getGeoHashDate(geohash).isEmpty.flatMapCompletable { isEmpty ->
                if (!isEmpty) {
                    poiLocalDatasource.getGeoHashDate(geohash).flatMapCompletable { poiGeohashDbModel ->
                        if (shouldRefreshGeohash(poiGeohashDbModel)) {
                            refreshGeohashPOIs(geohash)
                        } else {
                            Completable.complete()
                        }
                    }
                } else {
                    refreshGeohashPOIs(geohash)
                }
            })
        }

        return Completable.merge(remoteRequests)
    }

    private fun shouldRefreshGeohash(geohash: POIGeohashDbModel): Boolean {
        return geohash.lastCheckTimeStamp + GEOHASH_EXPIRATION_INTERVAL_MS < System.currentTimeMillis()
    }

    private fun refreshGeohashPOIs(geohash: String): Completable {
        return poiRemoteDatasource.getPOIs(geohash).flatMapCompletable { pois ->
            poiLocalDatasource.deleteAllPOIsForGeoHash(geohash).andThen(
                poiLocalDatasource.insertPoiGeohash(POIGeohashDbModel(geohash, System.currentTimeMillis())).andThen(
                    poiLocalDatasource.insertPOIs(pois.map { apiModel ->
                        POIDbModel(
                            id = apiModel.id.toString(),
                            name = apiModel.name,
                            latitude = apiModel.latitude,
                            longitude = apiModel.longitude,
                            poiType = apiModel.typeId,
                            geoHash = geohash
                        )
                    }.toHashSet())
                )
            )
        }
    }

    private fun convertGeohashesToStringGeohashes(geohashes: Set<GeoHash>): Set<String> {
        return geohashes.map { it.toBase32().substring(0, GEOHASH_PRECISION_STRING_LENGTH) }.toHashSet()
    }

    private companion object {
        private const val GEOHASH_EXPIRATION_INTERVAL_MS = 1000 * 60 * 60 * 2 // 2 hours
    }
}
