package cz.synetech.feature.map.presentation.ui.map

import android.content.Context
import androidx.lifecycle.LiveData
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.VisibleRegion
import cz.synetech.base.logger.Logger
import cz.synetech.feature.map.R
import cz.synetech.feature.map.domain.model.CameraPositionModel
import cz.synetech.feature.map.presentation.mapper.MapMarkerIconMapper
import cz.synetech.feature.map.presentation.model.POIClusterItem

class GoogleMapManager(
    private val context: Context,
    private val openPOICallback: (poiId: String) -> Unit,
    private val onCameraMoveListener: (visibleRegion: VisibleRegion) -> Unit,
    private val locationPermissionLiveData: LiveData<Boolean>
) : OnMapReadyCallback {

    private val mapMarkerIconMapper = MapMarkerIconMapper(context)
    private lateinit var clusterManager: CustomClusterManager
    private var googleMap: GoogleMap? = null
    override fun onMapReady(googleMap: GoogleMap?) {
        googleMap ?: return
        setupMap(googleMap)
        clusterManager = CustomClusterManager(context, googleMap, mapMarkerIconMapper, openPOICallback)
    }

    private fun setupMap(googleMap: GoogleMap) {
        this.googleMap = googleMap
        googleMap.uiSettings?.setAllGesturesEnabled(true)
        googleMap.uiSettings?.isMyLocationButtonEnabled = false
        googleMap.setMinZoomPreference(MIN_ZOOM)

        googleMap.setOnCameraMoveListener {
            onCameraMoveListener(googleMap.projection.visibleRegion)
        }
        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.mapstyle))
        updateLocationUI()
    }

    fun updateLocationUI() {
        try {
            googleMap?.isMyLocationEnabled = locationPermissionLiveData.value == true
        } catch (e: SecurityException) {
            Logger.e(e)
        }
    }

    fun moveCameraToPosition(cameraPosition: CameraPositionModel, isDeviceLocation: Boolean) {
        if (isDeviceLocation) {
            val update = CameraUpdateFactory.newLatLng(LatLng(cameraPosition.latitude, cameraPosition.longitude))
            googleMap?.animateCamera(update)
        } else {
            val update = CameraUpdateFactory.newLatLngZoom(
                LatLng(cameraPosition.latitude, cameraPosition.longitude), cameraPosition.zoom
            )
            googleMap?.moveCamera(update)
            googleMap?.projection?.visibleRegion?.let {
                onCameraMoveListener(it)
            }
        }
    }

    suspend fun onPOIsUpdate(data: Set<POIClusterItem>) {
        clusterManager.onPOIsUpdate(data)
    }

    fun getCameraPosition(): CameraPosition? {
        return googleMap?.cameraPosition
    }

    private companion object {
        private const val MIN_ZOOM = 10f
    }
}
