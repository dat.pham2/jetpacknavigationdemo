package cz.synetech.feature.map.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import cz.synetech.base.databinding.ext.setupDataBinding
import cz.synetech.feature.map.BR
import cz.synetech.feature.map.R
import cz.synetech.feature.map.databinding.FragmentDialogPoiDetailBinding
import cz.synetech.feature.map.presentation.viewmodel.POIDetailFragmentViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class POIDetailFragment : BottomSheetDialogFragment() {
    private val fragmentViewModel: POIDetailFragmentViewModel by viewModel()
    private val args: POIDetailFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val binding = setupDataBinding<FragmentDialogPoiDetailBinding>(
            R.layout.fragment_dialog_poi_detail,
            BR.viewModel to fragmentViewModel
        )
        fragmentViewModel.lifecycleOwner = this
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        fragmentViewModel.loadPOIDetail(args.poiId)
    }
}
