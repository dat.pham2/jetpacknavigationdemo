package cz.synetech.feature.map.domain.model

import com.google.android.gms.maps.model.LatLng

data class MapViewPortModel(
    val topLeft: LatLng,
    val topRight: LatLng,
    val bottomLeft: LatLng
)
