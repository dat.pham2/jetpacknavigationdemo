package cz.synetech.feature.map.presentation.model

import cz.synetech.feature.map.domain.model.CameraPositionModel

data class CameraPositionPresentationModel(
    val position: CameraPositionModel,
    val isDeviceLocation: Boolean
)
