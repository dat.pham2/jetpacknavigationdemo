package cz.synetech.feature.map.domain.usecase

import com.google.android.gms.maps.model.LatLng
import cz.synetech.feature.map.domain.model.POIMapModel
import cz.synetech.feature.map.domain.repository.MapPOIRepository
import io.reactivex.rxjava3.core.Observable

interface GetPOIsUseCase {
    fun getPOIs(
        topLeft: LatLng,
        topRight: LatLng,
        bottomLeft: LatLng,
        bottomRight: LatLng
    ): Observable<Set<POIMapModel>>
}

class GetPOIsUseCaseImpl(
    private val mapPoiRepository: MapPOIRepository,
    private val calculateGeohashesUseCase: CalculateGeohashesUseCase
) : GetPOIsUseCase {
    override fun getPOIs(
        topLeft: LatLng,
        topRight: LatLng,
        bottomLeft: LatLng,
        bottomRight: LatLng
    ): Observable<Set<POIMapModel>> {
        val geohashes = calculateGeohashesUseCase.calculateGeohashes(topLeft, topRight, bottomLeft, bottomRight)
        return mapPoiRepository.getPOIs(geohashes)
    }
}
