package cz.synetech.feature.map.domain.model

enum class POIType {
    FRIEND,
    HAIRDRESSER,
    GROCERY,
    UNKNOWN;

    companion object {
        fun intToPOIType(type: Int): POIType {
            return when (type - 1) {
                FRIEND.ordinal -> FRIEND
                HAIRDRESSER.ordinal -> HAIRDRESSER
                GROCERY.ordinal -> GROCERY
                else -> UNKNOWN
            }
        }
    }
}
