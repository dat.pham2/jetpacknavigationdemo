package cz.synetech.feature.map.data.datasource.local.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import cz.synetech.feature.map.data.model.CameraPositionDbModel
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Maybe

@Dao
interface LastMapCameraPositionDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCameraPosition(position: CameraPositionDbModel): Completable

    @Query("SELECT * FROM cameraPosition")
    fun getCameraPosition(): Maybe<CameraPositionDbModel>

    @Query("DELETE FROM cameraPosition")
    fun deleteCameraPosition(): Completable
}
