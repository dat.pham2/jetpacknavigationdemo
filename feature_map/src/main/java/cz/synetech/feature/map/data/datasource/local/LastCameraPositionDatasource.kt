package cz.synetech.feature.map.data.datasource.local

import cz.synetech.feature.map.data.datasource.local.db.LastMapCameraPositionDao
import cz.synetech.feature.map.data.model.CameraPositionDbModel
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Maybe

interface LastCameraPositionDatasource {
    fun insertCameraPosition(position: CameraPositionDbModel): Completable
    fun getCameraPosition(): Maybe<CameraPositionDbModel>
    fun deleteCameraPosition(): Completable
}

class LastCameraPositionDatasourceImpl(private val dao: LastMapCameraPositionDao) : LastCameraPositionDatasource {
    override fun insertCameraPosition(position: CameraPositionDbModel): Completable {
        return dao.insertCameraPosition(position)
    }

    override fun getCameraPosition(): Maybe<CameraPositionDbModel> {
        return dao.getCameraPosition()
    }

    override fun deleteCameraPosition(): Completable {
        return dao.deleteCameraPosition()
    }
}
