package cz.synetech.feature.map.data.datasource.remote

import android.content.Context
import com.google.gson.Gson
import cz.synetech.feature.map.data.model.POIEntity
import io.reactivex.rxjava3.core.Single
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.io.Reader
import java.io.StringWriter
import java.io.Writer
import java.nio.charset.Charset

interface POIRemoteDatasource {
    fun getPOIs(geohash: String): Single<Set<POIEntity>>
}

class POIRemoteDatasourceImpl(private val context: Context) :
    POIRemoteDatasource {
    override fun getPOIs(geohash: String): Single<Set<POIEntity>> {
        val cutGeohash = if (geohash.length >= GEOHASH_LENGTH_CUTOFF) {
            geohash.substring(0,
                GEOHASH_LENGTH_CUTOFF
            )
        } else {
            geohash
        }
        return Single.just(getPOIsFromFile(cutGeohash))
    }

    private fun getPOIsFromFile(geohash: String): Set<POIEntity> {
        val jsonString: String = readFile()
        val gson = Gson()
        val data: Array<POIEntity> = gson.fromJson<Array<POIEntity>>(jsonString, Array<POIEntity>::class.java)
        return data.filter { it.geohash.contains(geohash) }.toHashSet()
    }

    private fun readFile(): String {
        val inputStream: InputStream = context.assets.open("poi_all.json")
        val writer: Writer = StringWriter()
        val buffer = CharArray(BUFFER_SIZE)
        inputStream.use {
            val reader: Reader = BufferedReader(InputStreamReader(inputStream, Charset.defaultCharset()))
            var n: Int
            while (reader.read(buffer).also { n = it } != -1) {
                writer.write(buffer, 0, n)
            }
        }

        return writer.toString()
    }

    private companion object {
        private const val GEOHASH_LENGTH_CUTOFF = 5 // our data has geohashes with 5 characters precision
        private const val BUFFER_SIZE = 1024
    }
}
