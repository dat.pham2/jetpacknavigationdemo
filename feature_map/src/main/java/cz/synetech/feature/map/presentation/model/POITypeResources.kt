package cz.synetech.feature.map.presentation.model

import androidx.annotation.DrawableRes
import cz.synetech.feature.map.R

enum class POITypeResources(@DrawableRes val iconResId: Int, val poiTypeName: String) {
    FRIEND(R.drawable.ic_friend, "Friend"),
    HAIRDRESSER(R.drawable.ic_hairdresser, "Hairdresser"),
    GROCERY(R.drawable.ic_grocery, "Grocery")
}
