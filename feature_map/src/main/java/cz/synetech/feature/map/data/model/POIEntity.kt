package cz.synetech.feature.map.data.model

import com.google.gson.annotations.SerializedName

data class POIEntity(
    @SerializedName("POIID")
    val id: Long,
    @SerializedName("Name")
    val name: String,
    @SerializedName("POITypeId")
    val typeId: Int,
    @SerializedName("Geohash")
    val geohash: String,
    @SerializedName("Latitude")
    val latitude: Double,
    @SerializedName("Longitude")
    val longitude: Double
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as POIEntity

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}
