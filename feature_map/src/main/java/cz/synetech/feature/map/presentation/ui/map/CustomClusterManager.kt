package cz.synetech.feature.map.presentation.ui.map

import android.content.Context
import android.graphics.Color
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.ClusterManager.OnClusterItemClickListener
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import cz.synetech.feature.map.presentation.mapper.MapMarkerIconMapper
import cz.synetech.feature.map.presentation.model.POIClusterItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CustomClusterManager(
    context: Context,
    private val googleMap: GoogleMap,
    private val mapMarkerIconMapper: MapMarkerIconMapper,
    private val openPOICallback: (poiId: String) -> Unit
) : ClusterManager<POIClusterItem>(context, googleMap) {

    private val currentlyDisplayedPOIs = HashSet<POIClusterItem>()
    private val renderer = object : DefaultClusterRenderer<POIClusterItem>(context, googleMap, this) {
        override fun onBeforeClusterItemRendered(item: POIClusterItem, markerOptions: MarkerOptions) {
            mapMarkerIconMapper.getThingMarkerDescriptor(item.data.poiType)?.let { markerOptions.icon(it) }
            super.onBeforeClusterItemRendered(item, markerOptions)
        }

        override fun getColor(clusterSize: Int): Int {
            return Color.BLACK
        }
    }

    private val onClusterClickListener = OnClusterItemClickListener<POIClusterItem> { item ->
        openPOICallback(item.data.id)
        true
    }

    init {
        setRenderer(renderer)
        setOnClusterItemClickListener(onClusterClickListener)
        googleMap.setOnCameraIdleListener(this)
        googleMap.setOnMarkerClickListener(this)
    }

    suspend fun onPOIsUpdate(updatedPOIs: Set<POIClusterItem>) {
        withContext(Dispatchers.IO) {
            val poisToBeRemoved = currentlyDisplayedPOIs.minus(updatedPOIs)
            val poisToBeAdded = updatedPOIs.minus(currentlyDisplayedPOIs)
            currentlyDisplayedPOIs.removeAll(poisToBeRemoved)
            currentlyDisplayedPOIs.addAll(poisToBeAdded)
            this@CustomClusterManager.removeItems(poisToBeRemoved)
            this@CustomClusterManager.addItems(poisToBeAdded)
            if (poisToBeAdded.isNotEmpty() || poisToBeRemoved.isNotEmpty()) {
                withContext(Dispatchers.Main) {
                    this@CustomClusterManager.cluster()
                }
            }
        }
    }
}
