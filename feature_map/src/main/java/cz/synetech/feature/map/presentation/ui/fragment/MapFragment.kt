package cz.synetech.feature.map.presentation.ui.fragment

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsResponse
import com.google.android.gms.location.LocationSettingsStatusCodes
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.tasks.Task
import com.tbruyelle.rxpermissions3.RxPermissions
import cz.synetech.base.databinding.ext.setupDataBinding
import cz.synetech.base.livedata.observer.SpecificEventObserver
import cz.synetech.base.logger.Logger
import cz.synetech.feature.map.BR
import cz.synetech.feature.map.MapFeatureModule
import cz.synetech.feature.map.MapRouter
import cz.synetech.feature.map.R
import cz.synetech.feature.map.databinding.FragmentMapBinding
import cz.synetech.feature.map.presentation.ui.dialog.GoToSettingsDialog
import cz.synetech.feature.map.presentation.ui.map.GoogleMapManager
import cz.synetech.feature.map.presentation.usecase.InitMapFragmentUseCase
import cz.synetech.feature.map.presentation.viewmodel.MapFragmentViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class MapFragment : Fragment(), OnMapReadyCallback {
    private val router: MapRouter by inject()
    private val initMapFragmentUseCase: InitMapFragmentUseCase by inject()
    private val fragmentViewModel: MapFragmentViewModel by viewModel()
    private val googleMapManager by lazy {
        GoogleMapManager(
            context = requireContext(),
            openPOICallback = { poiId -> router.openPOIDetail(this, poiId) },
            onCameraMoveListener = { visibleRegion -> fragmentViewModel.onCameraMoved(visibleRegion) },
            locationPermissionLiveData = fragmentViewModel.locationPermission
        )
    }
    private var shouldGoToSettings = false
    private var isMapReady = false

    init {
        MapFeatureModule.load()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val binding = setupDataBinding<FragmentMapBinding>(
            R.layout.fragment_map,
            BR.viewModel to fragmentViewModel
        )
        binding.lifecycleOwner = viewLifecycleOwner
        fragmentViewModel.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observe()
        initMapFragmentUseCase.initMapFragment(childFragmentManager, this)
    }

    override fun onStart() {
        super.onStart()
        fragmentViewModel.checkLocationPermission(context?.applicationContext)
    }

    private fun observe() {
        fragmentViewModel.pointsOfInterest.observe(viewLifecycleOwner, Observer {
            GlobalScope.launch {
                googleMapManager.onPOIsUpdate(it.data)
                it.consume()
            }
        })
        fragmentViewModel.mapNavigationEvent.observe(
            viewLifecycleOwner, SpecificEventObserver<MapFragmentViewModel.MapNavigationEvent> { event ->
                when (event) {
                    is MapFragmentViewModel.MapNavigationEvent.OnAllowLocationRequest -> {
                        requestLocationPermission()
                    }
                }.let { }
            })

        fragmentViewModel.locationPermission.observe(viewLifecycleOwner, Observer { granted ->
            if (granted && isMapReady) {
                googleMapManager.updateLocationUI()
                showLocationPrompt()
            }
        })
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        isMapReady = true
        this.googleMapManager.onMapReady(googleMap)

        fragmentViewModel.cameraPosition.observe(viewLifecycleOwner, Observer { model ->
            googleMapManager.moveCameraToPosition(model.position, model.isDeviceLocation)
        })

        if (fragmentViewModel.locationPermission.value == true) {
            googleMapManager.updateLocationUI()
            showLocationPrompt()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                val granted = grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED
                fragmentViewModel.notifyLocationPermissionResult(granted)
            }
        }
    }

    override fun onPause() {
        fragmentViewModel.saveCameraPosition(googleMapManager.getCameraPosition())
        super.onPause()
    }

    private fun requestLocationPermission() {
        activity?.let { activity ->
            RxPermissions(activity)
                .requestEach(Manifest.permission.ACCESS_FINE_LOCATION)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { permissionResult ->
                    when {
                        permissionResult.granted -> { // Always true pre-M
                            fragmentViewModel.notifyLocationPermissionResult(true)
                        }
                        permissionResult.shouldShowRequestPermissionRationale -> {
                            // nothing to do
                        }
                        else -> {
                            // Don't show dialog when user has checked "Don't ask again" for the first time
                            if (shouldGoToSettings) {
                                GoToSettingsDialog(activity).show()
                            } else {
                                shouldGoToSettings = true
                            }
                        }
                    }
                }
        }
    }

    private fun showLocationPrompt() {
        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)

        val result: Task<LocationSettingsResponse> =
            LocationServices.getSettingsClient(requireActivity()).checkLocationSettings(builder.build())

        result.addOnCompleteListener { task ->
            try {
                val response = task.getResult(ApiException::class.java)
                fragmentViewModel.notifyLocationEnabled()
            } catch (exception: ApiException) {
                when (exception.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                        try {
                            val resolvable: ResolvableApiException = exception as ResolvableApiException
                            startIntentSenderForResult(
                                resolvable.resolution.intentSender,
                                REQUEST_TURN_LOCATION_ON,
                                null,
                                0,
                                0,
                                0,
                                null
                            )
                        } catch (e: IntentSender.SendIntentException) {
                            Logger.e(e)
                        }
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_TURN_LOCATION_ON && resultCode == Activity.RESULT_OK) {
            fragmentViewModel.notifyLocationEnabled()
        }
    }

    private companion object {
        private const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 101
        private const val REQUEST_TURN_LOCATION_ON = 102
    }
}
