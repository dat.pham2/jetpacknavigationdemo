package cz.synetech.feature.map.presentation.usecase

import androidx.fragment.app.FragmentManager
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import cz.synetech.feature.map.R

interface InitMapFragmentUseCase {
    fun initMapFragment(fragmentManager: FragmentManager, callback: OnMapReadyCallback)
}

class InitMapFragmentUseCaseImpl : InitMapFragmentUseCase {
    override fun initMapFragment(fragmentManager: FragmentManager, callback: OnMapReadyCallback) {
        val mapFragment = fragmentManager.findFragmentByTag(MAP_FRAGMENT_TAG) as? SupportMapFragment
            ?: SupportMapFragment.newInstance()
        val transaction = fragmentManager.beginTransaction()
        mapFragment?.let {
            transaction.replace(R.id.fl_map_container, it, MAP_FRAGMENT_TAG)
        }
        transaction.disallowAddToBackStack()
        transaction.commit()
        mapFragment?.getMapAsync(callback)
    }

    private companion object {
        private const val MAP_FRAGMENT_TAG = "BASE_APP_MAP_FRAGMENT"
    }
}
