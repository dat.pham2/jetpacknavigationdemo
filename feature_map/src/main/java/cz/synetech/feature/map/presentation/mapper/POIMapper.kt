package cz.synetech.feature.map.presentation.mapper

import cz.synetech.feature.map.domain.model.POIType
import cz.synetech.feature.map.presentation.model.POITypeResources

object POIMapper {
    fun poiTypeToPOITypeRes(poiType: POIType): POITypeResources? {
        return when (poiType) {
            POIType.FRIEND -> POITypeResources.FRIEND
            POIType.HAIRDRESSER -> POITypeResources.HAIRDRESSER
            POIType.GROCERY -> POITypeResources.GROCERY
            else -> null
        }
    }
}
