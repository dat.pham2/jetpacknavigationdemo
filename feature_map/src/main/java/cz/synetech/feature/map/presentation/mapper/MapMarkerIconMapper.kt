package cz.synetech.feature.map.presentation.mapper

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import cz.synetech.feature.map.R
import cz.synetech.feature.map.domain.model.POIType
import java.util.EnumMap

class MapMarkerIconMapper(private val context: Context) {

    private val thingsDescriptors = EnumMap<POIType, BitmapDescriptor>(POIType::class.java)

    fun getThingMarkerDescriptor(type: POIType): BitmapDescriptor? {
        if (thingsDescriptors.isEmpty()) {
            preCalculateThingDescriptors()
        }
        return thingsDescriptors[type]
    }

    private fun preCalculateThingDescriptors() {
        POIType.values().filter {
            it != POIType.UNKNOWN
        }.forEach { type ->
            POIMapper.poiTypeToPOITypeRes(type)?.iconResId?.let { resId ->
                thingsDescriptors[type] = getBitmapDescriptor(resId)
            }
        }
    }

    private fun getBitmapDescriptor(id: Int, bitmapScale: Double = 1.5): BitmapDescriptor {
        val iconDrawable = ContextCompat.getDrawable(context, id) as Drawable
        val height = (iconDrawable.intrinsicHeight * bitmapScale).toInt()
        val width = (iconDrawable.intrinsicWidth * bitmapScale).toInt()

        iconDrawable.setBounds(
            (width * ICON_PADDING_PERCENTAGE).toInt(),
            (height * ICON_PADDING_PERCENTAGE).toInt(),
            (width * (1 - ICON_PADDING_PERCENTAGE)).toInt(),
            (height * (1 - ICON_PADDING_PERCENTAGE)).toInt()
        )

        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = drawCircle(width, height, bitmap)

        iconDrawable.draw(canvas)

        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    private fun drawCircle(width: Int, height: Int, bitmap: Bitmap): Canvas {
        val canvas = Canvas(bitmap)
        val circleBackground = Paint()
        circleBackground.color = ContextCompat.getColor(context, android.R.color.black)
        circleBackground.style = Paint.Style.FILL
        val radius = if (height > width) {
            height
        } else {
            width
        }
        setShadowLayer(circleBackground)

        canvas.drawCircle(width / 2f, height / 2f, radius / CIRCLE_RADIUS_RATIO, circleBackground)
        return canvas
    }

    private fun setShadowLayer(circleBackground: Paint) {
        val shadowColor = ContextCompat.getColor(context, R.color.poiShadow)
        circleBackground.setShadowLayer(SHADOW_RADIUS, 0f, 0f, shadowColor)
    }

    private companion object {
        private const val ICON_PADDING_PERCENTAGE = 0.1
        private const val SHADOW_RADIUS = 5f
        private const val CIRCLE_RADIUS_RATIO = 2.5f
    }
}
