package cz.synetech.feature.map.presentation.ui.dialog

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import cz.synetech.baseapp.ui.dialog.PositiveButtonDialog
import cz.synetech.feature.map.R

internal class GoToSettingsDialog(
    activity: Activity
) : PositiveButtonDialog(
    context = activity,
    buttonClickCallback = { goToSettings(activity) },
    titleResId = R.string.map_permission_settings_dialog_title,
    messageResId = R.string.map_permission_settings_dialog_message,
    buttonResId = R.string.common_ok,
    cancelable = true
) {
    private companion object {
        private fun goToSettings(activity: Activity) {
            val uri = Uri.fromParts("package", activity.packageName, null)
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply { data = uri }
            activity.startActivity(intent)
        }
    }
}
