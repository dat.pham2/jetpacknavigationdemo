package cz.synetech.feature.map.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "POIGeohashDbModel")
data class POIGeohashDbModel(
    @PrimaryKey var geoHash: String,
    var lastCheckTimeStamp: Long
)
