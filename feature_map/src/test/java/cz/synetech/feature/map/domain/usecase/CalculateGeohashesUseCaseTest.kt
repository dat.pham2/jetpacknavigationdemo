package cz.synetech.feature.map.domain.usecase

import ch.hsr.geohash.GeoHash
import com.google.android.gms.maps.model.LatLng
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class CalculateGeohashesUseCaseTest {

    private val instance = CalculateGeohashesUseCaseImpl()

    @Test
    fun testCommonGeohashRegion5x4() {
        val calculated = instance.calculateGeohashes(
            topLeft = LatLng(50.54, 13.89), topRight = LatLng(50.54, 15.29),
            bottomLeft = LatLng(50.01, 13.89), bottomRight = LatLng(50.01, 13.89),
            stringPrecision = 4
        )
        val expectedResult = setOf(
            "u2cz", "u2cy", "u2cv", "u2cu",
            "u2fp", "u2fn", "u2fj", "u2fh",
            "u2fr", "u2fq", "u2fm", "u2fk",
            "u2fx", "u2fw", "u2ft", "u2fs",
            "u2fz", "u2fy", "u2fv", "u2fu"
        )
        checkGeohashes(actual = calculated, expected = expectedResult)
    }

    @Test
    fun testCommonGeohashRegion2x2() {
        val calculated = instance.calculateGeohashes(
            topLeft = LatLng(50.01, 17.05), topRight = LatLng(50.01, 17.4),
            bottomLeft = LatLng(49.83, 17.05), bottomRight = LatLng(49.83, 17.05),
            stringPrecision = 4
        )
        val expectedResult = setOf(
            "u2uh", "u2u5",
            "u2uk", "u2u7"
        )
        checkGeohashes(actual = calculated, expected = expectedResult)
    }

    @Test
    fun testCommonGeohashRegion2x1() {
        val calculated = instance.calculateGeohashes(
            topLeft = LatLng(50.01, 17.05), topRight = LatLng(50.01, 17.05),
            bottomLeft = LatLng(49.83, 17.05), bottomRight = LatLng(49.83, 17.05),
            stringPrecision = 4
        )
        val expectedResult = setOf(
            "u2uh", "u2u5"
        )
        checkGeohashes(actual = calculated, expected = expectedResult)
    }

    @Test
    fun testCommonGeohashRegion1x1() {
        val calculated = instance.calculateGeohashes(
            topLeft = LatLng(50.01, 17.05), topRight = LatLng(50.01, 17.05),
            bottomLeft = LatLng(50.01, 17.05), bottomRight = LatLng(50.01, 17.05),
            stringPrecision = 4
        )
        val expectedResult = setOf(
            "u2uh"
        )
        checkGeohashes(actual = calculated, expected = expectedResult)
    }

    /// Somewhere in Pacific ocean
    @Test
    fun testDateLineGeohashRegion4x3() {
        val calculated = instance.calculateGeohashes(
            topLeft = LatLng(43.33, 179.47), topRight = LatLng(43.33, -179.47),
            bottomLeft = LatLng(42.98, 179.47), bottomRight = LatLng(42.98, 179.47),
            stringPrecision = 4
        )
        val expectedResult = setOf(
            "xzxw", "xzxt", "xzxs",
            "xzxy", "xzxv", "xzxu",
            "8p8n", "8p8j", "8p8h",
            "8p8q", "8p8m", "8p8k"
        )
        checkGeohashes(actual = calculated, expected = expectedResult)
    }

    @Test
    fun testGreenwichGeohashRegion2x3() {
        val calculated = instance.calculateGeohashes(
            topLeft = LatLng(51.59, -0.18), topRight = LatLng(51.59, 0.18),
            bottomLeft = LatLng(51.24, -0.18), bottomRight = LatLng(51.24, -0.18),
            stringPrecision = 4
        )
        val expectedResult = setOf(
            "gcpv", "gcpu", "gcpg",
            "u10j", "u10h", "u105"
        )
        checkGeohashes(actual = calculated, expected = expectedResult)
    }

    @Test
    fun testPrecision1GeohashRegion4x4() {
        val calculated = instance.calculateGeohashes(
            topLeft = LatLng(68.0, -68.0), topRight = LatLng(68.0, 68.0),
            bottomLeft = LatLng(-68.0, -68.0), bottomRight = LatLng(-68.0, -68.0),
            stringPrecision = 1
        )
        val expectedResult = setOf(
            "f", "d", "6", "4",
            "g", "e", "7", "5",
            "u", "s", "k", "h",
            "v", "t", "m", "j"
        )
        checkGeohashes(actual = calculated, expected = expectedResult)
    }

    @Test
    fun testPrecision2GeohashRegion3x3() {
        val calculated = instance.calculateGeohashes(
            topLeft = LatLng(2.8, 84.0), topRight = LatLng(2.8, 107.0),
            bottomLeft = LatLng(-8.4, 84.0), bottomRight = LatLng(-8.4, 84.0),
            stringPrecision = 2
        )
        val expectedResult = setOf(
            "tb", "mz", "my",
            "w0", "qp", "qn",
            "w2", "qr", "qq"
        )
        checkGeohashes(actual = calculated, expected = expectedResult)
    }

    @Test
    fun testPrecision3GeohashRegion3x3() {
        val calculated = instance.calculateGeohashes(
            topLeft = LatLng(57.0, -103.4), topRight = LatLng(57.0, -100.5),
            bottomLeft = LatLng(54.1, -103.4), bottomRight = LatLng(54.1, -103.4),
            stringPrecision = 3
        )
        val expectedResult = setOf(
            "cdn", "c9y", "c9w",
            "cdp", "c9z", "c9x",
            "cf0", "ccb", "cc8"
        )
        checkGeohashes(actual = calculated, expected = expectedResult)
    }

    @Test
    fun testPrecision5GeohashRegion3x3() {
        val calculated = instance.calculateGeohashes(
            topLeft = LatLng(-37.771, 144.866), topRight = LatLng(-37.771, 144.954),
            bottomLeft = LatLng(-37.859, 144.866), bottomRight = LatLng(-37.859, 144.954),
            stringPrecision = 5
        )
        val expectedResult = setOf(
            "r1r10", "r1r0b", "r1r08",
            "r1r11", "r1r0c", "r1r09",
            "r1r14", "r1r0f", "r1r0d"
        )
        checkGeohashes(actual = calculated, expected = expectedResult)
    }

    @Test
    fun testSwitchedCornersGeohashRegion() {
        val calculated = instance.calculateGeohashes(
            topLeft = LatLng(-37.771, 144.954), topRight = LatLng(-37.771, 144.866),
            bottomLeft = LatLng(-37.859, 144.954), bottomRight = LatLng(-37.859, 144.866),
            stringPrecision = 5
        )
        val expectedResult = setOf(
            "r1r10", "r1r0b", "r1r08",
            "r1r11", "r1r0c", "r1r09",
            "r1r14", "r1r0f", "r1r0d"
        )
        checkGeohashes(actual = calculated, expected = expectedResult)
    }

    @Test
    fun testBigGeohashRegion() {
        val calculated = instance.calculateGeohashes(
            topLeft = LatLng(50.54, 130.89), topRight = LatLng(50.54, 150.0),
            bottomLeft = LatLng(50.01, 130.89), bottomRight = LatLng(50.01, 150.0),
            stringPrecision = 4
        )

        val expectedResult = setOf(
            "z0gk", "z0gm", "z0gh", "z0gj", "z0gq", "z0gr", "z0gn", "z0gp", "z0gu", "z0gv", "z0gs", "z0gt", "z0gy",
            "z0gz", "z0gw", "z0gx", "z0zk", "z0zm", "z0zh", "z0fz", "z0zj", "z0zq", "z0zr", "z0zn", "z0zp", "z0zu",
            "z0zv", "z0zs", "z0zt", "z0zy", "z0zz", "z0zw", "z0zx", "z0fj", "z0fk", "z0yz", "z0fh", "z0fp", "z0fq",
            "z0fm", "z0fn", "z0ft", "z0fu", "z0fr", "z0fs", "z0fx", "z0fy", "z0fv", "z0fw", "z0yj", "z0yk", "z0yh",
            "z0yp", "z0yq", "z0ym", "z0yn", "z0yt", "z0yu", "z0yr", "z0ys", "z0yx", "z0yy", "z0yv", "z0yw", "ybzk",
            "ybzm", "ybzh", "ybzj", "ybzq", "ybzr", "ybzn", "ybzp", "ybzu", "ybzv", "ybzs", "ybzt", "ybzy", "ybzz",
            "ybzw", "ybzx", "z2fj", "z2fk", "ybyz", "z2fh", "z2fp", "z2fq", "z2fm", "z2fn", "z2ft", "z2fr", "z2fs",
            "z2fx", "z2fw", "ybyj", "z0cy", "ybyk", "z0cz", "z0cw", "ybyh", "z0cx", "ybyp", "ybyq", "ybym", "ybyn",
            "ybyt", "ybyu", "ybyr", "ybys", "ybyx", "ybyy", "ybyv", "ybyw", "z0vy", "z0vz", "z0vw", "z0vx", "z0ck",
            "z0cm", "z0ch", "z0cj", "z0cq", "z0cr", "z0cn", "z0cp", "z0cu", "z0cv", "z0cs", "z0ct", "z0bx", "z0by",
            "z0bv", "z0bw", "z0vk", "z0vm", "z0vh", "z0bz", "z0vj", "z0vq", "z0vr", "z0vn", "z0vp", "z0vu", "z0vv",
            "z0vs", "z0vt", "z0ux", "z0uy", "z0uv", "z0uw", "z0bj", "z0bk", "z0uz", "z0bh", "z0bp", "z0bq", "z0bm",
            "z0bn", "z0bt", "z0bu", "z0br", "z0bs", "z2cy", "z2cz", "z2cw", "z2cx", "z0uj", "z0uk", "z0uh", "z0up",
            "z0uq", "z0um", "z0un", "z0ut", "z0uu", "z0ur", "z0us", "ybvy", "ybvz", "ybvw", "ybvx", "z2ck", "z2cm",
            "z2ch", "z2cj", "z2cq", "z2cr", "z2cn", "z2cp", "z2cu", "z2cv", "z2cs", "z2ct", "z2bx", "z2by", "z2bv",
            "z2bw", "ybvk", "ybvm", "ybvh", "z2bz", "ybvj", "ybvq", "ybvr", "ybvn", "ybvp", "ybvu", "ybvv", "ybvs",
            "ybvt", "z2bj", "z2bk", "z2bh", "z2bp", "z2bq", "z2bm", "z2bn", "z2bt", "z2bu", "z2br", "z2bs"
        )
        checkGeohashes(actual = calculated, expected = expectedResult)
    }

    @Test
    fun testMaxGeohashesFailsafe() {
        val calculated = instance.calculateGeohashes(
            topLeft = LatLng(50.54, 13.89), topRight = LatLng(50.54, 180.29),
            bottomLeft = LatLng(50.01, 13.89), bottomRight = LatLng(50.01, 180.29),
            stringPrecision = 4
        )
        assertTrue(calculated.size <= CalculateGeohashesUseCaseImpl.MAX_GEOHASHES_FAILSAFE)
    }

    private fun checkGeohashes(actual: Set<GeoHash>, expected: Set<String>) {
        assertEquals(expected.map { GeoHash.fromGeohashString(it) }.toSet(), actual)
    }
}
