import deps.Libs
import deps.Modules

plugins {
    id(deps.Plugins.Android.library)

    kotlin(deps.Plugins.Kotlin.android)
    kotlin(deps.Plugins.Kotlin.androidExtensions)
    kotlin(deps.Plugins.Kotlin.kapt)

    id(deps.Plugins.Android.X.safeArgs)
}

android {
    compileSdkVersion(Libs.AndroidSDKVersions.compileSdkVersion)

    defaultConfig {
        minSdkVersion(App.minSdkVersion)
        targetSdkVersion(Libs.AndroidSDKVersions.projectTargetSdkVersion)
        versionCode = 1
        versionName = "1.0"
    }

    buildFeatures {
        dataBinding = true
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(Libs.Kotlin.stdLib)

    // Common modules
    implementation(project(Modules.presentation))

    // Base modules
    implementation(project(Modules.Base.koin))
    implementation(project(Modules.Base.livedata))
    implementation(project(Modules.Base.viewmodel))
    implementation(project(Modules.Base.rx))

    // Data binding
    implementation(project(Modules.Base.databinding))
    annotationProcessor(Libs.AndroidX.databinding_compiler)
    kapt(Libs.AndroidX.databinding_compiler)

    // Navigation
    implementation(Libs.AndroidArch.Navigation.fragment_ktx)
    implementation(Libs.AndroidArch.Navigation.ui_ktx)

    // Map
    implementation(Libs.GoogleServices.location)
    implementation(Libs.GoogleServices.maps)
    implementation(Libs.GoogleServices.mapsKtx)
    implementation(Libs.GoogleServices.mapsUtils)
    implementation(Libs.GoogleServices.mapsUtilsKtx)
    implementation(Libs.geoHash)

    // Navigation
    implementation(Libs.AndroidArch.Navigation.fragment_ktx)
    implementation(Libs.AndroidArch.Navigation.ui_ktx)

    // Networking
    implementation(Libs.Google.gson)

    // Testing
    testImplementation(Libs.Test.junit)

    // Other libs
    implementation(Libs.AndroidX.constraintLayout)
    implementation(Libs.Rx.permissions)

    // Room
    implementation(Libs.AndroidX.Room.runtime)
    implementation(Libs.AndroidX.Room.rxjava3)
    kapt(Libs.AndroidX.Room.compiler)
}

project.setupTranslations(App.translationConfig.copy(listID = "Map"))
