package deps

object Plugins {
    object Android {
        const val application = "com.android.application"
        const val library = "com.android.library"

        object X {
            const val safeArgs = "androidx.navigation.safeargs"
        }
    }

    object Kotlin {
        const val kapt = "kapt"
        const val android = "android"
        const val androidExtensions = "android.extensions"
    }

    object Firebase {
        const val appDistribution = "com.google.firebase.appdistribution"
        const val crashlytics = "com.google.firebase.crashlytics"
    }

    object Google {
        const val services = "com.google.gms.google-services"
    }
}

object Modules {
    const val app = ":app"
    const val presentation = ":presentation"
    const val domain = ":domain"
    const val data = ":data"
    const val dynamicscreens = ":dynamicscreens"

    object Base {
        const val koin = ":base:b.koin"
        const val version = ":base:b.version"
        const val app = ":base:b.app"
        const val rxongoingprocess = ":base:b.rxongoingprocess"
        const val livedataadapter = ":base:b.livedataadapter"
        const val fullscreen = ":base:b.fullscreen"
        const val androidextensions = ":base:b.androidextensions"
        const val databinding = ":base:b.databinding"
        const val logger = ":base:b.logger"
        const val rx = ":base:b.rx"
        const val validation = ":base:b.validation"
        const val livedata = ":base:b.livedata"
        const val viewmodel = ":base:b.viewmodel"
        const val debug = ":base:b.debug"
        const val permissions = ":base:b.permissions"
        const val sharedprefspersistence = ":base:b.sharedprefspersistence"
        const val geofences = ":base:b.geofences"
    }

    object Features {
        const val subpage = ":feature_subpage"
        const val dashboard = ":feature_dashboard"
        const val terms = ":feature_terms"
        const val signin = ":feature_signin"
        const val dynamic_screen = ":feature_dynamic_screen"
        const val map = ":feature_map"
        const val onboarding = ":feature_onboarding"
        const val forgotten_password = ":feature_forgotten_password"
        const val splash = ":feature_splash"
        const val main = ":feature_main"
    }
}

object Libs {
    object Kotlin {
        const val version = "1.5.10"
        const val stdLib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$version"
    }

    object Gradle {
        const val buildTools = "com.android.tools.build:gradle:4.2.1"
        const val versions = "com.github.ben-manes:gradle-versions-plugin:0.39.0"
    }

    object AndroidSDKVersions {
        const val buildToolsVersion = "29.0.3"
        const val projectTargetSdkVersion = 29
        const val compileSdkVersion = 29
    }

    object Debug {
        const val timber = "com.jakewharton.timber:timber:4.7.1"

        object LeakCanary {
            const val android = "com.squareup.leakcanary:leakcanary-android:2.7"
        }
    }

    object Test {
        const val junit = "junit:junit:4.13"
        const val junitExt = "androidx.test.ext:junit:1.1.1"
        const val junitExtKtx = "androidx.test.ext:junit-ktx:1.1.1"
        const val runner = "androidx.test:runner:1.2.0"
        const val espresso = "androidx.test.espresso:espresso-core:3.2.0"
        const val espressoIdlingResource = "androidx.test.espresso:espresso-idling-resource:3.2.0"
        const val rules = "androidx.test:rules:1.2.0"
    }

    object Koin {
        private const val version = "2.2.3"
        const val gradle_plugin = "io.insert-koin:koin-gradle-plugin:$version"
        const val androidX_viewModel = "io.insert-koin:koin-androidx-viewmodel:$version"
        const val coreExt = "io.insert-koin:koin-core-ext:$version"
        const val androidExt = "io.insert-koin:koin-android-ext:$version"
    }

    object Rx {
        const val java = "io.reactivex.rxjava3:rxjava:3.0.13"
        const val android = "io.reactivex.rxjava3:rxandroid:3.0.0"
        const val kotlin = "io.reactivex.rxjava3:rxkotlin:3.0.1"
        const val permissions = "com.github.tbruyelle:rxpermissions:0.12"
        const val location = "com.patloew.colocation:colocation:1.1.0"
    }

    object AndroidX {
        const val app_compat = "androidx.appcompat:appcompat:1.3.0"
        const val core_ktx = "androidx.core:core-ktx:1.5.0"
        const val annotation = "androidx.annotation:annotation:1.2.0"
        const val legacy_support_v4 = "androidx.legacy:legacy-support-v4:1.0.0"
        const val legacy_support_v13 = "androidx.legacy:legacy-support-v13:1.0.0"
        const val legacy_preference_v14 = "androidx.legacy:legacy-preference-v14:1.0.0"
        const val databinding_runtime = "androidx.databinding:databinding-runtime:4.2.1"
        const val databinding_common = "androidx.databinding:databinding-common:4.2.1"
        const val databinding_compiler = "androidx.databinding:databinding-common:4.2.1"
        const val lifecycle_extensions = "androidx.lifecycle:lifecycle-extensions:2.2.0"
        const val constraintLayout = "androidx.constraintlayout:constraintlayout:2.0.4"
        const val recyclerView = "androidx.recyclerview:recyclerview:1.2.1"
        const val gridLayout = "androidx.gridlayout:gridlayout:1.0.0"
        const val cardView = "androidx.cardview:cardview:1.0.0"
        const val customTabs = "androidx.browser:browser:1.3.0"

        object Room {
            private const val roomVersion = "2.3.0"
            const val runtime = "androidx.room:room-runtime:$roomVersion"
            const val compiler = "androidx.room:room-compiler:$roomVersion"
            const val rxjava3 = "androidx.room:room-rxjava3:$roomVersion"
        }
    }

    object AndroidArch {
        object Navigation {
            private const val version = "2.3.5"
            const val fragment_ktx = "androidx.navigation:navigation-fragment-ktx:$version"
            const val ui_ktx = "androidx.navigation:navigation-ui-ktx:$version"
            const val safe_args = "androidx.navigation:navigation-safe-args-gradle-plugin:$version"
        }
    }

    object GoogleServices {
        const val lib = "com.google.gms:google-services:4.3.8"
        const val location = "com.google.android.gms:play-services-location:18.0.0"
        const val maps = "com.google.android.gms:play-services-maps:17.0.1"
        const val mapsKtx = "com.google.maps.android:maps-ktx:3.1.0"
        const val mapsUtils = "com.google.maps.android:android-maps-utils:2.2.3"
        const val mapsUtilsKtx = "com.google.maps.android:maps-utils-ktx:3.1.0"
        const val auth = "com.google.android.gms:play-services-auth:19.0.0"
    }

    object Firebase {
        const val auth = "com.google.firebase:firebase-auth:21.0.1"
        const val analytics = "com.google.firebase:firebase-analytics:19.0.0"
        const val appDistributionGradle = "com.google.firebase:firebase-appdistribution-gradle:2.1.2"
        const val crashlytics_gradle = "com.google.firebase:firebase-crashlytics-gradle:2.7.0"
        const val crashlytics = "com.google.firebase:firebase-crashlytics:18.0.1"
        const val firestore = "com.google.firebase:firebase-firestore-ktx:23.0.1"
        const val messaging = "com.google.firebase:firebase-messaging:22.0.0"
        const val storage = "com.google.firebase:firebase-storage:20.0.0"
        const val uiStorage = "com.firebaseui:firebase-ui-storage:7.2.0"
    }

    object Coroutines {
        private const val version = "1.5.0"
        const val core = "org.jetbrains.kotlinx:kotlinx-coroutines-core:$version"
        const val android = "org.jetbrains.kotlinx:kotlinx-coroutines-android:$version"
    }

    object UI {
        const val androidMaterial = "com.google.android.material:material:1.3.0"

        object Glide {
            private const val version = "4.12.0"
            const val lib = "com.github.bumptech.glide:glide:$version"
            const val compiler = "com.github.bumptech.glide:compiler:$version"
        }

        const val facebook_keyframes = "com.facebook.keyframes:keyframes:1.1.0"
        const val tapTargetView = "com.getkeepsafe.taptargetview:taptargetview:1.13.2"
    }

    object Network {
        object Retrofit {
            private const val version = "2.9.0"
            const val lib = "com.squareup.retrofit2:retrofit:$version"
            const val converter_scalars = "com.squareup.retrofit2:converter-scalars:2.9.0"
            const val converter_gson = "com.squareup.retrofit2:converter-gson:$version"
            const val adapter_rxJava3 = "com.squareup.retrofit2:adapter-rxjava3:$version"
        }

        object OkHttp {
            private const val version = "4.9.1"
            const val lib = "com.squareup.okhttp3:okhttp:$version"
            const val urlConnection = "com.squareup.okhttp3:okhttp-urlconnection:$version"
            const val logging_interceptor = "com.squareup.okhttp3:logging-interceptor:$version"
        }
    }

    object WorkManager {
        private const val version = "2.5.0"
        const val lib = "androidx.work:work-runtime-ktx:$version"
        const val rxSupport = "androidx.work:work-rxjava3:$version"
    }

    object PlayCore {
        private const val version = "1.8.1"
        const val lib = "com.google.android.play:core-ktx:$version"
    }

    object Facebook {
        const val login = "com.facebook.android:facebook-login:11.0.0"
    }

    object Google {
        const val gson = "com.google.code.gson:gson:2.8.7"
    }

    const val geoHash = "ch.hsr:geohash:1.4.0"
}
