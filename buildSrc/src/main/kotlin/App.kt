object App {
    val applicationId = "cz.synetech.baseapp"
    val version = Version(major = 0, minor = 2, patch = 0, versionCandidate = 0)
    val minSdkVersion = 23
    val firebaseAppDistribution = FirebaseAppDistribution(
            groups = EnumerationParam(listOf("synetech")),
            releaseNotesFilePath = "release_notes.txt"
    )
    val translationConfig = TranslationConfig(sheetID = "1gmGSFDreZp1lQ8aQMjuyZcS4wiHjZ3zhLsorZ0uVsqo")

    enum class Signing(override val id: String) : SigningDefinition {
        DEBUG("debug"),
        STORE("store")
    }

    enum class Flavor(override val id: String, override val suffix: String?) : FlavourDefinition {
        DEV("dev", "dev"),
        STAGE("stage", "stage"),
        PROD("prod", null);

        companion object {
            fun findFlavor(flavorFromBuildConfig: String) = Flavor.values().find { flavor ->
                flavor.id.toLowerCase() == flavorFromBuildConfig.toLowerCase()
            }
        }
    }
}