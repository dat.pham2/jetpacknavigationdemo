plugins {
    `kotlin-dsl`
}

repositories {
    jcenter()
}

sourceSets {
    main {
        java {
            kotlin {
                srcDirs(rootProject.file("../buildSrcShared/src/main/kotlin/").absolutePath)
            }
        }
    }
}