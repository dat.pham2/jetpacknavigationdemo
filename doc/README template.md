PROJECT ONE - Project Workflow and Conventions
=================

This is a live document describing our Git Workflow and code guidelines regarding source code development.

Please do **NOT** place here any confidential data like secret tokens/passwords!

## Project Architecture

## Clone TODO list
Steps you need to do to build this project.

- Supply your own `signing.properties` and `gradle.properties` files. If you are unsure how to get these, see [signing.properties](#signing-properties) and [gradle.properties](#gradle-properties).
- open terminal and navigate to root of the project - run `git submodule update --init`
- run gradle sync (or `./gradlew assembleDevDebug`)
- make sure the project compiles

## Project resources

### Build Configuration

`./fastlane/Fastfile`

### Jira Project

Issue prefix: `TODO`

Link: [TODO: FILL IN JIRA / NOTION BOARD LINK](https://XXX.atlassian.net/projects/YYY/summary)

### Documentation

Link: [TODO: FILL IN CONFLUENCE / NOTION LINK](https://XXX.atlassian.net/wiki/YYY/)

### Visual Prototype

Link: [TODO: FILL IN INVISION / ZEPLIN LINK](https://XXX.YYY.com/)

### Translation Spreadsheet

Link: [TODO: FILL IN GOOGLE SPREADSHEET LINK](https://XXX.YYY.com/)

### Colors Spreadsheet

Link: [TODO: FILL IN GOOGLE SPREADSHEET LINK](https://XXX.YYY.com/)

## Git workflow and branching

![Git Workflow](/doc/branchworkflow.png?raw=true "Git Branching Workflow")

This workflow uses two branches to record the history of the project. The master branch stores the official release history, and the development branch serves as an integration branch for features. It's also convenient to tag all commits in the master branch with a version number.

We are using modified branching model according to [A successful Git branching model](http://nvie.com/posts/a-successful-git-branching-model/).

# Versioning

All release tags are made using [Semantic Versioning 2.0.0](http://semver.org) as the versioning base. This will produce the following example:

2.0.0

- **MAJOR** version when you make backwards incompatible changes,
- **MINOR** version when you add functionality in a backwards-compatible manner,
- **PATCH (ITERATION)** version when you make backwards-compatible bug fixes or improvements.

When the development of the app version is finished, then the last commit is a commit with file that changes version. When the version is merged to `master` and the version is production ready, then the merge commit in master is tagged using version tag (e.g. `v1.2.3`).

## Commits:

**Default branches respecting development flow:**

The code operates on four main branches - `dev`, `stage` and `master`

### `dev`
This is the main branch where the source code of HEAD always reflects a state with the latest delivered development changes for the next release. Larger tasks, issues and/or features are branched into a feature branch only to be merged back into the `dev` branch in the future.

#### Common conventions:
- **Naming convention:** `dev` (or `dev/*` in special cases)
- **branch off:** `master` (or `dev/*` in special cases)
- **merge into:** `stage`,  `master`(if `stage` not used in project), (or `dev/*` in special cases)

### `stage`
Once development has acquired enough features for a release (or a predetermined release date is approaching), you fork a `stage` branch off of `dev`. Creating this branch starts the next release cycle, so no new features can be added after this point only bug fixes, documentation generation, and other release-oriented tasks should go in this branch. Once it's ready to ship, the `stage` gets merged into `master` and tagged with a version number. In addition, it should be merged back into `dev`, which may have progressed since the release was initiated.

Using a dedicated branch to prepare releases makes it possible for one team to polish the current release while another team continues working on features for the next release. It also creates well-defined phases of development (e.g., it's easy to say, this week we're preparing for version 2.0.3 and to actually see it in the structure of the repository).

### `master`
This is the main repository used for deploying releases. No development is done here but rather branched to **dev** or **hotfix** to later be merged back into the `master` branch when ready for a release. The `master` holds all release tags, and tags are made using [Semantic Versioning 2.0.0](http://semver.org).

#### Common conventions:
- **Naming convention:** Refer to [Semantic Versioning 2.0.0](http://semver.org)

### `hotfix`
Hotfix branches are used to quickly patch production releases. This is the only branch that should fork directly off of `master`. As soon as the fix is complete, it should be merged into both `master` and `dev` (or the current release branch), and `master` should be tagged with an updated version number.

#### Common conventions:
- **Naming convention:** `hotfix/*`
- **branch off:** `master`
- **merge into:** `master`/`dev`

### `issue`
Feature branches (or sometimes called topic branches) are used to development new features for the upcoming or a distant future release. When starting development of a feature, the target release in which this feature will be incorporated may well be unknown at that point. The essence of a feature branch is that it exists as long as the feature is in development, but will eventually be merged back into `dev` (to definitely add the new feature to the upcoming release) or discarded (in case of a disappointing experiment).

**issue branches:**

- **issue/_ISSUE-ID_** (where _ISSUE-ID_ is ID from project tracking system)
        -   Contains commits related to feature with given _ISSUE-ID_.
        -   Commits do not have to be build-passing but the final one MUST be.

#### Common conventions:
- **branch off:** `dev` (or `stage` in special cases)
- **merge into:** `dev` (or `stage` in special cases)
- **naming convention:** `issue/_ISSUE-ID_`

## Code Approval

!!! Every code change in non-feat-and-fix branches **MUST** be approved by checking code style, static code analysis and must be merged using pull request to proper developer(s). !!!

## Commits & Commit Messages

Every logic part (feature) should be divided into commits. Tasks should be divided into smaller sub-tasks (subproblems) where every sub-task is a new commit (in one branch). By keeping commits grained code history and progress of development is more meaningful and roll-back to older versions of code is easier. Different users should not work on the same branch since there will be conflict in pushing changes. If a user is leaving a office or won't work on project for longer time, it's needed to create new commit and push it to remote branch so the code and changes are backed up and accessible to other developers in case of need.

### Commit Message

Each commit must contain commit message in English. Commit message should contain description of what was done and in special cases how it was done.

All commits must be associated with an Issue in JIRA. **If there isn't the you should create a new one**. For example, you might need to refactor some code. Instead of logging your time to the last issue you were working on, create a new issue called "refactoring code" and commit that code with a comment to that very issue. That will keep ourselves informed on what's happening and why someone just "wasted" 8 hours work on some seamingly meaningless task that didn't produce any neat features.

#### Structure of message

##### One-line message:

`_PREFIX_ ISSUE-ID* COMMIT_MESSAGE`

##### Multiple-line message:

`_PREFIX_ ISSUE-ID* BRIEF_COMMIT_MESSAGE`

`<empty line>`

`OTHER LINES (detailed commit message)`

#### Table of PREFIXes
Prefix | Usage
--- | --- 
`FIX`	| Fix of a feature (bugfix, hotfix, ...)
`UI`	| Manipulation with UI objects without effect to change of logic
`REF`	| Code refactoring
`LIB`	| Manipulation with libraries (added, removed, upgraded, ...)
`FEAT`	| Implementation of new feature
`VERSION`		| Version bump (commit message contains only version number - no ISSUE-ID)
`RELEASENOTE`		| Part of commit with any of identifier mentioned above. Text after the RELEASENOTE keyword will be processed to client. This note must be written in language of whole project (default:English). Note must not contain any technical notes or vulgar expressions.

#### Examples

One-line commit message:

`FIX EP-201 Fixed wrong parameter passed to login function RELEASENOTE Fixed login issues`

`UI EP-100 Login button color changed to green`

`REF Application.java`

`LIB EP-1 Added GSON library`

`FEAT EP-123 Added setting screen`

`VERSION 1.2.3`

Multiple-line commit message:

`FIX OR-201 Fixed wrong parameter passed to login function` 
`<empty line>`
`RELEASENOTE Fixed login issues`

## Further details and complete guide
- [A successful Git branching model](http://nvie.com/posts/a-successful-git-branching-model/)
- [Semantic Versioning 2.0.0](http://semver.org)

## Valuable Tools

- [Gitflow](https://github.com/nvie/gitflow)
- [SourceTree](https://www.sourcetreeapp.com/)
- [StackEdit](https://stackedit.io/)

# The Repository Team

### Development Team

TODO: Fill in real team

- [John Doe](mailto:john@doe.com) - Software Architect 
- [Jane Doe](mailto:jane@doe.com) - Android Developer
- [Josh Doe](mailto:josh@doe.com) - Tester

# Project Specific Development Information

Application ID `com.company.product.TODO`

## Product Flavours

Flavour | Purpose | Recommended buildtype | Branch Trigger
--- | --- | --- | ---
`dev` | Development flavour used by developers on **development** environment, should be signed with **debug key** | `Debug` | `dev`, PRs
`stage` | Staging flavour used for testing on **testing** environment, should be signed with **debug key** | `Release` | `stage`
`prod` | Production flavour - connected to **production** environment, should be signed with **release key**| `Release` | `master`

# Useful Commands

`echo "doDownloadTranslations=true" >> gradle.properties` to allow download translations from server

`git commit -m "VERSION 0.2.0"` version bump commit

Mark version commit with tag `git tag v0.2.0`

# Signing Properties

Please create file `signing.properties` in with following content:

```
SIGNING_DEBUG_STORE_FILE=/Path/To/Keystore/synetech_debug.jks
SIGNING_DEBUG_STORE_PASSWORD=V6NjmCAkvYpn^E(2VZ
SIGNING_DEBUG_KEY_ALIAS=debug
SIGNING_DEBUG_KEY_PASSWORD=V6NjmCAkvYpn^E(2VZ

SIGNING_STORE_STORE_FILE=/Path/To/Keystore/synetech_debug.jks
SIGNING_STORE_STORE_PASSWORD=V6NjmCAkvYpn^E(2VZ
SIGNING_STORE_KEY_ALIAS=debug
SIGNING_STORE_KEY_PASSWORD=V6NjmCAkvYpn^E(2VZ
```

*Note: Edit paths and secrets if needed, but DO NOT place secrets here in `README.md`*

# Gradle Properties

Please add following content to `gradle.properties`:
```
org.gradle.jvmargs=-Xmx4g -XX:MaxMetaspaceSize=512m -XX:+HeapDumpOnOutOfMemoryError -Dfile.encoding=UTF-8
org.gradle.daemon=true
org.gradle.parallel=true
org.gradle.configureondemand=true
kotlin.parallel.tasks.in.project=true
android.useAndroidX=true
android.enableJetifier=true

doDownloadTranslations=true
```

# Android Architecture

## App Components

![](doc/AND_ARCH_App.png)

Every app is composed of following components:

- **Application** component which connects all features ( `app` module)
- **Feature components** that provide feature-specific functionality (`feature` modules)
- **Shared domain** which provides shared domain functionality for *feature components* and *application* ( `domain` module)
- **Shared presentation** module which provides shared Android resources and presentation classes for *feature components* and *application* (`presentation` module)
- **Android SDK & party libs & base modules**  - external dependencies

Feature components and shared domain require either injection of functionality or some kind of parametrisation. For this reason, the project needs one more component - **data** component which provides implementation of interfaces from shared domain and from feature components.

## Modules Overview

Project is composed of following modules (alphabetical ordering):

- `app` module,
- `base` modules (optional),
- `data` module,
- `domain` module (optional),
- `feature` modules,
- `presentation` module (optional)

### `app` module

*Top-level module that composes all modules together (`data`, `domain` and `feature` modules).*

**Responsibilities:**

- Defines app building parameters:
    - Application ID
    - Flavours
    - Signing configurations

- Provides launcher icons
- Sets up application-wide frameworks (Logger, Crashlytics, Google Analytics, …)

 **Dependencies:**

- `data` module (must-have)
- `feature` modules (must-have)
- `domain` module (if exists)
- `presentation` module (if exists)
- `base` modules (if exists and required)

**Note**: Package name for classes is `cz.synetech.app`

### `data` module

*A module that provides implementation for repositories, use-cases and other interfaces required by `domain` and `feature` modules as well as other implementations such as datasources, APIs and other data specific classes.*

**Naming:**

- Package name for classes should start with package name of interface that is being implemented, for example:
    - `cz.synetech.app.data.` for app specific interfaces
    - `cz.synetech.shared.data.` for domain (shared) interfaces
    - `cz.synetech.feature.NAME.data.` for feature specific interfaces

 **Responsibilities:**

- Provides implementation for `domain` interfaces
- Provides implementation for `feature` domain interfaces

**Dependencies:**

- `feature` modules (must-have)
- `domain` module (if exists)
- `base` modules (if exists and required)

### `feature` modules

Feature modules that highly de-coupled from app and can be reused. Every project has one feature module at least. Custom content should be injected (for example: if “about” feature module requires repository with app information, then implementation should be provided by app data module, not by feature module).

**Naming:** Feature modules must start with `feature_` prefix.

**Responsibilities:**

- Provides closed feature logic - typically a subset of screens

**Dependencies:**

- `domain` module (optional)
- `presentation` module (optional)
- `base` modules (optional)
- 3rd party libraries

**Note**: Package name for classes is `cz.synetech.feature.NAME` where name is name of the feature - for example `cz.synetech.feature.about` for “about” feature module.

### `domain` module (optional)

*Domain logic shared through project. This module can be omitted when there is no shared domain logic.* **Responsibilities:**

- Provides models - `cz.synetech.shared.model` package
- Provides repositories - `cz.synetech.shared.repository` package
- Provides use-cases - `cz.synetech.shared.usecase` package

**Dependencies:**

- **none** or `base` modules

**Note**: Package name for classes is `cz.synetech.shared`

### `presentation` module (optional)

*A module for sharing Android resources through features. This module can be omitted when there are no shared resources.*

**Responsibilities:**

* Provide resources for other modules

**Dependencies:**

- 3rd party libraries
- `domain` module (optional)
- `base` modules (optional)

### `base` modules (optional)

*Low level reusable modules that are shared over all projects. These modules can be omitted when there are no base modules.*

**For example:**

- Android extensions
- Databinding adapters
- Debug helpers
- Koin extensions
- LiveData extensions
- Logger implementations
- RX extensions
- Validation checkers
- … and other ones

 **Naming:** Every base module is located in `base/` directory and starting with `b.` prefix - for example: `base/b.logger`. This convention is because of grouping of base modules in Android Studio.

**Responsibilities:**

- Provides reusable functionalities over projects

**Dependencies:**

- 3-rd party libraries (NO PROJECT DEPENDENCIES)

**Note:** Package name for classes is `cz.synetech.base.NAME` where name is name of base module - for example `cz.synetech.base.logger` for `b.logger` module.

## Feature Modules

### High-level Archítecture

![](doc/AND_ARCH_Layers.png)

- **Presentation layer:**
    - UI - uses view-models
    - ViewModel - uses usecases and repositories from domain layer
- **Domain layer:**
    - Usecases
    - Repositories
    - Models
- **Data layer:**
    - Datasources
    - APIs
    - Repositories
    - Models

### Presentation layer

This layer defines what user sees and what user can interact with. Presentation layer depends on *domain layer*. We can split presentation layer into two sub-layers: *UI* (user interface) and *ViewModel*.

**UI** - a set of activities, fragments, views, dialogs, list adapters, …

**ViewModel** - holds state of view and serves as bridge between UI and *domain layer* (business logic).

### **Domain layer**

This layer defines whole business logic of the feature-app and has **no dependency to other layer**. Ideally this should be well testable and platform independent (fewer dependencies is better). The heart of domain layer is use-cases that manipulates with data from repositories. Models in this layer should not reflect UI.

### **Data layer**

This layer provides datasources and implementation of necessary interfaces from business logic from *domain layer*. Data layer depends on *domain layer* and on many 3rd party libraries such as database providers, framework SDKs, …

### Package Structure

- *.data - represents data layer
- *.domain
- *.presentation.viewmodel
- *.presentation.ui

    - data (only for internal implementation of interfaces of current feature domain)
        - api
        - model
        - datasource
        - repository
    - domain (can expose interface out of module)
        - model
        - repository
        - usecase
    - presentation
        - model
        - viewmodel
        - ui
            - view
            - dialog
            - activity
            - fragment
            - viewholder
            - adapter
    FeatureModule.kt (e.g. AboutFeatureModule.kt)
    FeatureRouter.kt (e.g. AboutRouter.kt)

A feature module should expose only required interfaces (for example: domain repositories, domain models, domain usecases, router interface) , activities, fragments and its module class.