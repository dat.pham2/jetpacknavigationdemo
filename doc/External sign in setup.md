# Google SignIn setup

- Firebase console -> `Authentication` -> `Sign-in method` -> `Google` -> Fill in project support email and enable. Click Save.

- `Project Overview` -> Application settings (whichever) -> `Add fingerprint` for all flavors (release should use release key)

### How to get SHA1 fingerprint:
```
keytool -v -list -keystore ./synetech/synetech_debug.jks
```
and copy the string after "SHA1: ".


# Facebook SignIn setup

- Go to `https://developers.facebook.com/docs/facebook-login/android`

- Click `Create a New App` -> `For Everything Else` -> Fill in app name and email

- Go to `Settings` -> `Basic` and fill in `Privacy Policy URL` (use `https://mockflow.com/privacy_policy/` if you have to) -> `Save Changes`

- Switch your app from `In developement` to `Live` in top bar.

- Go to `Add a Product` -> Facebook Login `Set Up` button -> Android -> Skip to step 3 -> Fill in your package name and default activity -> Fill in key hashes and `Save`

- Skip to step 6. Find `fb_strings.xml` and replace `fb.app.id` and `fb.login.protocol.scheme` with the ones provided in step 6. That's it for setting up Facebook Login.

### How to get keyhashes for facebook:
Use
```
keytool -v -list -keystore ./synetech/synetech_debug.jks
```
to get the SHA1 fingerprint. And

```
echo "[SHA1_FINGERPRINT]" | xxd -r -p | openssl base64
```
to get the key hash. Do this for both debug and release keys.

