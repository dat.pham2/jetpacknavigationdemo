Fork TODO list
=================

Steps you need to do after forking this repository.

# README
Copy [template readme](README template.md) and place it in the root of your project. Fill in missing TODOs.

# CHANGELOG
Copy [template changelog](CHANGELOG template.md) and place it in the root of your project.

# App
- in `buildSrc/src/main/kotlin/App.kt`
	- change `applicationId`
	- change `version`
	- change `translationConfig`:
	    * override the `sheetID` with the id of the corresponding google sheet
	    * ensure that `owncloud@synetech.cz` can access that google sheet (file -> share -> add the account as viewer)
	    * for a module, if you want to apply a specific list which is part of the google sheet, add line 
	      `project.setupTranslations(App.translationConfig.copy(listID = "[LIST_ID]"))` at the very end of the module's
	      `build.gradle.kts` file, where `[LIST_ID]` is the name of that list (case sensitive)

# Firebase 
- Check if someone has created Firebase project - if not, create new project
- Add Android applications - every flavor has to be an application 
	- Android package name is package ID + flavor suffix
- Download `google-services.json` from any of the application's setting on Firebase and replace `app/google-services.json`
- Specify which flavors to upload in `fastlane/fastfile` - `@flavoursToUploadToFirebase = ['dev', 'stage']`
- Open settings in Firebase console for your project, go to `Service accounts` tab and click on `Manage service account permissions` link in top right (you will be redirected to *Google Cloud Platform*). Tap on *Create service account*, fill in name (e.g. app-distribution), add *Firebase App Distribution Admin* role to the account and then generate a private key (json).
- Add the json as a variable to your gitlab repository (or the whole group - for both iOS and Android project) for key `GOOGLE_APPLICATION_CREDENTIALS` .
- Open the firebase project and activate *Firebase App Distribution.* You have to agree with their T&C otherwise you get an error like this one

```
Error: failed to fetch app information. HTTP Error: 403, Firebase App Distribution API has not been used in project [MASKED] before or it is disabled. Enable it by visiting https://console.developers.google.com/apis/api/firebaseappdistribution.googleapis.com/overview?project=[MASKED] then retry. If you enabled this API recently, wait a few minutes for the action to propagate to our systems and retry.
```

# Signing
See [Android certificates repository](https://bitbucket.org/synetech/android-certificates/src/template/) and follow instructions in their README to create signing for a new app.

# Fastfile
Modify [fastlane/Fastfile](./fastlane/Fastfile) based on your app's configuration - follow TODOs.

# External SignIn
If you are planning to use external sign ins (Google and Facebook) in your app, see [setup](./External sign in setup.md).

# Cleanup
From `doc` remove
- `CHANGELOG template.md`
- `README template.md`
- `External sign in setup.md`
- `Fork TODO.md`
