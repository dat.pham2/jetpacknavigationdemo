applyBuildScript("codeQuality/detekt.gradle")

tasks.register("codeQuality") {
    dependsOn(tasks.getAt("detekt"))
}