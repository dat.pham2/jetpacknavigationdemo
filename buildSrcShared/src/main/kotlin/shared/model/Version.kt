data class Version(
        val major: Int,
        val minor: Int,
        val patch: Int,
        val versionCandidate: Int
) {
    fun versionCode() = major * 1000000 + minor * 10000 + patch * 100 + versionCandidate

    fun versionName() = "$major.$minor.$patch.$versionCandidate"

    fun shortVersionName() = "$major.$minor.$patch"
}