interface FlavourDefinition {
    val id: String
    val suffix: String?

    fun applicationIdSuffix() = if (suffix != null) ".$suffix" else ""

    fun versionNameSuffix() = if (suffix != null) "-$suffix" else ""
}