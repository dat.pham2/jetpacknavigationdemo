data class AndroidSDKVersions(
        val buildToolsVersion: String,
        val projectTargetSdkVersion: Int,
        val compileSdkVersion: Int
)