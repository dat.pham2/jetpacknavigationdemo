data class FirebaseAppDistribution(
        val groups: FileOrEnumerationParam? = null,
        val testers: FileOrEnumerationParam? = null,
        val releaseNotesFilePath: String? = null
)

sealed class FileOrEnumerationParam {
    abstract val serializedContent: String
}

data class FileParam(
        val path: String
): FileOrEnumerationParam() {
    override val serializedContent: String
        get() = path
}

data class EnumerationParam(
        val items: List<String>
): FileOrEnumerationParam() {
    override val serializedContent: String
        get() = if (items.isEmpty()) "" else items.joinToString(",")
}