data class TranslationConfig(
        val host: String = "translations.bbeight.synetech.cz",
        val platform: String = "android",
        val destinationPath: String = "/src/main/res/",
        val sheetID: String? = null,
        val listID: String = "App",
        val keyColumn: Int = 1,
        val commentColumn: Int = 2,
        val firstLangColumn: Int = 3,
        val defaultLangColumn: Int = 3
)