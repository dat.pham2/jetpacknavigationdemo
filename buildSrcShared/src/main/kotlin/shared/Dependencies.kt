package shared.deps

object Plugins {
    object Detekt {
        const val id = "io.gitlab.arturbosch.detekt"
        const val version = "1.1.1"
    }
}

object Libs {
    object CodeQuality {
        object Detekt {
            const val formatting = "io.gitlab.arturbosch.detekt:detekt-formatting:${Plugins.Detekt.version}"
        }
    }
}