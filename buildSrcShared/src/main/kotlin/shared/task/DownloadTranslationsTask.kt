import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import org.gradle.kotlin.dsl.*
import java.io.File
import java.io.FileOutputStream
import java.net.URL
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream

private const val propertyName = "doDownloadTranslations"

open class DownloadTranslationsTask : DefaultTask() {
    @Input
    val config = project.objects.property(TranslationConfig::class.java)

    @TaskAction
    fun download() {
        if (project.properties.containsKey(propertyName) && project.properties[propertyName] == "true") {
            val trConfig = config.get()
            if (trConfig.sheetID == null) {
                println("Translations: No sheet ID defined -> skipping")
            } else {
                val urlString = "https://${trConfig.host}/" +
                        "${trConfig.platform}" +
                        "/sheetID/${trConfig.sheetID}" +
                        "?listID=${trConfig.listID}" +
                        "&key=${trConfig.keyColumn}" +
                        "&comment=${trConfig.commentColumn}" +
                        "&firstLang=${trConfig.firstLangColumn}" +
                        "&defaultLang=${trConfig.defaultLangColumn}" +
                        "&directDownload=yes"
                val url = URL(urlString)
                val zipStream = ZipInputStream(url.openStream())
                val buffer = ByteArray(2048)
                try {
                    var entry: ZipEntry? = null
                    while ({ entry = zipStream.nextEntry; entry }() != null) {
                        entry?.let {
                            if (!it.isDirectory) {
                                val outpath = project.projectDir.absolutePath + trConfig.destinationPath + it.name
                                println("Translations: -> Writing to: " + outpath)
                                val file = File(outpath)
                                file.parentFile.mkdirs()
                                var output: FileOutputStream? = null
                                try {
                                    output = FileOutputStream(file)
                                    var len = 0
                                    while ({ len = zipStream.read(buffer); len }() > 0) {
                                        output.write(buffer, 0, len)
                                    }
                                } finally {
                                    if (output != null) {
                                        output.close()
                                    }
                                }
                            }
                        }
                    }
                } finally {
                    zipStream.close()
                }
            }

        } else {
            println("Translations: `$propertyName` property isn't set to `true` -> skipping")
        }
    }
}

fun Project.setupTranslations(translationConfig: TranslationConfig) {
    tasks {
        val downloadTranslations by registering(DownloadTranslationsTask::class) {
            config.set(translationConfig)
        }
        val preBuild by existing
        preBuild {
            dependsOn(downloadTranslations)
        }
    }
}