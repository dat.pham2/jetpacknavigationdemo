package shared

import applyBuildScript
import org.gradle.api.Project
import org.gradle.kotlin.dsl.ScriptHandlerScope
import org.gradle.plugin.use.PluginDependenciesSpec

@Suppress("UNUSED_PARAMETER")
fun setupAllProjects(project: Project) {}

@Suppress("UNUSED_PARAMETER")
fun setupPlugins(scope: PluginDependenciesSpec) {
    scope.apply {
        id(shared.deps.Plugins.Detekt.id).version(shared.deps.Plugins.Detekt.version)
    }
}

@Suppress("UNUSED_PARAMETER")
fun setupSubProjects(project: Project) {
    project.applyBuildScript("codeQuality.gradle.kts")
}

@Suppress("UNUSED_PARAMETER")
fun setupBuildScript(scope: ScriptHandlerScope) {}