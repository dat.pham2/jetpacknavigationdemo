import org.gradle.api.Project
import org.gradle.api.plugins.ExtraPropertiesExtension
import java.io.File
import java.io.FileInputStream
import java.util.Properties

fun Project.loadPropertiesToExtra(path: String, extra: ExtraPropertiesExtension) = this.loadPropertiesToExtra(file(path), extra)

fun Project.loadPropertiesToExtra(file: File, extra: ExtraPropertiesExtension) {
    this.loadProperties(file)?.forEach { (k, v) ->
        extra["$k"] = v
    }
}

fun Project.loadProperties(file: File): Properties? {
    if (!file.exists()) return null
    return Properties().apply {
        load(FileInputStream(file))
    }
}