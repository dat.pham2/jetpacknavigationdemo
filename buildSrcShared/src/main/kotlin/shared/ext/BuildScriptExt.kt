import org.gradle.api.Project

fun Project.applyBuildScript(filename: String) {
    apply {
        from(sharedBuildSrcGradleDirFile(filename))
    }
}

fun Project.sharedBuildSrcGradleDirFile(filename: String): String {
   return this.sharedBuildSrcFile("gradle/$filename")
}

fun Project.sharedBuildSrcFile(filename: String): String {
    return rootProject.rootDir.absolutePath + "/buildSrcShared/$filename"
}