import org.gradle.api.Project
import java.io.File

fun Project.loadSingingConfiguration(file: File, signingDefinition: SigningDefinition): SigningConfig? {
    return loadSingingConfiguration(file, signingDefinition.id)
}

fun Project.loadSingingConfiguration(file: File, singingId: String): SigningConfig? {
    return loadProperties(file)?.let {
        SigningConfig(
                keyAlias = it["SIGNING_${singingId}_KEY_ALIAS"] as String? ?: "",
                keyPassword = it["SIGNING_${singingId}_KEY_PASSWORD"] as String? ?: "",
                storeFile = it["SIGNING_${singingId}_STORE_FILE"] as String? ?: "",
                storePassword = it["SIGNING_${singingId}_STORE_PASSWORD"] as String? ?: ""
        )
    }
}