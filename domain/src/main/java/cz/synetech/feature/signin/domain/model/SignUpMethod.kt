package cz.synetech.feature.signin.domain.model

enum class SignUpMethod {
    EMAIL,
    FACEBOOK,
    GOOGLE;

    companion object {
        fun fromString(name: String): SignUpMethod? {
            return when (name) {
                EMAIL.name -> EMAIL
                FACEBOOK.name -> FACEBOOK
                GOOGLE.name -> GOOGLE
                else -> null
            }
        }
    }
}
