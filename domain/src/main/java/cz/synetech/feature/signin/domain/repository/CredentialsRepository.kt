package cz.synetech.feature.signin.domain.repository

import cz.synetech.feature.signin.domain.model.SignUpMethod

interface CredentialsRepository {
    fun storeSignUpInfo(signUpMethod: SignUpMethod, signUpInfo: String, password: String?)
    fun isUserSignedIn(): Boolean
    fun logoutUser()
}
