package cz.synetech.feature.signin.domain.repository

interface StorageRepository {
    fun getValue(key: String): String?
    fun getEncryptedValue(key: String): String?
    fun storeValue(key: String, value: String?)
    fun storeValueEncrypted(key: String, value: String?)
    fun clear()
}
