package cz.synetech.app.navigation

import android.app.Activity

interface RestartRouter {
    fun restartApplication(caller: Activity)
}
