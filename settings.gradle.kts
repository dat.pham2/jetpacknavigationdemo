rootProject.buildFileName = "build.gradle.kts"

include(
    "feature_subpage",
    ":app",
    ":presentation",
    ":data",
    ":domain",
    "base:b.app",
    "base:b.logger",
    "base:b.rx",
    "base:b.validation",
    "base:b.livedata",
    "base:b.viewmodel",
    "base:b.debug",
    "base:b.databinding",
    "base:b.androidextensions",
    "base:b.fullscreen",
    "base:b.livedataadapter",
    "base:b.rxongoingprocess",
    "base:b.version",
    "base:b.koin",
    "base:b.permissions",
    "base:b.sharedprefspersistence",
    "base:b.geofences",
    "feature_main",
    "feature_splash",
    "feature_onboarding",
    "feature_forgotten_password",
    "feature_map",
    "feature_dynamic_screen",
    ":dynamicscreens",
    "feature_signin",
    "feature_terms",
    "feature_dashboard"
)
